/**
 * @format
 */
// import React from 'react';

import {AppRegistry} from 'react-native';
import 'react-native-get-random-values';

import {App} from './src/App';
import {name as appName} from './app.json';

import './src/i18n';

// if (process.env.NODE_ENV === 'development') {
//   const whyDidYouRender = require('@welldone-software/why-did-you-render');
//   whyDidYouRender(React, {
//     trackAllPureComponents: true,
//   });
// }

AppRegistry.registerComponent(appName, () => App);

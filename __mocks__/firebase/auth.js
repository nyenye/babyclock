export default () => ({
  currentUser: {},
  onAuthStateChanged: jest.fn(),
  createUserWithEmailAndPassword: jest.fn(),
  signInWithEmailAndPassword: jest.fn(),
});

const FirebaseAuthTypes = {
  UserCredentail: true,
};

export {FirebaseAuthTypes};

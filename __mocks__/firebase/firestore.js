export default () => ({
  collection: jest.fn(),
});

const FirebaseFirestoreTypes = {
  DocumentSnapshot: true,
  QueryDocumentSnapshot: true,
};

export {FirebaseFirestoreTypes};

// test-utils.js
import React from 'react';
import {render} from '@testing-library/react-native';

import '../src/i18n';

import {ThemeHook} from '../src/hooks';

const AllTheProviders = ({children}) => {
  return (
    <ThemeHook.Provider defaultTheme="light">{children}</ThemeHook.Provider>
  );
};

const customRender = (ui, options) =>
  render(ui, {wrapper: AllTheProviders, ...options});

// re-export everything
export * from '@testing-library/react-native';

// override render method
export {customRender as render};

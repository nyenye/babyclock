import React from 'react';
import {StyleSheet} from 'react-native';

import {View} from '../Layout';
import {Colors} from '../../styles';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    zIndex: 10,
  },
  cell: {
    backgroundColor: 'transparent',
    borderBottomColor: Colors.tealLight,
  },
});

function getDynamicStyles(index: number) {
  return StyleSheet.create({
    cell: {
      borderBottomWidth: index % 6 === 0 && index < 24 ? 1.5 : 0.5,
    },
  });
}

const cells = [
  1,
  2,
  3,
  4,
  5,
  6,
  7,
  8,
  9,
  10,
  11,
  12,
  13,
  14,
  15,
  16,
  17,
  18,
  19,
  20,
  21,
  22,
  23,
  24,
];

function DayDivider() {
  return (
    <View
      direction="column"
      position="absolute"
      fill="fillBoth"
      style={styles.container}>
      {cells.map((n: number) => {
        return (
          <View
            key={n}
            flex={1}
            style={[styles.cell, getDynamicStyles(n).cell]}
          />
        );
      })}
    </View>
  );
}

export {DayDivider};

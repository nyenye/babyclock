import React from 'react';
import {StyleSheet, View} from 'react-native';

import {Text} from '../Typography';

const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
    paddingBottom: 0,
    paddingRight: 15,
  },
  text: {
    flex: 1,
    marginTop: -10,
  },
  lastHour: {
    position: 'absolute',
    bottom: -10,
  },
});

function HourIndicator() {
  return (
    <View style={styles.container}>
      <Text size="small" style={styles.text}>
        00:00
      </Text>
      <Text size="small" style={styles.text}>
        03:00
      </Text>
      <Text size="small" weight="bold" style={styles.text}>
        06:00
      </Text>
      <Text size="small" style={styles.text}>
        09:00
      </Text>
      <Text size="small" weight="bold" style={styles.text}>
        12:00
      </Text>
      <Text size="small" style={styles.text}>
        15:00
      </Text>
      <Text size="small" weight="bold" style={styles.text}>
        18:00
      </Text>
      <Text size="small" style={styles.text}>
        21:00
      </Text>
      <Text size="small" style={styles.lastHour}>
        24:00
      </Text>
    </View>
  );
}

export {HourIndicator};

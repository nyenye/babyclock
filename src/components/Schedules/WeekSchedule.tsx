import React from 'react';
import {StyleSheet, Dimensions} from 'react-native';

import {View} from '../Layout';
import {TrackingHightlight} from './TrackingHighlight';
import {HourIndicator} from './HourIndicator';
import {DaySchedule} from './DaySchedule';

import {TrackingType} from '../../types';

import {Layouts} from '../../styles';
import {ReportHook} from '../../hooks';

import data from '../../../mock-data.json';

const windowHeight = Dimensions.get('window').height;

const typedData: Array<{
  type: TrackingType;
  start: string;
  end: string;
}> = data as Array<{type: TrackingType; start: string; end: string}>;

const styles = StyleSheet.create({
  container: {
    height: windowHeight - 175,
  },
});

function WeekSchedule() {
  const {activeTrackingTypes} = ReportHook.useState();

  const filtered = typedData.filter((item) =>
    activeTrackingTypes.has(item.type),
  );

  return (
    <View direction="row" main="justifyStart" style={styles.container}>
      <HourIndicator />
      <View style={[Layouts.expand, Layouts.row]}>
        <DaySchedule dayOfTheWeek={0}>
          {filtered.map((range) => {
            return (
              <TrackingHightlight
                key={range.start}
                type={range.type}
                start={range.start}
                end={range.end}
              />
            );
          })}
        </DaySchedule>
        <DaySchedule dayOfTheWeek={1} />
        <DaySchedule dayOfTheWeek={2} />
        <DaySchedule dayOfTheWeek={3}>
          {filtered.map((range) => {
            return (
              <TrackingHightlight
                key={range.start}
                type={range.type}
                start={range.start}
                end={range.end}
              />
            );
          })}
        </DaySchedule>
        <DaySchedule dayOfTheWeek={4} />
        <DaySchedule dayOfTheWeek={5} />
        <DaySchedule dayOfTheWeek={6} />
      </View>
    </View>
  );
}

export {WeekSchedule};

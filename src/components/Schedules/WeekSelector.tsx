import React from 'react';
import {useTranslation} from 'react-i18next';
import dayjs from 'dayjs';

import {View} from '../Layout';
import {Button} from '../Button';
import {Text} from '../Typography';

import {Typography} from '../../styles';
import {ReportHook} from '../../hooks';
import {TFunction} from 'i18next';

type GetWeekReturn = {
  currentYear: string;
  currentStartOfWeek: string;
  currentEndOfWeek: string;
  isNextDisabled: boolean;
};

function getWeek(startingWeek: string, t: TFunction): GetWeekReturn {
  const date = dayjs(startingWeek);
  const currentYear = date.format('YYYY');
  const currentStartOfWeek = date.format(t('DATE_FORMAT_DAY_MONTH'));
  const currentEndOfWeek = date
    .endOf('week')
    .format(t('DATE_FORMAT_DAY_MONTH'));

  const isNextDisabled = !date.isBefore(dayjs().startOf('week'));

  return {currentYear, currentStartOfWeek, currentEndOfWeek, isNextDisabled};
}

function WeekSelector() {
  const {t} = useTranslation('translations');

  const {currentStartOfWeek} = ReportHook.useState();
  const dispatch = ReportHook.useDispatch();

  function goToPrevWeek() {
    dispatch({type: 'PREV_WEEK'});
  }

  function goToNextWeek() {
    dispatch({type: 'NEXT_WEEK'});
  }

  const state = getWeek(currentStartOfWeek, t);

  return (
    <View direction="row" fill="fillWidth">
      <Button
        icon="chevron-left"
        type="primary"
        onPress={goToPrevWeek}
        size="small"
      />
      <View
        direction="column"
        main="justifyCenter"
        cross="alignCenter"
        flex={1}>
        <Text weight="bold" style={Typography.color.teal}>
          {state.currentYear}
        </Text>
        <Text style={Typography.color.teal}>
          {state.currentStartOfWeek} - {state.currentEndOfWeek}
        </Text>
      </View>
      <Button
        icon="chevron-right"
        type="primary"
        onPress={goToNextWeek}
        disabled={state.isNextDisabled}
        size="small"
      />
    </View>
  );
}

export {WeekSelector};

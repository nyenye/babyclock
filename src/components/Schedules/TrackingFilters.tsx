import React from 'react';

import {View} from '../Layout';
import {TrackingType} from '../../types';

import {TrackingFilter} from './TrackingFilter';
import {ReportHook} from '../../hooks';

const trackingTypes: Array<TrackingType> = ['sleeping', 'feeding'];

function TrackingFilters() {
  const {activeTrackingTypes} = ReportHook.useState();
  const dispatch = ReportHook.useDispatch();

  const onFilterPress = React.useCallback(
    (trackingTypeToToggle: TrackingType) => {
      dispatch({type: 'TOGGLE_TRACKING_FILTER', filter: trackingTypeToToggle});
    },
    [dispatch],
  );

  return (
    <View direction="row" main="justifyEnd" fill="fillWidth">
      {trackingTypes.map((type) => (
        <TrackingFilter
          key={type}
          type={type}
          isActive={activeTrackingTypes.has(type)}
          onFilterPress={onFilterPress}
        />
      ))}
    </View>
  );
}

export {TrackingFilters};

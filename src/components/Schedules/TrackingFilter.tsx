import React from 'react';
import {StyleSheet, TouchableWithoutFeedback, Image} from 'react-native';

import {View} from '../Layout';
import {Colors} from '../../styles';
import {TrackingType} from '../../types';
import {ThemeHook} from '../../hooks';
import {TrackingUtils} from '../../utils';

const styles = StyleSheet.create({
  container: {
    height: 40,
    width: 40,
    marginLeft: 10,
    borderRadius: 40 / 2,
  },
  innerContainer: {
    height: 38,
    width: 38,
    borderRadius: 38 / 2,
  },
});

function getDynamicStyles(
  theme: ThemeHook.Theme,
  type: TrackingType,
  isActive: boolean,
) {
  return StyleSheet.create({
    container: {
      backgroundColor: TrackingUtils.typeToColor[type],
    },
    innerContainer: {
      backgroundColor: Colors.backgroundColors[theme],
    },
    icon: {
      opacity: isActive ? 1 : 0.5,
    },
  });
}

type TrackingFilterProps = {
  type: TrackingType;
  isActive: boolean;
  onFilterPress: Function;
};

function TrackingFilter({type, isActive, onFilterPress}: TrackingFilterProps) {
  const {theme} = ThemeHook.useState();

  const dynamicTypes = getDynamicStyles(theme, type, isActive);

  function onPress() {
    onFilterPress(type);
  }

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View
        main="justifyCenter"
        cross="alignCenter"
        style={[styles.container, dynamicTypes.container]}>
        <View
          main="justifyCenter"
          cross="alignCenter"
          style={[styles.innerContainer, dynamicTypes.innerContainer]}>
          <Image source={TrackingUtils.typeToIcon[type].active} />
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

export {TrackingFilter};

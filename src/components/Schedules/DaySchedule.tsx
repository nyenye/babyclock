import React from 'react';
import {StyleSheet} from 'react-native';
import * as I18N from 'react-i18next';
import dayjs from 'dayjs';

import {View} from '../Layout';
import {DayDivider} from './DayDivider';
import {Text} from '../Typography';

import {Colors} from '../../styles';
import {ThemeHook, ReportHook} from '../../hooks';

const styles = StyleSheet.create({
  header: {
    height: 30,
  },
  body: {
    flexGrow: 1,
    flexShrink: 0,
    borderLeftColor: Colors.tealLight,
    borderLeftWidth: 0.25,
    borderRightColor: Colors.tealLight,
    borderRightWidth: 0.25,
  },
});

function getDynamicStyles(theme: ThemeHook.Theme) {
  return StyleSheet.create({
    body: {
      backgroundColor:
        theme === 'light' ? Colors.tealLighter : Colors.tealDarker,
    },
  });
}

const daysOfTheWeek = [
  'SUNDAY',
  'MONDAY',
  'TUESDAY',
  'WEDNESDAY',
  'THURSDAY',
  'FRIDAY',
  'SATURDAY',
];

type DayScheduleProps = {
  dayOfTheWeek: number;
  children?: React.ReactNode;
};

function DaySchedule({dayOfTheWeek = 0, children}: DayScheduleProps) {
  const {t} = I18N.useTranslation('translations');
  const {theme} = ThemeHook.useState();

  const {currentStartOfWeek} = ReportHook.useState();

  function isCurrentDay() {
    if (!dayjs(currentStartOfWeek).isSame(dayjs().startOf('week'))) {
      return false;
    }

    if (dayjs().get('day') !== dayOfTheWeek) {
      return false;
    }

    return true;
  }

  const dynamicStyles = getDynamicStyles(theme);

  return (
    <View direction="column" flex={1}>
      <View style={[styles.header]}>
        <Text
          weight={(isCurrentDay() && 'bold') || 'normal'}
          alignment="center">
          {t(`WEEKDAY_${daysOfTheWeek[dayOfTheWeek]}_SHORT`)}
        </Text>
      </View>
      <View style={[styles.body, dynamicStyles.body]}>
        {children}
        <DayDivider />
      </View>
    </View>
  );
}

export {DaySchedule};

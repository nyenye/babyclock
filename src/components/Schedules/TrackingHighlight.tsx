import React from 'react';
import dayjs from 'dayjs';

import {View} from '../Layout';
import {TrackingType} from '../../types';

import {TrackingUtils} from '../../utils';
import {StyleSheet} from 'react-native';

// max zIndex should be 9;
const zIndexByType = {
  sleeping: 1,
  feeding: 2,
  diaper: 1,
};

function getDynamicStyles(type: TrackingType, y: number, height: number) {
  return StyleSheet.create({
    highlighter: {
      backgroundColor: TrackingUtils.typeToColor[type],
      top: `${y}%`,
      height: `${height}%`,
      zIndex: zIndexByType[type],
    },
  });
}

type TrackingHightlightProps = {
  start: string;
  end: string;
  type: TrackingType;
};

const dayDuration = 60 * 60 * 24 * 1000;

function TrackingHightlight({start, end, type}: TrackingHightlightProps) {
  const startDelta = dayjs(start).diff(
    dayjs(start).startOf('day'),
    'millisecond',
  );

  const delta = dayjs(end).diff(start, 'millisecond');

  const dynamicStyles = getDynamicStyles(
    type,
    (startDelta / dayDuration) * 100,
    (delta / dayDuration) * 100,
  );

  return (
    <View
      fill="fillWidth"
      position="absolute"
      style={dynamicStyles.highlighter}
    />
  );
}

export {TrackingHightlight};

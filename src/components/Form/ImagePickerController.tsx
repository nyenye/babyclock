import React from 'react';
import {
  Image,
  ViewStyle,
  StyleSheet,
  TouchableNativeFeedback,
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import ImagePicker, {ImagePickerResponse} from 'react-native-image-picker';

import * as I18N from 'react-i18next';

import {Button} from '../Button';
import {View, Padding} from '../Layout';
import {Text, TextBnW} from '../Typography';

import {Colors} from '../../styles';

const styles = StyleSheet.create({
  image: {
    height: 100,
    width: 100,
    borderRadius: 8,
  },
  noImageView: {
    height: 100,
    width: 100,
  },
});

interface ImagePickerControllerProps {
  label?: string;
  errorMsg?: string;
  imageUri: string | null;
  onChangePickedImage: (uri: string) => void;
  containerStyle?: ViewStyle;
}

function ImagePickerController({
  label = undefined,
  errorMsg = '',
  imageUri,
  onChangePickedImage,
  containerStyle = undefined,
}: ImagePickerControllerProps) {
  const {t} = I18N.useTranslation('translations');

  function onPickImageButtonPressed() {
    ImagePicker.showImagePicker((response: ImagePickerResponse) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        onChangePickedImage(response.uri);
      }
    });
  }

  return (
    <View direction="row" style={containerStyle}>
      {imageUri === null ? (
        <TouchableNativeFeedback onPress={onPickImageButtonPressed}>
          <View
            main="justifyCenter"
            cross="alignCenter"
            style={styles.noImageView}
            bgHighlight={1}
            rounded>
            <Icon name="user" color={Colors.teal} size={50} />
          </View>
        </TouchableNativeFeedback>
      ) : (
        <TouchableNativeFeedback onPress={onPickImageButtonPressed}>
          <Image source={{uri: imageUri}} style={styles.image} />
        </TouchableNativeFeedback>
      )}

      <Padding sizeH={7.5} />

      <View direction="column" flex={1}>
        {label !== undefined && <TextBnW>{label}</TextBnW>}
        <Text>{errorMsg}</Text>
        <View flex={1} />
        <Button
          size="small"
          title={t('FORM_CONTROLLERS_IMAGE_PICKER_BUTTON_LABEL').toUpperCase()}
          onPress={onPickImageButtonPressed}
        />
      </View>
    </View>
  );
}

export {ImagePickerController};

import React from 'react';
import {StyleSheet, ViewStyle} from 'react-native';

import {Colors} from '../../styles';

import {View, Padding} from '../Layout';
import {Text, TextBnW} from '../Typography';

const styles = StyleSheet.create({
  errorMsg: {
    color: Colors.pink,
  },
  inRow: {
    flex: 1,
  },
});

interface TextInputControllerProps {
  label?: string;
  errorMsg?: string;
  children: React.ReactNode;
  containerStyle?: ViewStyle;
}

function TextInputController({
  label = undefined,
  errorMsg = '',
  children,
  containerStyle = undefined,
}: TextInputControllerProps) {
  return (
    <View direction="column" style={containerStyle}>
      {label !== undefined && <TextBnW>{label}</TextBnW>}
      {label !== undefined && <Padding sizeV={2.5} />}
      {children}
      <Padding sizeV={2.5} />
      <Text size="small" style={styles.errorMsg}>
        {errorMsg}
      </Text>
    </View>
  );
}

function TextInputControllerInRow({
  children,
  ...props
}: TextInputControllerProps) {
  return (
    <TextInputController {...props} containerStyle={styles.inRow}>
      {children}
    </TextInputController>
  );
}

export {TextInputController, TextInputControllerInRow};

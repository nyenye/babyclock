import React from 'react';

import {View, Padding} from '../Layout';
import {Text, TextBnW} from '../Typography';
import {StyleSheet} from 'react-native';
import {Colors} from '../../styles';

const styles = StyleSheet.create({
  errorMsg: {
    color: Colors.pink,
  },
});

interface CheckBoxControllerProps {
  label?: string;
  errorMsg?: string;
  children: React.ReactNode;
}

function CheckBoxController({
  label,
  errorMsg = '',
  children,
}: CheckBoxControllerProps) {
  return (
    <View direction="column">
      {label && <TextBnW>{label}</TextBnW>}
      {label && <Padding sizeV={2.5} />}
      {children}
      <Padding sizeV={2.5} />
      <Text size="small" style={styles.errorMsg}>
        {errorMsg}
      </Text>
    </View>
  );
}

export {CheckBoxController};

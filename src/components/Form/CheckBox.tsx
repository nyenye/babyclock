import React from 'react';
import {
  AccessibilityProps,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';

import {View, Padding} from '../Layout';
import {Text} from '../Typography';
import {ThemeHook} from '../../hooks';
import {Colors} from '../../styles';

const styles = StyleSheet.create({
  outter: {
    padding: 2,
    borderRadius: 1,
  },
  inner: {
    width: 15,
    height: 15,
    borderRadius: 1,
  },
});

function getDynamicStyles(
  theme: ThemeHook.Theme,
  value: boolean,
  disabled: boolean,
) {
  return StyleSheet.create({
    outter: {
      backgroundColor: disabled
        ? Colors.gray
        : value
        ? Colors.checkboxColors[theme].isSelected.outter
        : Colors.checkboxColors[theme].isNotSelected.outter,
    },
    inner: {
      backgroundColor: disabled
        ? Colors.gray
        : value
        ? Colors.checkboxColors[theme].isSelected.inner
        : Colors.checkboxColors[theme].isNotSelected.inner,
    },
  });
}

interface CheckBoxProps extends AccessibilityProps {
  disabled?: boolean;
  label?: string;
  onValueChange: Function;
  value?: boolean;
}

function CheckBox({
  disabled = false,
  label = '',
  onValueChange,
  value = false,
  ...accessibilityProps
}: CheckBoxProps) {
  const {theme} = ThemeHook.useState();

  function onPress() {
    onValueChange(!value);
  }

  const dynamicStyles = getDynamicStyles(theme, value, disabled);

  return (
    <TouchableWithoutFeedback
      disabled={disabled}
      onPress={onPress}
      {...accessibilityProps}>
      <View direction="row" push="pushLeft">
        {disabled && (
          <View style={[styles.outter, dynamicStyles.outter]}>
            <View style={[styles.inner, dynamicStyles.inner]} />
          </View>
        )}
        {(!disabled && value && (
          <View style={[styles.outter, dynamicStyles.outter]}>
            <View style={[styles.inner, dynamicStyles.inner]} />
          </View>
        )) ||
          (!disabled && (
            <View style={[styles.outter, dynamicStyles.outter]}>
              <View style={[styles.inner, dynamicStyles.inner]} />
            </View>
          ))}
        <Padding sizeH={5} />
        <Text>{label}</Text>
      </View>
    </TouchableWithoutFeedback>
  );
}

export {CheckBox};

import React from 'react';
import {TouchableNativeFeedback, ViewStyle} from 'react-native';

import {ThemeHook} from '../../hooks';
import {FormStyles} from '../../styles';

import {View} from '../Layout';
import {Text} from '../Typography';

export interface SelectorProps<T> {
  label: string;
  value: T;
  onSelected: (value: T) => void;
  isSelected: boolean;
  containerStyle?: ViewStyle;
}

function Selector<T = string>({
  label,
  value,
  onSelected,
  isSelected = false,
  containerStyle,
}: SelectorProps<T>) {
  const {theme} = ThemeHook.useState();

  function onSelectorPressed() {
    onSelected(value);
  }

  const borderStyle = [
    FormStyles.selector.common.borderView,
    isSelected
      ? FormStyles.selector[theme].borderViewSelected
      : FormStyles.selector[theme].borderView,
    containerStyle,
  ];

  const touchable = [
    FormStyles.selector.common.touchable,
    FormStyles.selector[theme].touchable,
  ];

  const textStyle = [
    FormStyles.selector.common.text,
    isSelected ? FormStyles.selector[theme].textSelected : undefined,
  ];

  return (
    <View flex={1} padding={2} style={borderStyle} rounded>
      <TouchableNativeFeedback onPress={onSelectorPressed}>
        <View
          flex={1}
          style={touchable}
          main="justifyCenter"
          cross="alignCenter"
          rounded>
          <Text style={textStyle}>{label}</Text>
        </View>
      </TouchableNativeFeedback>
    </View>
  );
}

export {Selector};

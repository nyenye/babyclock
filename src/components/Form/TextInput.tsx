import React from 'react';
import {TextInput, TextInputProps, ViewStyle} from 'react-native';

import {View} from '../Layout';
import {ThemeHook, ToggleHook} from '../../hooks';
import {FormStyles} from '../../styles';

interface CustomTextInputProps extends TextInputProps {
  containerStyle?: ViewStyle;
  forwardRef?: React.MutableRefObject<TextInput | undefined> | undefined;
  isRequired?: boolean;
  hasError?: boolean;
  bg?: 'bg0' | 'bg1' | 'bg2';
}

function CustomTextInput({
  forwardRef = undefined,
  isRequired = false,
  hasError = false,
  bg = 'bg1',
  containerStyle,
  style,
  ...props
}: CustomTextInputProps) {
  const {theme} = ThemeHook.useState();

  const [isFocused, toggleFocus] = ToggleHook.useToggle();

  const containerStyles = [
    FormStyles.textInput.common.container,
    hasError
      ? FormStyles.textInput.common.containerError
      : isFocused
      ? FormStyles.textInput.common.containerFocused
      : FormStyles.textInput[theme][bg].container,
    containerStyle,
  ];

  const inputStyles = [
    FormStyles.textInput.common.input,
    FormStyles.textInput[theme][bg].input,
    style,
  ];

  return (
    <View style={containerStyles} rounded>
      <TextInput
        ref={(input) => {
          if (input !== null && forwardRef) {
            forwardRef.current = input;
          }
        }}
        style={inputStyles}
        onFocus={toggleFocus}
        onBlur={toggleFocus}
        {...props}
      />

      {isRequired && (
        <View
          style={FormStyles.textInput.common.isRequiredMark}
          position="absolute"
        />
      )}
    </View>
  );
}

interface TextAreaProps extends CustomTextInputProps {
  rows?: 'two' | 'three' | 'four' | 'five' | 'fullHeight';
}

function TextArea({rows = 'two', ...props}: TextAreaProps) {
  return (
    <CustomTextInput
      {...props}
      multiline={true}
      style={FormStyles.textArea[rows]}
      textAlignVertical="top"
    />
  );
}

export {CustomTextInput as TextInput, TextArea};

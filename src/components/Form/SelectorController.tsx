import React from 'react';
import {StyleSheet, ViewStyle} from 'react-native';

import {Colors} from '../../styles';

import {View, Padding} from '../Layout';
import {Text, TextBnW} from '../Typography';

import {Selector, SelectorProps} from './Selector';

const styles = StyleSheet.create({
  selectorsWrapper: {
    height: 40,
  },
  notLastSelector: {
    marginRight: 10,
  },
  errorMsg: {
    color: Colors.pink,
  },
});

export interface SelectorControllerProps<T> {
  label?: string;
  errorMsg?: string;
  selectors: Array<{label: string; value: T}>;
  selected: T | null;
  onChangeSelected: (value: T) => void;
  containerStyle?: ViewStyle;
}

function SelectorController<T = string>({
  label = undefined,
  errorMsg = '',
  selectors,
  selected,
  onChangeSelected,
}: SelectorControllerProps<T>) {
  const SelectorRef = React.useRef((props: SelectorProps<T>) =>
    Selector<T>(props),
  );

  return (
    <View>
      {label !== undefined && <TextBnW>{label}</TextBnW>}
      {label !== undefined && <Padding sizeV={2.5} />}
      <View direction="row" fill="fillWidth" style={styles.selectorsWrapper}>
        {selectors.map((selector: {label: string; value: T}, index: number) => (
          <SelectorRef.current
            key={selector.label}
            label={selector.label}
            value={selector.value}
            onSelected={onChangeSelected}
            isSelected={selector.value === selected}
            containerStyle={
              index < selectors.length - 1 ? styles.notLastSelector : {}
            }
          />
        ))}
      </View>
      <Padding sizeV={2.5} />
      <Text size="small" style={styles.errorMsg}>
        {errorMsg}
      </Text>
    </View>
  );
}

export {SelectorController};

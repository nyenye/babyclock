import React from 'react';
import {StyleSheet} from 'react-native';

import {Colors} from '../../styles';

import {View, Padding} from '../Layout';
import {Text, TextBnW} from '../Typography';

const styles = StyleSheet.create({
  errorMsg: {
    color: Colors.pink,
  },
  display: {
    height: 40,
  },
  disabledDisplay: {
    opacity: 0.25,
  },
});

interface PickerControllerProps {
  label?: string;
  errorMsg?: string;
  isRequired?: boolean;
  disabled?: boolean;
  children: React.ReactNode;
}

function PickerController({
  label = undefined,
  errorMsg = '',
  children,
}: PickerControllerProps) {
  return (
    <View direction="column">
      {label !== undefined && <TextBnW>{label}</TextBnW>}
      {label !== undefined && <Padding sizeV={2.5} />}

      <View bgHighlight={1} rounded>
        {children}
      </View>

      <Padding sizeV={2.5} />
      <Text size="small" style={styles.errorMsg}>
        {errorMsg}
      </Text>
    </View>
  );
}

export {PickerController};

import React from 'react';
import {StyleSheet, ViewStyle, TouchableNativeFeedback} from 'react-native';
import DateTimePicker, {
  AndroidNativeProps,
} from '@react-native-community/datetimepicker';
import * as I18N from 'react-i18next';
import dayjs from 'dayjs';

import {ToggleHook} from '../../hooks';
import {Colors, FormStyles} from '../../styles';

import {View, Padding} from '../Layout';
import {Text, TextBnW} from '../Typography';
import {TrackingUtils} from '../../utils';

const styles = StyleSheet.create({
  errorMsg: {
    color: Colors.pink,
  },
  display: {
    height: 40,
  },
  disabledDisplay: {
    opacity: 0.25,
  },
});

interface DateTimePickerControllerProps extends AndroidNativeProps {
  label?: string;
  isNull?: boolean;
  errorMsg?: string;
  isRequired?: boolean;
  disabled?: boolean;
  controllerMode?: 'date' | 'time' | 'datetime';
  onChangeDate: (date: Date) => void;
  containerStyle?: ViewStyle;
  bgHighlight?: 0 | 1 | 2;
}

function DateTimePickerController({
  label = undefined,
  errorMsg = '',
  value,
  isNull = false,
  onChangeDate,
  controllerMode = 'date',
  is24Hour = false,
  minimumDate,
  maximumDate,
  isRequired = false,
  disabled = false,
  containerStyle = undefined,
  bgHighlight = 1,
}: DateTimePickerControllerProps) {
  const {t} = I18N.useTranslation('translations');
  const [showPicker, toggleShowPicker] = ToggleHook.useToggle(false);

  const [mode, setMode] = React.useState<'time' | 'date'>(
    controllerMode === 'time' ? 'time' : 'date',
  );

  const datetime = React.useRef(new Date());

  function onDateTimePickerChange(event: any, date?: Date | undefined) {
    if (date === undefined) {
      if (controllerMode === 'datetime') {
        setMode('date');
      }
      return toggleShowPicker();
    }

    if (controllerMode === 'datetime') {
      if (mode === 'date') {
        datetime.current = date;
        setMode('time');
      } else {
        toggleShowPicker();

        datetime.current.setHours(date.getHours(), date.getMinutes());

        onChangeDate(datetime.current);
        setMode('date');
      }
    } else {
      toggleShowPicker();
      const newDate = date;
      onChangeDate(newDate);
    }
  }

  const isToday = TrackingUtils.isToday(value);

  const displayDate = isToday
    ? t('COMMON_TODAY')
    : dayjs(value).format(t('DATE_LONG_FORMAT'));
  const displayTime = dayjs(value).format('HH:mm');

  const displayText =
    controllerMode === 'datetime'
      ? `${displayDate} ${t('COMMON_AT')} ${displayTime}`
      : controllerMode === 'date'
      ? displayDate
      : displayTime;

  return (
    <View direction="column" style={containerStyle}>
      {label !== undefined && <TextBnW>{label}</TextBnW>}
      {label !== undefined && <Padding sizeV={2.5} />}

      <TouchableNativeFeedback disabled={disabled} onPress={toggleShowPicker}>
        <View
          main="justifyCenter"
          cross="alignCenter"
          bgHighlight={bgHighlight}
          rounded
          style={[styles.display, disabled && styles.disabledDisplay]}>
          {isNull ? <Text>-</Text> : <Text>{displayText}</Text>}

          {isRequired && (
            <View
              style={FormStyles.textInput.common.isRequiredMark}
              position="absolute"
            />
          )}
        </View>
      </TouchableNativeFeedback>

      <Padding sizeV={2.5} />
      <Text size="small" style={styles.errorMsg}>
        {errorMsg}
      </Text>

      {showPicker && (
        <DateTimePicker
          value={value}
          onChange={onDateTimePickerChange}
          mode={mode}
          is24Hour={is24Hour}
          minimumDate={minimumDate}
          maximumDate={maximumDate}
        />
      )}
    </View>
  );
}

export {DateTimePickerController};

import React from 'react';
import {Image, StyleSheet, ViewStyle} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import * as I18N from 'react-i18next';
import dayjs from 'dayjs';

import {Button} from '../Button';
import {View, Padding} from '../Layout';
import {Text, Heading} from '../Typography';

import {BabiesHook} from '../../hooks';
import {Colors} from '../../styles';
import {Baby} from '../../types';

const styles = StyleSheet.create({
  notLastItem: {
    marginBottom: 5,
  },
  noImageView: {
    height: 100,
    width: 100,
  },
  textPlaceholder: {
    height: 20,
  },
  longTextPlaceholder: {
    width: '80%',
  },
  shortTextPlaceholder: {
    width: '57%',
  },
});

interface ListBabiesProps {
  onAddBabyButtonPressed: () => void;
  onEditButtonPressed: (baby: Baby) => void;
}

function ListBabies({
  onAddBabyButtonPressed,
  onEditButtonPressed,
}: ListBabiesProps) {
  const {t} = I18N.useTranslation('translations');

  const {babies} = BabiesHook.useState();

  return (
    <>
      <View direction="row" main="spaceBetween" cross="alignCenter">
        <Heading type="h3">{t('FAMILY_LIST_BABIES_LABEL')}</Heading>

        <Button
          size="small"
          title={t('FAMILY_BABIES_ADD_BABY_BUTTON')}
          onPress={onAddBabyButtonPressed}
        />
      </View>

      <Padding sizeV={7.5} />

      {babies.length ? (
        babies.map((baby: Baby, index: number) => (
          <ListBabiesItem
            key={baby.id}
            baby={baby}
            onEditButtonPressed={onEditButtonPressed}
            containerStyles={
              index < babies.length - 1 ? styles.notLastItem : undefined
            }
          />
        ))
      ) : (
        <View direction="row" bgHighlight={1} padding={15} rounded>
          <View
            main="justifyCenter"
            cross="alignCenter"
            style={styles.noImageView}
            bgHighlight={0}
            rounded>
            <Icon name="user" color={Colors.teal} size={50} />
          </View>

          <Padding sizeH={7.5} />

          <View direction="column" flex={1}>
            <View
              rounded
              bgHighlight={2}
              style={[styles.textPlaceholder, styles.longTextPlaceholder]}
            />
            <Padding sizeV={5} />
            <View
              rounded
              bgHighlight={2}
              style={[styles.textPlaceholder, styles.shortTextPlaceholder]}
            />
            <Padding sizeV={2.5} />
            <View
              rounded
              bgHighlight={2}
              style={[styles.textPlaceholder, styles.shortTextPlaceholder]}
            />
          </View>
        </View>
      )}
    </>
  );
}

const itemStyles = StyleSheet.create({
  image: {
    height: 100,
    width: 100,
    borderRadius: 8,
  },
});

interface ListBabiesItemProps {
  baby: Baby;
  onEditButtonPressed: (baby: Baby) => void;
  containerStyles?: ViewStyle;
}

function ListBabiesItem({
  baby,
  onEditButtonPressed,
  containerStyles,
}: ListBabiesItemProps) {
  const {t} = I18N.useTranslation('translations');

  const birthDateFormatted = dayjs(baby.birthDate as Date).format(
    t('DATE_LONG_FORMAT'),
  );

  function onEdit() {
    onEditButtonPressed(baby);
  }

  React.useEffect(() => {
    if (baby.pictureFilename !== undefined && baby.pictureUri === undefined) {
      // TODO: Request pictureUri.
    }
  }, [baby, baby.pictureFilename, baby.pictureUri]);

  return (
    <View
      direction="row"
      bgHighlight={1}
      padding={15}
      rounded
      style={containerStyles}>
      {baby.pictureUri !== undefined ? (
        <Image
          source={{uri: baby.pictureUri}}
          resizeMode="cover"
          style={itemStyles.image}
        />
      ) : (
        <View
          main="justifyCenter"
          cross="alignCenter"
          style={styles.noImageView}
          bgHighlight={0}
          rounded>
          <Icon name="child-care" color={Colors.teal} size={50} />
        </View>
      )}

      <Padding sizeV={7.5} />

      <View direction="column" flex={1}>
        <Text size="large" weight="bold">
          {baby.name}
        </Text>
        <Text>{t(`GENDER_${baby.gender.toUpperCase()}`)}</Text>
        <Text>{birthDateFormatted}</Text>

        <View flex={1} />

        <Button
          size="small"
          title={t('FAMILY_BABIES_EDIT_BABY_BUTTON')}
          onPress={onEdit}
        />
      </View>
    </View>
  );
}

export {ListBabies};

import React from 'react';
import {
  ActivityIndicator,
  TouchableNativeFeedback,
  TouchableNativeFeedbackProps,
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';

import {ButtonStyles, Colors} from '../../styles';

import {Padding, View} from '../Layout';
import {Text} from '../Typography';

type ButtonType = 'outlined' | 'primary';

type ButtonSize = 'small' | 'medium' | 'large' | 'xl';

type ButtonColor =
  | 'white'
  | 'teal'
  | 'green'
  | 'orange'
  | 'pink'
  | 'gray'
  | 'grayLight'
  | 'grayDark';

interface ButtonProps extends TouchableNativeFeedbackProps {
  title?: string;
  icon?: string;
  type?: ButtonType;
  flex?: number;
  color?: ButtonColor;
  size?: ButtonSize;
  rounded?: boolean;
  circle?: boolean;
  loading?: boolean;
}

function Button({
  title,
  icon,
  type = 'primary',
  flex = 0,
  color = 'teal',
  size = 'medium',
  rounded = false,
  circle = false,
  disabled = false,
  loading = false,
  onPress,
  ...accessibilityProps
}: ButtonProps) {
  const outterContainerStyles = [
    ButtonStyles.common.outterContainer,
    ButtonStyles[type][color].container,
    ButtonStyles.size[size].container,
    rounded && ButtonStyles.common.rounded,
    circle && ButtonStyles.circle[size],
    disabled && ButtonStyles.common.disabled,
  ];

  const containerStyles = [
    ButtonStyles.common.container,
    ButtonStyles.size[size].container,
    rounded && ButtonStyles.common.rounded,
    circle ? ButtonStyles.circle[size] : ButtonStyles.padding[size],
  ];

  const textStyles = [
    ButtonStyles.common.text,
    ButtonStyles.size[size].text,
    ButtonStyles[type][color].text,
  ];

  const iconStyles = [...textStyles, ButtonStyles.size[size].icon];

  return (
    <View style={outterContainerStyles} flex={flex}>
      <TouchableNativeFeedback
        onPress={onPress}
        disabled={disabled || loading}
        {...accessibilityProps}
        testID="touchable">
        <View direction="row" style={containerStyles} flex={1}>
          {loading ? (
            <ActivityIndicator
              size="small"
              color={type === 'primary' ? Colors.white : Colors[color]}
              testID="activity-indicator"
            />
          ) : (
            <>
              {icon && <Icon name={icon} style={iconStyles} />}
              {icon && title && <Padding sizeH={15} />}
              {title && <Text style={textStyles}>{title}</Text>}
            </>
          )}
        </View>
      </TouchableNativeFeedback>
    </View>
  );
}

export {Button};

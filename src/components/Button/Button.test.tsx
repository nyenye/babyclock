import React from 'react';
import 'react-native';
// @ts-ignore
import {render, fireEvent} from 'test-utils';

import {Button} from './Button';

describe('Button renders correctly', () => {
  it('renders without title', () => {
    render(<Button />);
  });

  it('renders with title', () => {
    const title = 'Title';

    const {getByText} = render(<Button title={title} />);

    const text = getByText(title);
    expect(text).toBeTruthy();
  });

  it('renders ActivityIndicator when loading', () => {
    const title = 'Title';

    const {queryByText, getByTestId} = render(<Button title={title} loading />);

    expect(queryByText(title)).toBeNull();

    const activityIndicator = getByTestId('activity-indicator');
    expect(activityIndicator).toBeTruthy();
  });

  it('renders with background color', () => {
    // TODO: Test if button changes color when passing color prop.
  });
});

describe('Button acts correctly', () => {
  it('calls onPress callback prop when Button is pressed', () => {
    const onPress = jest.fn();

    const {getByTestId} = render(<Button onPress={onPress} />);

    const touchable = getByTestId('touchable');
    fireEvent.press(touchable);
    expect(onPress).toHaveBeenCalledTimes(1);
  });

  it("doesn't call onPress callback prop when Button is disabled", () => {
    const onPress = jest.fn();

    const {getByTestId} = render(<Button onPress={onPress} disabled />);

    const touchable = getByTestId('touchable');
    fireEvent.press(touchable);
    expect(onPress).toHaveBeenCalledTimes(0);
  });

  it("doesn't call onPress callback prop when Button is loading", () => {
    const onPress = jest.fn();

    const {getByTestId} = render(<Button onPress={onPress} loading />);

    const touchable = getByTestId('touchable');
    fireEvent.press(touchable);
    expect(onPress).toHaveBeenCalledTimes(0);
  });
});

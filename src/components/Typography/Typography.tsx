import React from 'react';

import {Text, TextProps} from 'react-native';
import {ThemeHook} from '../../hooks';
import {Typography} from '../../styles';

export type ColorProp =
  | 'default'
  | 'white'
  | 'black'
  | 'tealLighter'
  | 'teal'
  | 'tealDarker'
  | 'green'
  | 'orange'
  | 'pink'
  | 'red';

interface CustomTextProps extends TextProps {
  size?: 'small' | 'medium' | 'large' | 'xl';
  weight?: 'normal' | 'bold';
  alignment?: 'center' | 'left' | 'right' | 'justify';
  color?: ColorProp;
  children: React.ReactNode;
}

function CustomText({
  size = 'medium',
  weight = 'normal',
  alignment = 'left',
  color = 'default',
  style,
  children,
  ...props
}: CustomTextProps) {
  const {theme} = ThemeHook.useState();
  const themedColors = Typography.getThemedTypographyColors(theme);

  const styles = React.useMemo(
    () => [
      Typography.size[size],
      Typography.weight[weight],
      Typography.alignment[alignment],
      themedColors.text,
      color !== 'default' && Typography.color[color],
      style,
    ],
    [size, weight, alignment, color, themedColors, style],
  );

  return (
    <Text style={styles} {...props}>
      {children}
    </Text>
  );
}

function TextBnW(props: CustomTextProps) {
  const {theme} = ThemeHook.useState();

  return (
    <CustomText
      {...props}
      color="default"
      style={Typography.color[theme === 'light' ? 'black' : 'white']}
    />
  );
}

interface HeadingProps extends TextProps {
  type?: 'h1' | 'h2' | 'h3' | 'h4';
  alignment?: 'center' | 'left' | 'right' | 'justify';
  color?: ColorProp;
  children: React.ReactNode;
}

function Heading({
  type = 'h1',
  alignment = 'left',
  color = 'default',
  style,
  children,
  ...props
}: HeadingProps) {
  const {theme} = ThemeHook.useState();
  const themedColors = Typography.getThemedTypographyColors(theme);

  const styles = React.useMemo(
    () => [
      Typography.headings[type],
      themedColors.text,
      color !== 'default' && Typography.color[color],
      Typography.alignment[alignment],
      style,
    ],
    [type, alignment, themedColors, color, style],
  );

  return (
    <Text style={styles} {...props}>
      {children}
    </Text>
  );
}

function HeadingBnW(props: HeadingProps) {
  const {theme} = ThemeHook.useState();

  return (
    <Heading
      {...props}
      color="default"
      style={Typography.color[theme === 'light' ? 'black' : 'white']}
    />
  );
}

export {CustomText as Text, TextBnW, Heading, HeadingBnW};

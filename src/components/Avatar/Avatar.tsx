import React from 'react';
import {Image, StyleSheet} from 'react-native';

import {View} from '../Layout';

const styles = StyleSheet.create({
  container: {
    height: 100,
    width: 100,
    borderRadius: 50,
    padding: 4,
  },
  img: {
    height: 92,
    width: 92,
    borderRadius: 46,
  },
});

interface AvatarProps {
  imageUri: string;
}

function Avatar({imageUri}: AvatarProps) {
  return (
    <View bgHighlight={1} style={styles.container}>
      <Image source={{uri: imageUri}} style={styles.img} />
    </View>
  );
}

export {Avatar};

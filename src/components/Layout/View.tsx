import React from 'react';
import {View, ViewProps} from 'react-native';

import {Layouts, Colors} from '../../styles';
import {ThemeHook} from '../../hooks';

interface CustomViewProps extends ViewProps {
  direction?: 'row' | 'column';
  main?:
    | 'justifyStart'
    | 'justifyCenter'
    | 'justifyEnd'
    | 'spaceBetween'
    | 'spaceEvenly';
  cross?: 'alignStart' | 'alignCenter' | 'alignEnd' | 'alignStretch';
  fill?: 'noFill' | 'fillWidth' | 'fillHeight' | 'fillBoth';
  position?: 'absolute' | 'relative';
  push?: 'noPush' | 'pushRight' | 'pushLeft' | 'pushTop' | 'pushBottom';
  padding?: number;
  flex?: number;
  bgHighlight?: -1 | 0 | 1 | 2;
  rounded?: boolean;
  children?: React.ReactNode;
}

function CustomView({
  direction = 'column',
  main = 'justifyStart',
  cross = 'alignStretch',
  fill = 'noFill',
  position = 'relative',
  push = 'noPush',
  flex = 0,
  padding = 0,
  bgHighlight = -1,
  rounded = false,
  children,
  style,
  ...props
}: CustomViewProps) {
  const {theme} = ThemeHook.useState();

  const styles = React.useMemo(() => {
    return [
      Layouts[cross],
      Layouts[direction],
      Layouts[fill],
      Layouts[main],
      Layouts[position],
      Layouts[push],
      {
        borderRadius: rounded ? 8 : 0,
        padding,
        flex,
        backgroundColor:
          bgHighlight !== -1
            ? Colors.backgroundColors.highlights[theme][bgHighlight]
            : Colors.transparent,
      },
      style,
    ];
  }, [
    bgHighlight,
    cross,
    direction,
    fill,
    flex,
    main,
    padding,
    position,
    push,
    rounded,
    style,
    theme,
  ]);

  return (
    <View style={styles} {...props}>
      {children}
    </View>
  );
}

export {CustomView as View};

import React from 'react';
import {StyleSheet} from 'react-native';

import {View} from './View';

type Direction = 'vertical' | 'horizontal';

function getDynamicStyles(direction: Direction, size: number, margin: number) {
  return StyleSheet.create({
    divider: {
      width: direction === 'vertical' ? size : '100%',
      height: direction === 'horizontal' ? size : '100%',
      marginHorizontal: direction === 'vertical' ? margin : undefined,
      marginVertical: direction === 'horizontal' ? margin : undefined,
    },
  });
}

type DividerProps = {
  highlight?: 0 | 1 | 2;
  direction?: Direction;
  size?: number;
  margin?: number;
};

function Divider({
  highlight = 0,
  direction = 'horizontal',
  size = 1,
  margin = 0,
}: DividerProps) {
  const dynamicStyles = getDynamicStyles(direction, size, margin);

  return <View style={dynamicStyles.divider} bgHighlight={highlight} />;
}

export {Divider};

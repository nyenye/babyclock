import React from 'react';
import {View, StyleSheet} from 'react-native';

function getDynamicStyles(
  size: number,
  sizeV: number = size,
  sizeH: number = size,
) {
  return StyleSheet.create({
    container: {
      paddingHorizontal: sizeH,
      paddingVertical: sizeV,
    },
  });
}

type PaddingProps = {
  size?: number;
  sizeV?: number;
  sizeH?: number;
  children?: React.ReactNode;
};

function Padding({size = 0, sizeV, sizeH, children}: PaddingProps) {
  const dynamicStyles = getDynamicStyles(size, sizeV, sizeH);
  return <View style={dynamicStyles.container}>{children}</View>;
}

export {Padding};

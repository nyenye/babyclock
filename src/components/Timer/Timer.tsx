import React from 'react';
import useInterval from 'use-interval';

import {TrackingUtils} from '../../utils';

import {ColorProp, Text} from '../Typography';

interface TimerProps {
  start: Date;
  initialValue?: number;
  isRunning?: boolean;
  format?: 'h:m:s' | 'm:s';
  textFormat?: {
    color?: ColorProp;
    size?: 'small' | 'medium' | 'large' | 'xl';
    weight?: 'bold' | 'normal';
  };
}

function Timer({
  start,
  initialValue = 0,
  isRunning = true,
  format = 'h:m:s',
  textFormat = {},
}: TimerProps) {
  let [milliseconds, setMilliseconds] = React.useState<number>(
    Math.max(initialValue + Date.now().valueOf() - start.valueOf(), 0),
  );

  useInterval(
    () => {
      setMilliseconds(milliseconds + 1000);
    },
    isRunning ? 1000 : null,
  );

  React.useEffect(() => {
    setMilliseconds(
      Math.max(initialValue + Date.now().valueOf() - start.valueOf(), 0),
    );
  }, [initialValue, start]);

  return (
    <Text
      alignment="center"
      size={textFormat.size || 'medium'}
      color={textFormat.color || 'default'}
      weight={textFormat.weight || 'normal'}>
      {format === 'h:m:s'
        ? TrackingUtils.durationToHHmmss(milliseconds)
        : TrackingUtils.durationTommss(milliseconds)}
    </Text>
  );
}

export {Timer};

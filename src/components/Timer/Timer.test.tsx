import React from 'react';
import 'react-native';
// @ts-ignore
import {render} from 'test-utils';

import {Timer} from './Timer';

it('renders correctly', () => {
  render(<Timer start={new Date()} isRunning={false} />);
});

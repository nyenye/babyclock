import React from 'react';
import {Dimensions, StyleSheet, Image} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';

import * as I18N from 'react-i18next';

import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

import {TrackingsStackParamList} from '../../../navigation/Stacks';
import {FinishedTracking, OngoingTracking, TrackingType} from '../../../types';
import {TrackingUtils} from '../../../utils';

import {Button} from '../../Button';
import {View, Padding, Divider} from '../../Layout';
import {Text, TextBnW} from '../../Typography';
import {Timer} from '../../Timer';

dayjs.extend(relativeTime);

const {width} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    width: width,
    zIndex: 1,
    marginTop: -40,
    overflow: 'visible',
  },
  scrollContainer: {
    flexDirection: 'row',
    paddingVertical: 10,
  },
  card: {
    width: (width - 15 * 4) / 3,
    zIndex: 10,
    borderRadius: 10,
    elevation: 5,
  },
  icon: {
    zIndex: 30,
  },
  iconShadow: {
    zIndex: 20,
    position: 'absolute',
    width: 35,
    height: 35,
    borderRadius: 35,
    top: '50%',
    left: '25%',
    opacity: 0.2,
  },
});

function getDynamicStyles(trackingType: TrackingType) {
  return StyleSheet.create({
    shadowColor: {
      backgroundColor: TrackingUtils.typeToColor[trackingType],
    },
  });
}

interface TrackingCardProps {
  navigation: StackNavigationProp<TrackingsStackParamList, 'TRACKINGS'>;
  trackingType: TrackingType;
  title: string;
  lastTrack: FinishedTracking | undefined;
  ongoingTrack: OngoingTracking | undefined;
}

function TrackingCard({
  navigation,
  trackingType,
  title,
  lastTrack,
  ongoingTrack,
}: TrackingCardProps) {
  const {t} = I18N.useTranslation('translations');

  const dynamicStyles = getDynamicStyles(trackingType);

  function goToTrackings() {
    navigation.navigate('TRACKINGS_DETAILS', {initialTracking: trackingType});
  }

  if (ongoingTrack !== undefined) {
    return (
      <View
        direction="column"
        style={styles.card}
        bgHighlight={0}
        cross="alignCenter">
        <View padding={5} cross="alignCenter">
          <Padding sizeV={5} />
          <View style={[styles.iconShadow, dynamicStyles.shadowColor]} />
          <Image
            style={[styles.icon]}
            source={TrackingUtils.typeToIcon[trackingType].active}
            resizeMode="contain"
          />
        </View>
        <View padding={10} cross="alignCenter" flex={1}>
          <TextBnW size="large">{title}</TextBnW>
          <Timer start={ongoingTrack.start} />
        </View>

        <Divider highlight={1} />

        <View padding={10} main="justifyCenter" cross="alignCenter">
          <Button
            size="small"
            icon="square"
            circle={true}
            color={TrackingUtils.typeToColorName[trackingType]}
            onPress={goToTrackings}
          />
        </View>
      </View>
    );
  }

  const displayMessage =
    lastTrack !== undefined
      ? dayjs(lastTrack.end).from(new Date())
      : t('TRACKINGS_START_TRACKING');

  return (
    <View
      direction="column"
      style={styles.card}
      bgHighlight={0}
      cross="alignCenter">
      <View padding={5} cross="alignCenter">
        <Padding sizeV={5} />
        <View style={[styles.iconShadow, dynamicStyles.shadowColor]} />
        <Image
          style={[styles.icon]}
          source={TrackingUtils.typeToIcon[trackingType].active}
          resizeMode="contain"
        />
      </View>
      <View padding={10} cross="alignCenter" flex={1}>
        <TextBnW size="large">{title}</TextBnW>
        <Text alignment="center" size="small">
          {displayMessage}
        </Text>
      </View>

      <Divider highlight={1} />

      <View padding={10} main="justifyCenter" cross="alignCenter">
        <Button
          size="small"
          icon="plus"
          circle={true}
          color={TrackingUtils.typeToColorName[trackingType]}
          onPress={goToTrackings}
        />
      </View>
    </View>
  );
}

interface TrackingCardsProps {
  navigation: StackNavigationProp<TrackingsStackParamList, 'TRACKINGS'>;
  lastSleep: FinishedTracking | undefined;
  ongoingSleep: OngoingTracking | undefined;
  lastFeed: FinishedTracking | undefined;
  ongoingFeed: OngoingTracking | undefined;
  lastDiaper: FinishedTracking | undefined;
}

function TrackingCards({
  navigation,
  lastSleep,
  ongoingSleep,
  lastFeed,
  ongoingFeed,
  lastDiaper,
}: TrackingCardsProps) {
  const {t} = I18N.useTranslation('translations');

  return (
    <View style={styles.container} direction="row" main="spaceEvenly">
      <TrackingCard
        navigation={navigation}
        trackingType="feeding"
        title={t('TRACKINGS_TYPE_FEEDING')}
        lastTrack={lastFeed}
        ongoingTrack={ongoingFeed}
      />
      <TrackingCard
        navigation={navigation}
        trackingType="sleeping"
        title={t('TRACKINGS_TYPE_SLEEPING')}
        lastTrack={lastSleep}
        ongoingTrack={ongoingSleep}
      />
      <TrackingCard
        navigation={navigation}
        trackingType="diaper"
        title={t('TRACKINGS_TYPE_DIAPER')}
        lastTrack={lastDiaper}
        ongoingTrack={undefined}
      />
    </View>
  );
}

export {TrackingCards};

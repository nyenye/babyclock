import React from 'react';
import {Image, StyleSheet, TouchableNativeFeedback} from 'react-native';

import * as I18N from 'react-i18next';

import {DiaperMode} from '../../../../types';
import {TrackingUtils} from '../../../../utils';

import {View, Padding} from '../../../Layout';
import {Text, TextBnW} from '../../../Typography';
import {Colors} from '../../../../styles';

const possibleModes: DiaperMode[] = ['pee', 'poop'];

const styles = StyleSheet.create({
  scrollView: {
    flex: 0,
    flexDirection: 'row',
  },
  optionContainer: {
    width: 65,
  },
  optionIcon: {
    width: 50,
    height: 50,
  },
  optionShadow: {
    position: 'absolute',
    top: 10,
    backgroundColor: Colors.pink,
    opacity: 0.2,
    width: 50,
    height: 25,
    borderRadius: 15,
  },
});

interface DiaperModeOptionProps {
  mode: DiaperMode;
  isSelected: boolean;
  onChange: (mode: DiaperMode) => void;
}

function DiaperModeOption({mode, isSelected, onChange}: DiaperModeOptionProps) {
  const {t} = I18N.useTranslation('translations');

  function onPress() {
    onChange(mode);
  }

  return (
    <TouchableNativeFeedback onPress={onPress}>
      <View
        direction="column"
        cross="alignCenter"
        style={styles.optionContainer}>
        <View>
          {isSelected && <View style={styles.optionShadow} />}
          <Image
            source={TrackingUtils.diaperModeToIcon[mode]}
            style={styles.optionIcon}
            resizeMode="contain"
          />
        </View>
        <Text
          size="small"
          alignment="center"
          color={isSelected ? 'pink' : 'default'}>
          {t(`TRACKINGS_DIAPER_MODE_${mode.toUpperCase()}`)}
        </Text>
      </View>
    </TouchableNativeFeedback>
  );
}

interface DiaperModeSelectorProps {
  selectedMode: DiaperMode;
  onChange: (mode: DiaperMode) => void;
}

function DiaperModeSelector({selectedMode, onChange}: DiaperModeSelectorProps) {
  const {t} = I18N.useTranslation('translations');

  return (
    <View>
      <TextBnW>{t('FORM_LABEL_TRACKING_DIAPER_MODE')}</TextBnW>

      <Padding sizeV={2.5} />

      <View direction="row" main="spaceEvenly">
        {possibleModes.map((mode) => {
          return (
            <DiaperModeOption
              key={mode}
              mode={mode}
              isSelected={mode === selectedMode}
              onChange={onChange}
            />
          );
        })}
      </View>
      <Padding size={7.5} />
    </View>
  );
}

export {DiaperModeSelector};

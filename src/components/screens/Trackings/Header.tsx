import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';

import GradientView from 'react-native-linear-gradient';

import * as I18N from 'react-i18next';

import {Colors} from '../../../styles';
import {Baby} from '../../../types';

import {Avatar} from '../../Avatar';
import {View, Padding} from '../../Layout';
import {Text, Heading} from '../../Typography';
import {ThemeHook} from '../../../hooks';

const {height} = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.teal,
    minHeight: height / 3,
    padding: 15,
    paddingTop: 30,
    paddingBottom: 15 + 40,
    alignItems: 'center',
  },
  lightBg: {
    backgroundColor: Colors.teal,
  },
  darkBg: {
    backgroundColor: Colors.tealDark,
  },
  coloredText: {
    color: '#3C7B8A',
  },
  settings: {
    position: 'absolute',
    right: 15,
    top: 15,
    opacity: 0.95,
  },
});

const gradients = {
  dark: [Colors.tealDark, '#589CAC', '#83BECB'],
  light: [Colors.teal, '#83BECB', '#589CAC'],
};

interface HeaderProps {
  selectedBaby: Baby;
}

function Header({selectedBaby}: HeaderProps) {
  const {t} = I18N.useTranslation('translations');
  const {theme} = ThemeHook.useState();

  return (
    <GradientView
      style={[
        styles.container,
        theme === 'light' ? styles.lightBg : styles.darkBg,
      ]}
      start={{x: 0.5, y: 0}}
      end={{x: 0.5, y: 1}}
      colors={gradients[theme]}>
      <Avatar imageUri={selectedBaby.pictureUri} />

      <Padding sizeV={7.5} />

      <View direction="row" cross="alignEnd" fill="fillWidth">
        <View direction="column" cross="alignCenter" flex={1}>
          <Text color="white" size="large" weight="bold">
            {selectedBaby.weight ? `${selectedBaby.weight} kg` : '-'}
          </Text>
          <Text style={styles.coloredText}>
            {t('COMMON_WEIGHT').toLowerCase()}
          </Text>
        </View>
        <View direction="column" cross="alignCenter" flex={1.5}>
          <Heading color="white">{selectedBaby.name}</Heading>
          <Text style={styles.coloredText}>3 months 2 weeks</Text>
        </View>
        <View direction="column" cross="alignCenter" flex={1}>
          <Text color="white" size="large" weight="bold">
            {selectedBaby.length ? `${selectedBaby.length} kg` : '-'}
          </Text>
          <Text style={styles.coloredText}>
            {t('COMMON_LENGTH').toLowerCase()}
          </Text>
        </View>
      </View>
    </GradientView>
  );
}

export {Header};

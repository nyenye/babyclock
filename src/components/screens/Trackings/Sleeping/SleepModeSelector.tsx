import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  TouchableNativeFeedback,
} from 'react-native';

import * as I18N from 'react-i18next';

import {SleepingMode} from '../../../../types';
import {TrackingUtils} from '../../../../utils';

import {View, Padding} from '../../../Layout';
import {Text, TextBnW} from '../../../Typography';
import {Colors} from '../../../../styles';

const possibleModes: SleepingMode[] = [
  'crib',
  'stroller',
  'held',
  'nursing',
  'car',
];

const styles = StyleSheet.create({
  scrollView: {
    flex: 0,
    flexDirection: 'row',
  },
  optionContainer: {
    width: 65,
  },
  optionIcon: {
    width: 50,
    height: 50,
  },
  optionShadow: {
    position: 'absolute',
    top: 10,
    backgroundColor: Colors.teal,
    opacity: 0.2,
    width: 50,
    height: 25,
    borderRadius: 15,
  },
});

interface SleepModeOptionProps {
  mode: SleepingMode;
  isSelected: boolean;
  onOptionPressed: (mode: SleepingMode) => void;
}

function SleepModeOption({
  mode,
  isSelected,
  onOptionPressed,
}: SleepModeOptionProps) {
  const {t} = I18N.useTranslation('translations');

  function onPress() {
    onOptionPressed(mode);
  }

  return (
    <TouchableNativeFeedback onPress={onPress}>
      <View
        direction="column"
        cross="alignCenter"
        style={styles.optionContainer}>
        <View>
          {isSelected && <View style={styles.optionShadow} />}
          <Image
            source={TrackingUtils.sleepModeToIcon[mode]}
            style={styles.optionIcon}
            resizeMode="contain"
          />
        </View>
        <Text
          size="small"
          alignment="center"
          color={isSelected ? 'teal' : 'default'}>
          {t(`TRACKINGS_SLEEPING_MODE_${mode.toUpperCase()}`)}
        </Text>
      </View>
    </TouchableNativeFeedback>
  );
}

interface SleepModeSelectorProps {
  selectedModes: Record<SleepingMode, boolean>;
  onChange: (modes: Record<SleepingMode, boolean>) => void;
}

function SleepModeSelector({selectedModes, onChange}: SleepModeSelectorProps) {
  const {t} = I18N.useTranslation('translations');

  function onOptionPressed(mode: SleepingMode) {
    onChange({...selectedModes, [mode]: !selectedModes[mode]});
  }

  return (
    <View>
      <TextBnW>{t('FORM_LABEL_TRACKING_SLEEP_MODE')}</TextBnW>

      <Padding sizeV={2.5} />

      <ScrollView contentContainerStyle={styles.scrollView} horizontal>
        {possibleModes.map((mode) => {
          return (
            <SleepModeOption
              key={mode}
              mode={mode}
              isSelected={selectedModes[mode]}
              onOptionPressed={onOptionPressed}
            />
          );
        })}
      </ScrollView>

      <Padding size={7.5} />
    </View>
  );
}

export {SleepModeSelector};

import React from 'react';
import {StyleSheet, TouchableNativeFeedback, Image} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import * as I18N from 'react-i18next';
import dayjs from 'dayjs';

import {ThemeHook} from '../../../hooks';
import {Colors} from '../../../styles';
import {FinishedTracking, TrackingType} from '../../../types';
import {TrackingUtils} from '../../../utils';

import {View, Padding} from '../../Layout';
import {Text, TextBnW} from '../../Typography';

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    elevation: 4,
    height: 65,
    borderRadius: 65,
  },
});

const entryIconStyles = StyleSheet.create({
  common: {
    height: 53,
    width: 53,
  },
  inner: {
    top: 0,
    zIndex: 1,
    height: 53,
    width: 53,
    borderRadius: 53,
    backgroundColor: 'red',
    opacity: 0.2,
  },
  icon: {
    position: 'absolute',
    width: 35,
    height: 35,
  },
  sleeping: {
    backgroundColor: TrackingUtils.typeToColor.sleeping,
  },
  feeding: {
    backgroundColor: TrackingUtils.typeToColor.feeding,
  },
  diaper: {
    backgroundColor: TrackingUtils.typeToColor.diaper,
  },
});

interface TrackEntryIconProps {
  trackingType: TrackingType;
}

function TrackEntryIcon({trackingType}: TrackEntryIconProps) {
  return (
    <View
      style={[entryIconStyles.common]}
      main="justifyCenter"
      cross="alignCenter"
      padding={5}>
      <View style={[entryIconStyles.inner, entryIconStyles[trackingType]]} />
      <Image
        style={[entryIconStyles.icon]}
        source={TrackingUtils.typeToIcon[trackingType].active}
        resizeMode="contain"
      />
    </View>
  );
}

interface TrackEntryProps {
  readonly tracking: FinishedTracking;
}

function TrackEntry({tracking}: TrackEntryProps) {
  const {t} = I18N.useTranslation('translations');
  const {theme} = ThemeHook.useState();

  const duration = tracking.end.valueOf() - tracking.start.valueOf();

  const {hours, minutes} = TrackingUtils.durationToHms(duration);
  const isToday = TrackingUtils.isToday(tracking.end);
  const isYesterday = isToday ? false : TrackingUtils.isYesterday(tracking.end);
  const isCurrentYear = TrackingUtils.isCurrentYear(tracking.end);

  const date = isToday
    ? t('COMMON_TODAY')
    : isYesterday
    ? t('COMMON_YESTERDAY')
    : isCurrentYear
    ? dayjs(tracking.end).format(t('DATE_FORMAT_DAY_MONTH'))
    : dayjs(tracking.end).format(t('DATE_LONG_FORMAT'));

  const time =
    hours > 0
      ? `${t('COMMON_HOUR', {count: hours})} ${t(
          'COMMON_AND',
        )} ${t('COMMON_MINUTE', {count: minutes})}`
      : `${t('COMMON_MINUTE', {count: minutes})}`;

  return (
    <View style={styles.container} main="justifyCenter">
      <TouchableNativeFeedback>
        <View direction="row" bgHighlight={0} cross="alignCenter" padding={8}>
          <TrackEntryIcon trackingType={tracking.type} />

          <Padding sizeH={7.5} />

          <View direction="column" flex={1}>
            <TextBnW>
              {t('COMMON_LAST_SOMETHING', {
                something: t(
                  `TRACKINGS_TYPE_${tracking.type.toUpperCase()}`,
                ).toLowerCase(),
              })}
            </TextBnW>
            <Text size="small">
              {date} {t('COMMON_FOR')} {time}
            </Text>
          </View>

          <Padding sizeH={5} />

          <Icon
            name="chevron-right"
            color={Colors.textColors[theme]}
            size={20}
          />
        </View>
      </TouchableNativeFeedback>
    </View>
  );
}

export {TrackEntry};

import React from 'react';
import {StyleSheet, TouchableNativeFeedback} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import * as I18N from 'react-i18next';
import dayjs from 'dayjs';

import {
  FinishedFeedTracking,
  FinishedBreastFeedTracking,
  FinishedBottleFeedTracking,
} from '../../../../types';
import {TrackingUtils} from '../../../../utils';

import {View, Padding} from '../../../Layout';
import {Text, TextBnW} from '../../../Typography';

const styles = StyleSheet.create({
  itemContainer: {
    paddingVertical: 7.5,
  },
  firstColumn: {
    paddingRight: 20,
    width: '50%',
  },
});

interface BreastFeedingEntryListItemProps {
  tracking: FinishedBreastFeedTracking;
  goToBreastFeedFormScreen: (
    tracking: FinishedBreastFeedTracking,
    edit: boolean,
  ) => void;
}

function BreastFeedingEntryListItem({
  tracking,
  goToBreastFeedFormScreen,
}: BreastFeedingEntryListItemProps) {
  const {t} = I18N.useTranslation('translations');

  function onPress() {
    goToBreastFeedFormScreen(tracking, true);
  }

  const isToday = TrackingUtils.isToday(tracking.start);
  const isYesterday = isToday
    ? false
    : TrackingUtils.isYesterday(tracking.start);
  const isCurrentYear = TrackingUtils.isCurrentYear(tracking.start);

  const date = isToday
    ? t('COMMON_TODAY')
    : isYesterday
    ? t('COMMON_YESTERDAY')
    : isCurrentYear
    ? dayjs(tracking.end).format(t('DATE_FORMAT_DAY_MONTH'))
    : dayjs(tracking.end).format(t('DATE_LONG_FORMAT'));

  let breastsDisplay = '';

  if (tracking.leftDuration > 0 && tracking.rightDuration > 0) {
    breastsDisplay = `${t('TRACKINGS_FEEDING_LEFT')}/${t(
      'TRACKINGS_FEEDING_RIGHT',
    )}`;
  } else if (tracking.leftDuration > 0) {
    breastsDisplay = t('TRACKINGS_FEEDING_LEFT');
  } else if (tracking.rightDuration > 0) {
    breastsDisplay = t('TRACKINGS_FEEDING_RIGHT');
  }

  const durationInMinutes = Math.round(
    (tracking.leftDuration + tracking.rightDuration) / (1000 * 60),
  );

  return (
    <TouchableNativeFeedback onPress={onPress}>
      <View direction="row" cross="alignCenter" style={styles.itemContainer}>
        <View style={styles.firstColumn}>
          <Text>{date}</Text>
        </View>
        <View flex={1}>
          <Text>{breastsDisplay}</Text>
        </View>
        <View direction="row">
          <Text>{t('COMMON_MINUTES', {count: durationInMinutes})}</Text>
          <Padding sizeH={2.5} />
          <Text>
            <Icon name="chevron-right" size={18} />
          </Text>
        </View>
      </View>
    </TouchableNativeFeedback>
  );
}

interface BottleFeedingEntryListItemProps {
  tracking: FinishedBottleFeedTracking;
  goToBottleFeedFormScreen: (
    tracking: FinishedBottleFeedTracking,
    edit: boolean,
  ) => void;
}

function BottleFeedingEntryListItem({
  tracking,
  goToBottleFeedFormScreen,
}: BottleFeedingEntryListItemProps) {
  const {t} = I18N.useTranslation('translations');

  function onPress() {
    goToBottleFeedFormScreen(tracking, true);
  }

  const isToday = TrackingUtils.isToday(tracking.start);
  const isYesterday = isToday
    ? false
    : TrackingUtils.isYesterday(tracking.start);
  const isCurrentYear = TrackingUtils.isCurrentYear(tracking.start);

  const date = isToday
    ? t('COMMON_TODAY')
    : isYesterday
    ? t('COMMON_YESTERDAY')
    : isCurrentYear
    ? dayjs(tracking.end).format(t('DATE_FORMAT_DAY_MONTH'))
    : dayjs(tracking.end).format(t('DATE_LONG_FORMAT'));

  const volumeDisplay = `${tracking.mL}${t('UNITS_VOLUME_ML')}`;

  const duration = tracking.end.valueOf() - tracking.start.valueOf();

  return (
    <TouchableNativeFeedback onPress={onPress}>
      <View direction="row" style={styles.itemContainer}>
        <View style={styles.firstColumn}>
          <Text>{date}</Text>
        </View>
        <View flex={1}>
          <Text>{volumeDisplay}</Text>
        </View>
        <View direction="row">
          <Text>{TrackingUtils.durationTommss(duration)}</Text>
          <Padding sizeH={2.5} />
          <Text>
            <Icon name="chevron-right" size={18} />
          </Text>
        </View>
      </View>
    </TouchableNativeFeedback>
  );
}

interface FeedingEntryListItemProps {
  tracking: FinishedFeedTracking;
  goToBreastFeedFormScreen: (
    tracking: FinishedBreastFeedTracking,
    edit: boolean,
  ) => void;
  goToBottleFeedFormScreen: (
    tracking: FinishedBottleFeedTracking,
    edit: boolean,
  ) => void;
}
function FeedingEntryListItem({
  tracking,
  goToBreastFeedFormScreen,
  goToBottleFeedFormScreen,
}: FeedingEntryListItemProps) {
  if (tracking.mode === 'breast') {
    return (
      <BreastFeedingEntryListItem
        tracking={tracking as FinishedBreastFeedTracking}
        goToBreastFeedFormScreen={goToBreastFeedFormScreen}
      />
    );
  }
  return (
    <BottleFeedingEntryListItem
      tracking={tracking as FinishedBottleFeedTracking}
      goToBottleFeedFormScreen={goToBottleFeedFormScreen}
    />
  );
}

interface FeedingEntryListProps {
  trackings: Array<FinishedFeedTracking>;
  goToBreastFeedFormScreen: (
    tracking: FinishedBreastFeedTracking,
    edit: boolean,
  ) => void;
  goToBottleFeedFormScreen: (
    tracking: FinishedBottleFeedTracking,
    edit: boolean,
  ) => void;
}
function FeedingEntryList({
  trackings,
  goToBreastFeedFormScreen,
  goToBottleFeedFormScreen,
}: FeedingEntryListProps) {
  const {t} = I18N.useTranslation('translations');

  return (
    <>
      <View direction="row" main="spaceBetween">
        <TextBnW weight="bold">{t('TRACKINGS_FEEDING_LATEST')}</TextBnW>
        <TextBnW weight="bold">{t('COMMON_DURATION')}</TextBnW>
      </View>

      <Padding sizeV={7.5} />

      {trackings.map((tracking: FinishedFeedTracking) => {
        return (
          <FeedingEntryListItem
            key={tracking.id}
            tracking={tracking}
            goToBreastFeedFormScreen={goToBreastFeedFormScreen}
            goToBottleFeedFormScreen={goToBottleFeedFormScreen}
          />
        );
      })}
    </>
  );
}

export {FeedingEntryList};

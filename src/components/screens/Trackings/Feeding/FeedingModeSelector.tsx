import React from 'react';
import {TouchableWithoutFeedback, StyleSheet} from 'react-native';

import * as I18N from 'react-i18next';

import {Colors} from '../../../../styles';
import {FeedingMode} from '../../../../types';

import {View} from '../../../Layout';
import {Text} from '../../../Typography';

const styles = StyleSheet.create({
  wrapper: {
    borderRadius: 25,
  },
  container: {
    borderRadius: 25,
    height: 25,
    marginRight: 15,
    paddingHorizontal: 10,
  },
  background: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.orange,
    opacity: 0.2,
    borderRadius: 25,
  },
});

interface ModeSelectorProps {
  mode: FeedingMode;
  isActive: boolean;
  onChange: (mode: FeedingMode) => void;
}

function ModeSelector({mode, onChange, isActive}: ModeSelectorProps) {
  const {t} = I18N.useTranslation('translations');

  function onPress() {
    onChange(mode);
  }

  return (
    <View style={styles.wrapper}>
      <TouchableWithoutFeedback onPress={onPress}>
        <View main="justifyCenter" cross="alignCenter" style={styles.container}>
          {isActive && <View style={styles.background} />}

          <Text color={isActive ? 'orange' : 'default'} size="small">
            {t(`TRACKINGS_FEEDING_MODE_${mode.toUpperCase()}`)}
          </Text>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
}

interface FeedingModeSelectorProps {
  currentMode: FeedingMode;
  onChange: (mode: FeedingMode) => void;
}

function FeedingModeSelector({
  currentMode,
  onChange,
}: FeedingModeSelectorProps) {
  return (
    <View direction="row" main="justifyCenter">
      <ModeSelector
        mode="breast"
        onChange={onChange}
        isActive={currentMode === 'breast'}
      />
      <ModeSelector
        mode="bottle"
        onChange={onChange}
        isActive={currentMode === 'bottle'}
      />
    </View>
  );
}

export {FeedingModeSelector};

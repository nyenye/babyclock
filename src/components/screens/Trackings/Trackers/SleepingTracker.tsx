import React from 'react';
import {Alert, Animated, Easing, StyleSheet} from 'react-native';

import * as I18N from 'react-i18next';

import {TrackingHook} from '../../../../hooks';
import {Colors} from '../../../../styles';
import {OngoingSleepTracking, FinishedSleepTracking} from '../../../../types';

import {View, Padding} from '../../../Layout';
import {Text} from '../../../Typography';
import {Button} from '../../../Button';
import {Timer} from '../../../Timer';
import {TrackingUtils} from '../../../../utils';

const styles = StyleSheet.create({
  container: {
    height: 270,
  },
  animatedView: {
    position: 'absolute',
  },
  shadowBackground: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.teal,
    opacity: 0.2,
  },
  sleepIcon: {
    width: 96,
    height: 96,
  },
  sleepIconShadow: {
    top: 35,
    right: -2,
    width: 70,
    height: 70,
    borderRadius: 70,
    opacity: 0.18,
    backgroundColor: TrackingUtils.typeToColor.sleeping,
  },
});

function AnimatedSleepIcon() {
  const fadeAnim = React.useRef(new Animated.Value(0)).current; // Initial value for opacity: 0

  React.useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(fadeAnim, {
          toValue: 1,
          duration: 1250,
          easing: Easing.inOut(Easing.ease),
          useNativeDriver: true,
        }),
        Animated.timing(fadeAnim, {
          toValue: 0,
          duration: 750,
          easing: Easing.inOut(Easing.ease),
          useNativeDriver: true,
        }),
      ]),
    ).start();
  }, [fadeAnim]);

  return (
    <Animated.Image
      style={[
        styles.sleepIcon,
        {
          transform: [
            {
              translateY: fadeAnim.interpolate({
                inputRange: [0, 1],
                outputRange: [0, -5], // 0 : 150, 0.5 : 75, 1 : 0
              }),
            },
          ],
        },
      ]}
      source={TrackingUtils.typeToIcon.sleeping.active}
    />
  );
}

interface SleepTrackerProps {
  babyId: string;
  tracking: OngoingSleepTracking | undefined;
  goToFormScreen: (
    tracking?: FinishedSleepTracking,
    isEditting?: boolean,
  ) => void;
}

function SleepTracker({babyId, tracking, goToFormScreen}: SleepTrackerProps) {
  const {t} = I18N.useTranslation('translations');

  const trackingDispatch = TrackingHook.useDispatch();
  const {
    removeTrackingRequestState,
    startTrackingRequestState,
    updateTrackingRequestState,
  } = TrackingHook.useState();

  function onAddManualEntry() {
    goToFormScreen();
  }

  function startSleeping() {
    TrackingHook.startTrackingSleep(trackingDispatch, babyId);
  }

  function endSleeping() {
    TrackingHook.stopTracking(trackingDispatch, babyId, tracking!);

    setTimeout(() => {
      goToFormScreen({...tracking!, end: new Date()}, true);
    }, 500);
  }

  function cancelSleeping() {
    Alert.alert(
      t('ALERT_TITLE_TRACKING_CANCEL'),
      t('ALERT_MESSAGE_TRACKING_CANCEL'),
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            TrackingHook.removeTracking(trackingDispatch, babyId, tracking!.id);
          },
        },
      ],
      {cancelable: true},
    );
  }

  React.useEffect(() => {
    if (startTrackingRequestState.status === 'resolved') {
      trackingDispatch({
        type: 'RESET_REQUEST',
        requestStateId: 'startTrackingRequestState',
      });
    }
    if (removeTrackingRequestState.status === 'resolved') {
      trackingDispatch({
        type: 'RESET_REQUEST',
        requestStateId: 'removeTrackingRequestState',
      });
    }
    if (updateTrackingRequestState.status === 'resolved') {
      trackingDispatch({
        type: 'RESET_REQUEST',
        requestStateId: 'updateTrackingRequestState',
      });
    }
  });

  if (tracking === undefined) {
    return (
      <>
        <View direction="column" padding={20} rounded style={styles.container}>
          <View position="absolute" rounded style={styles.shadowBackground} />

          <Text color="teal" size="large" alignment="center" weight="bold">
            {t('TRACKINGS_SLEEPING_TAP_TO_START')}
          </Text>

          <View flex={1} />

          <View direction="row" main="justifyCenter">
            {/* Left Button */}
            <View direction="column">
              <Button
                onPress={startSleeping}
                title={t('COMMON_START')}
                color="teal"
                type="outlined"
                size="xl"
                circle
              />
              <Padding sizeV={5} />
            </View>
          </View>

          <View flex={1} />
        </View>

        <Padding sizeV={7.5} />

        <Button
          onPress={onAddManualEntry}
          title={t('TRACKINGS_ADD_MANUAL_ENTRY')}
          color="teal"
          size="small"
        />
      </>
    );
  }

  return (
    <>
      <View
        direction="column"
        cross="alignCenter"
        padding={20}
        rounded
        style={styles.container}>
        <View position="absolute" rounded style={styles.shadowBackground} />

        <Timer
          start={tracking.start}
          format="m:s"
          textFormat={{
            size: 'xl',
            color: 'teal',
            weight: 'bold',
          }}
        />

        <Text color="teal" alignment="center">
          {t('TRACKINGS_SLEEPING_TOTAL')}
        </Text>

        <View flex={1} />

        <View>
          <View position="absolute" style={styles.sleepIconShadow} />
          <AnimatedSleepIcon />
        </View>

        <View flex={1} />
      </View>

      <Padding sizeV={7.5} />

      <View direction="row">
        <Button
          onPress={cancelSleeping}
          title={t('COMMON_CANCEL')}
          color="grayLight"
          type="outlined"
          size="small"
          flex={1}
        />
        <Padding sizeH={7.5} />
        <Button
          onPress={endSleeping}
          title={t('COMMON_STOP')}
          color="teal"
          size="small"
          flex={1}
        />
      </View>
    </>
  );
}

export {SleepTracker};

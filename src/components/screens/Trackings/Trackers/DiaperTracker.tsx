import React from 'react';
import {StyleSheet} from 'react-native';

import * as I18N from 'react-i18next';

import {TrackingHook} from '../../../../hooks';
import {Colors} from '../../../../styles';
import {DiaperMode} from '../../../../types';

import {View, Padding} from '../../../Layout';
import {Text} from '../../../Typography';
import {Button} from '../../../Button';
import {TrackingUtils} from '../../../../utils';

const styles = StyleSheet.create({
  container: {
    height: 270,
  },
  shadowBackground: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.pink,
    opacity: 0.2,
  },
  sleepIcon: {
    width: 96,
    height: 96,
  },
  sleepIconShadow: {
    top: 35,
    right: -2,
    width: 70,
    height: 70,
    borderRadius: 70,
    opacity: 0.18,
    backgroundColor: TrackingUtils.typeToColor.diaper,
  },
});

interface DiaperTrackerProps {
  babyId: string;
  goToFormScreen: () => void;
}

function DiaperTracker({babyId, goToFormScreen}: DiaperTrackerProps) {
  const {t} = I18N.useTranslation('translations');

  const trackingDispatch = TrackingHook.useDispatch();

  function onAddManualEntry() {
    goToFormScreen();
  }

  function reportDiaper(mode: DiaperMode) {
    TrackingHook.reportDiaperTracking(trackingDispatch, babyId, mode);
  }

  function reportPee() {
    reportDiaper('pee');
  }

  function reportPoop() {
    reportDiaper('poop');
  }

  return (
    <>
      <View direction="column" padding={20} rounded style={styles.container}>
        <View position="absolute" rounded style={styles.shadowBackground} />

        <Text color="pink" size="large" alignment="center" weight="bold">
          {t('TRACKINGS_DIAPER_REPORT')}
        </Text>

        <View flex={1} />

        <View direction="row" main="spaceEvenly">
          <Button
            onPress={reportPee}
            title="💦"
            color="pink"
            type="outlined"
            size="large"
            circle
          />
          <Button
            onPress={reportPoop}
            title="💩"
            color="pink"
            type="outlined"
            size="large"
            circle
          />
        </View>

        <View flex={1} />
      </View>

      <Padding sizeV={7.5} />

      <Button
        onPress={onAddManualEntry}
        title={t('TRACKINGS_ADD_MANUAL_ENTRY')}
        color="pink"
        size="small"
      />
    </>
  );
}

export {DiaperTracker};

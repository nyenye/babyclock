import React from 'react';
import {Alert, StyleSheet} from 'react-native';

import * as I18N from 'react-i18next';

import {TrackingHook} from '../../../../hooks';
import {Colors} from '../../../../styles';
import {
  OngoingBreastFeedTracking,
  OngoingBottleFeedTracking,
  LorR,
  FinishedBreastFeedTracking,
  FinishedBottleFeedTracking,
} from '../../../../types';

import {View, Padding} from '../../../Layout';
import {Text} from '../../../Typography';
import {Button} from '../../../Button';
import {Timer} from '../../../Timer';

const styles = StyleSheet.create({
  container: {
    height: 270,
  },
  shadowBackground: {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: Colors.orange,
    opacity: 0.2,
  },
});

interface BreastFeedingTrackerProps {
  babyId: string;
  tracking: OngoingBreastFeedTracking | undefined;
  goToFormScreen: (
    tracking?: FinishedBreastFeedTracking,
    isEditting?: boolean,
  ) => void;
}

function BreastFeedingTracker({
  babyId,
  tracking,
  goToFormScreen,
}: BreastFeedingTrackerProps) {
  const {t} = I18N.useTranslation('translations');

  const trackingDispatch = TrackingHook.useDispatch();

  function startBreastfeeding(breast: LorR) {
    TrackingHook.startTrackingBreastfeed(trackingDispatch, babyId, breast);
  }

  function endBreastfeeding() {
    TrackingHook.stopTracking(trackingDispatch, babyId, tracking!);
  }

  function cancelBreastfeeding() {
    Alert.alert(
      t('ALERT_TITLE_TRACKING_CANCEL'),
      t('ALERT_MESSAGE_TRACKING_CANCEL'),
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            TrackingHook.removeTracking(trackingDispatch, babyId, tracking!.id);
          },
        },
      ],
      {cancelable: true},
    );
  }

  function startBreastTimer(breast: LorR) {
    if (tracking === undefined) {
      return startBreastfeeding(breast);
    }

    const leftTimers =
      breast === 'l'
        ? [...tracking.leftTimers, {start: new Date()}]
        : [...tracking.leftTimers];
    const rightTimers =
      breast === 'r'
        ? [...tracking.rightTimers, {start: new Date()}]
        : [...tracking.rightTimers];

    const trackingUpdate = {
      ...tracking,
      leftTimers,
      rightTimers,
    };

    TrackingHook.updateTracking(trackingDispatch, babyId, trackingUpdate);
  }

  function startLeftBreastTimer() {
    startBreastTimer('l');
  }

  function startRightBreastTimer() {
    startBreastTimer('r');
  }

  function endBreastTimer(breast: LorR) {
    const leftTimers = [...tracking!.leftTimers];
    const rightTimers = [...tracking!.rightTimers];

    const lastTimer =
      breast === 'l'
        ? leftTimers[leftTimers.length - 1]
        : rightTimers[rightTimers.length - 1];
    lastTimer.end = new Date();

    const lastDuration = lastTimer.end.valueOf() - lastTimer.start.valueOf();

    const trackingUpdate = {
      ...tracking!,
      leftTimers,
      leftDuration:
        breast === 'l'
          ? tracking!.leftDuration + lastDuration
          : tracking!.leftDuration,
      rightTimers,
      rightDuration:
        breast === 'r'
          ? tracking!.rightDuration + lastDuration
          : tracking!.rightDuration,
    };

    TrackingHook.updateTracking(trackingDispatch, babyId, trackingUpdate);
  }

  function endLeftBreastTimer() {
    endBreastTimer('l');
  }

  function endRightBreastTimer() {
    endBreastTimer('r');
  }

  function toggleBreastTimer(running: LorR) {
    const leftTimers =
      running === 'l'
        ? [...tracking!.leftTimers]
        : [...tracking!.leftTimers, {start: new Date()}];
    const rightTimers =
      running === 'r'
        ? [...tracking!.rightTimers]
        : [...tracking!.rightTimers, {start: new Date()}];

    const lastTimer =
      running === 'l'
        ? leftTimers[leftTimers.length - 1]
        : rightTimers[rightTimers.length - 1];
    lastTimer.end = new Date();

    const lastDuration = lastTimer.end.valueOf() - lastTimer.start.valueOf();

    const trackingUpdate = {
      ...tracking!,
      leftTimers,
      leftDuration:
        running === 'l'
          ? tracking!.leftDuration + lastDuration
          : tracking!.leftDuration,
      rightTimers,
      rightDuration:
        running === 'r'
          ? tracking!.rightDuration + lastDuration
          : tracking!.rightDuration,
    };

    TrackingHook.updateTracking(trackingDispatch, babyId, trackingUpdate);
  }

  function toggleRightToLeft() {
    toggleBreastTimer('r');
  }

  function toggleLeftToRight() {
    toggleBreastTimer('l');
  }

  function onAddManualEntry() {
    goToFormScreen();
  }

  if (tracking === undefined) {
    return (
      <>
        <View direction="column" padding={20} rounded style={styles.container}>
          <View position="absolute" rounded style={styles.shadowBackground} />

          <Text color="orange" size="large" alignment="center" weight="bold">
            {t('TRACKINGS_FEEDING_TAP_L_OR_R')}
          </Text>

          <View flex={2} />

          <View direction="row" main="spaceEvenly">
            {/* Left Button */}
            <View direction="column">
              <Button
                onPress={startLeftBreastTimer}
                title={t('TRACKINGS_FEEDING_LEFT')}
                color="orange"
                type="outlined"
                size="large"
                circle
              />
              <Padding sizeV={5} />
              <Timer
                start={new Date()}
                isRunning={false}
                format="m:s"
                textFormat={{
                  size: 'large',
                  color: 'orange',
                }}
              />
            </View>

            {/* Right Button */}
            <View direction="column">
              <Button
                onPress={startRightBreastTimer}
                title={t('TRACKINGS_FEEDING_RIGHT')}
                color="orange"
                type="outlined"
                size="large"
                circle
              />
              <Padding sizeV={5} />
              <Timer
                start={new Date()}
                isRunning={false}
                format="m:s"
                textFormat={{
                  size: 'large',
                  color: 'orange',
                }}
              />
            </View>
          </View>

          <View flex={1} />
        </View>

        <Padding sizeV={7.5} />

        <Button
          onPress={onAddManualEntry}
          title={t('TRACKINGS_ADD_MANUAL_ENTRY')}
          color="orange"
          size="small"
        />
      </>
    );
  }

  const lTimer = tracking.leftTimers[tracking.leftTimers.length - 1];
  const rTimer = tracking.rightTimers[tracking.rightTimers.length - 1];

  const isLeftRunning =
    (lTimer && lTimer.start !== undefined && lTimer.end === undefined) || false;
  const isRightRunning =
    (rTimer && rTimer.start !== undefined && rTimer.end === undefined) || false;

  return (
    <>
      <View direction="column" padding={20} rounded style={styles.container}>
        <View position="absolute" rounded style={styles.shadowBackground} />

        <Timer
          start={new Date() || tracking.start}
          initialValue={tracking.leftDuration + tracking.rightDuration}
          isRunning={isLeftRunning || isRightRunning}
          format="m:s"
          textFormat={{
            size: 'xl',
            color: 'orange',
            weight: 'bold',
          }}
        />

        <Text color="orange" alignment="center">
          {t('TRACKINGS_FEEDING_TOTAL')}
        </Text>

        <View flex={2} />

        <View direction="row" main="spaceEvenly">
          {/* Left Button */}
          <View direction="column">
            <Button
              onPress={
                isLeftRunning
                  ? endLeftBreastTimer
                  : isRightRunning
                  ? toggleRightToLeft
                  : startLeftBreastTimer
              }
              title={t('TRACKINGS_FEEDING_LEFT')}
              color="orange"
              type={isLeftRunning ? 'primary' : 'outlined'}
              size="large"
              circle
            />
            <Padding sizeV={5} />
            <Timer
              start={isLeftRunning ? lTimer.start : new Date()}
              format="m:s"
              initialValue={tracking.leftDuration}
              isRunning={isLeftRunning}
              textFormat={{
                size: 'large',
                color: 'orange',
              }}
            />
          </View>

          {/* Right Button */}
          <View direction="column">
            <Button
              onPress={
                isRightRunning
                  ? endRightBreastTimer
                  : isLeftRunning
                  ? toggleLeftToRight
                  : startRightBreastTimer
              }
              title={t('TRACKINGS_FEEDING_RIGHT')}
              color="orange"
              type={isRightRunning ? 'primary' : 'outlined'}
              size="large"
              circle
            />
            <Padding sizeV={5} />
            <Timer
              start={isRightRunning ? rTimer.start : new Date()}
              format="m:s"
              initialValue={tracking.rightDuration}
              isRunning={isRightRunning}
              textFormat={{
                size: 'large',
                color: 'orange',
              }}
            />
          </View>
        </View>

        <View flex={1} />
      </View>

      <Padding sizeV={7.5} />

      <View direction="row">
        <Button
          onPress={cancelBreastfeeding}
          title={t('COMMON_CANCEL')}
          color="grayLight"
          type="outlined"
          size="small"
          flex={1}
        />
        <Padding sizeH={7.5} />
        <Button
          onPress={endBreastfeeding}
          title={t('COMMON_STOP')}
          color="orange"
          size="small"
          flex={1}
          disabled={isLeftRunning || isRightRunning}
        />
      </View>
    </>
  );
}

interface BottleFeedingTrackerProps {
  babyId: string;
  tracking: OngoingBottleFeedTracking | undefined;
  goToFormScreen: (
    tracking?: FinishedBottleFeedTracking,
    isEditting?: boolean,
  ) => void;
}

function BottleFeedingTracker({}: BottleFeedingTrackerProps) {
  return <View />;
}

export {BreastFeedingTracker, BottleFeedingTracker};

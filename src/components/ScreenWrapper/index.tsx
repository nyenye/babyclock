import React from 'react';
import {
  KeyboardAvoidingView,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Platform,
} from 'react-native';

import {ThemeHook} from '../../hooks';
import {Layouts, Colors} from '../../styles';

const styles = StyleSheet.create({
  defaultContainer: {
    flex: 1,
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  containerWithScrollView: {
    flex: 1,
  },
  scrollViewContainer: {
    minHeight: '100%',
  },
});

function getDynamicStyles(
  theme: ThemeHook.Theme,
  backgroundColor: string | undefined,
  hasPadding: boolean,
) {
  return StyleSheet.create({
    defaultContainer: {
      backgroundColor:
        (backgroundColor && backgroundColor) || Colors.backgroundColors[theme],
      paddingHorizontal: hasPadding ? 15 : 0,
      paddingVertical: hasPadding ? 15 : 0,
    },
    containerWithScrollView: {
      backgroundColor:
        (backgroundColor && backgroundColor) || Colors.backgroundColors[theme],
    },
    scrollViewContainer: {
      paddingHorizontal: hasPadding ? 15 : 0,
      paddingVertical: hasPadding ? 15 : 0,
    },
  });
}

interface ScreenWrapperProps {
  backgroundColor?: string;
  statusBarColor?: string;
  isScrollView?: boolean;
  hasPadding?: boolean;
  children: React.ReactNode;
}

function ScreenWrapper({
  backgroundColor = undefined,
  statusBarColor = undefined,
  isScrollView = false,
  hasPadding = true,
  children,
}: ScreenWrapperProps) {
  const {theme} = ThemeHook.useState();

  const dynamicStyles = getDynamicStyles(theme, backgroundColor, hasPadding);

  if (isScrollView) {
    return (
      <SafeAreaView
        style={[
          styles.containerWithScrollView,
          dynamicStyles.containerWithScrollView,
        ]}>
        <StatusBar
          backgroundColor={statusBarColor || Colors.statusbarColors[theme]}
        />
        <ScrollView
          contentContainerStyle={[
            styles.scrollViewContainer,
            dynamicStyles.scrollViewContainer,
          ]}>
          {children}
        </ScrollView>
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView
      style={[
        Layouts.expand,
        styles.defaultContainer,
        dynamicStyles.defaultContainer,
      ]}>
      <StatusBar backgroundColor={statusBarColor} />
      {children}
    </SafeAreaView>
  );
}

const stylesKAV = StyleSheet.create({container: {flex: 1}});

interface ScreenWrapperWithKeyboardAVProps extends ScreenWrapperProps {
  scrollVerticalOffset?: number;
}

function ScreenWrapperWithKeyboardAV({
  scrollVerticalOffset = 0,
  ...props
}: ScreenWrapperWithKeyboardAVProps) {
  return (
    <KeyboardAvoidingView
      style={stylesKAV.container}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={scrollVerticalOffset}>
      <ScreenWrapper {...props} />
    </KeyboardAvoidingView>
  );
}

export {ScreenWrapper, ScreenWrapperWithKeyboardAV};

// In App.js in a new project

// Make sure it's at the top and there's nothing else before it
import 'react-native-gesture-handler';

import * as React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';

import {ThemeHook, AuthHook} from './hooks';
import {Navigation} from './navigation';

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
});

function App() {
  return (
    <SafeAreaView style={styles.safeArea}>
      <NavigationContainer>
        <Navigation />
      </NavigationContainer>
    </SafeAreaView>
  );
}

function ProvidedApp() {
  return (
    <AuthHook.Provider>
      <ThemeHook.Provider defaultTheme="light">
        <App />
      </ThemeHook.Provider>
    </AuthHook.Provider>
  );
}

export {ProvidedApp as App};

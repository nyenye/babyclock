import React from 'react';
import 'react-native';
// @ts-ignore
import {render} from 'test-utils';

import {App} from './App';

jest.useFakeTimers();

it('renders correctly', () => {
  render(<App />);
});

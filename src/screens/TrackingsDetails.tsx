import React from 'react';
import {Image, StyleSheet, TouchableNativeFeedback} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {
  TabView,
  Route,
  SceneRendererProps,
  NavigationState,
} from 'react-native-tab-view';

import * as I18N from 'react-i18next';

import {ScreenWrapper, View, Text, TextBnW} from '../components';
import {TrackingsStackParamList} from '../navigation/Stacks';
import {Colors} from '../styles';
import {TrackingType} from '../types';
import {TrackingUtils} from '../utils';

import {SleepTrackingTab} from './Trackings/SleepTrackingTab';
import {FeedTrackingTab} from './Trackings/FeedTrackingTab';
import {DiaperTrackingTab} from './Trackings/DiaperTrackingTab';

const stylesTabBar = StyleSheet.create({
  bar: {
    flexDirection: 'row',
    height: 85,
    borderBottomWidth: 0.5,
    borderBottomColor: Colors.backgroundColors.highlights.light[2],
  },
  icon: {
    width: 50,
    height: 50,
  },
  highlighter: {
    width: '33%',
    bottom: -0.5,
    height: 0,
  },
  highlighterActive: {
    height: 2,
  },
  shadow: {
    top: 14,
    right: -4,
    width: 35,
    height: 35,
    borderRadius: 35,
    opacity: 0.2,
  },
  sleeping: {
    backgroundColor: TrackingUtils.typeToColor.sleeping,
  },
  feeding: {
    backgroundColor: TrackingUtils.typeToColor.feeding,
  },
  diaper: {
    backgroundColor: TrackingUtils.typeToColor.diaper,
  },
});

const trackingToIndex: Record<TrackingType, number> = {
  feeding: 0,
  sleeping: 1,
  diaper: 2,
};

interface TrackingsDetailsProps {
  navigation: StackNavigationProp<TrackingsStackParamList, 'TRACKINGS_DETAILS'>;
  route: RouteProp<TrackingsStackParamList, 'TRACKINGS_DETAILS'>;
}

function TrackingsDetails({navigation, route}: TrackingsDetailsProps) {
  const {t} = I18N.useTranslation('translations');

  const {initialTracking} = route.params;

  const [index, setIndex] = React.useState(
    initialTracking ? trackingToIndex[initialTracking] : 1,
  );

  React.useEffect(() => {
    if (initialTracking !== undefined) {
      // Without timeout, renderScene, doesn't catch up, and it doesn't render the correct TabView
      setTimeout(() => setIndex(trackingToIndex[initialTracking]), 100);
    }
  }, [initialTracking]);

  const [routes] = React.useState<Route[]>([
    {key: 'feeding', title: t('TRACKINGS_TYPE_FEEDING')},
    {key: 'sleeping', title: t('TRACKINGS_TYPE_SLEEPING')},
    {key: 'diaper', title: t('TRACKINGS_TYPE_DIAPER')},
  ]);

  function renderTabBar(
    props: SceneRendererProps & {navigationState: NavigationState<Route>},
  ) {
    const {navigationState} = props;

    return (
      <View direction="row" bgHighlight={0} style={stylesTabBar.bar}>
        {navigationState.routes.map((tabRoute: Route, i: number) => {
          const isCurrent: boolean = i === navigationState.index;

          const trackingType = tabRoute.key as TrackingType;

          return (
            <TouchableNativeFeedback
              key={tabRoute.key}
              onPress={() => {
                setIndex(i);
              }}>
              <View
                flex={1}
                direction="column"
                main="justifyCenter"
                cross="alignCenter">
                <View>
                  <View
                    position="absolute"
                    style={[stylesTabBar.shadow, stylesTabBar[trackingType]]}
                  />
                  <Image
                    style={stylesTabBar.icon}
                    resizeMode="contain"
                    source={TrackingUtils.typeToIcon[trackingType].active}
                  />
                </View>

                {(isCurrent && (
                  <TextBnW>
                    {t(`TRACKINGS_TYPE_${trackingType.toUpperCase()}`)}
                  </TextBnW>
                )) || (
                  <Text>
                    {t(`TRACKINGS_TYPE_${trackingType.toUpperCase()}`)}
                  </Text>
                )}
                <View
                  position="absolute"
                  bgHighlight={2}
                  style={[
                    stylesTabBar.highlighter,
                    isCurrent && [
                      stylesTabBar[trackingType],
                      stylesTabBar.highlighterActive,
                    ],
                  ]}
                />
              </View>
            </TouchableNativeFeedback>
          );
        })}
      </View>
    );
  }

  function renderScene({
    route: sceneRoute,
  }: SceneRendererProps & {route: Route}) {
    switch (sceneRoute.key) {
      case 'feeding':
        return <FeedTrackingTab navigation={navigation} route={route} />;
      case 'sleeping':
        return <SleepTrackingTab navigation={navigation} route={route} />;
      case 'diaper':
        return <DiaperTrackingTab navigation={navigation} route={route} />;
      default:
        return null;
    }
  }

  return (
    <ScreenWrapper hasPadding={false}>
      <TabView
        navigationState={{index, routes}}
        renderTabBar={renderTabBar}
        renderScene={renderScene}
        onIndexChange={setIndex}
      />
    </ScreenWrapper>
  );
}

export {TrackingsDetails};

import React from 'react';
import {TextInput as TextInputNative, Alert} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import * as I18N from 'react-i18next';

import {
  Button,
  Divider,
  ListBabies,
  ImagePickerController,
  Padding,
  ScreenWrapperWithKeyboardAV,
  TextInput,
  TextInputController,
  View,
  Heading,
} from '../components';
import {BabiesHook, AuthHook} from '../hooks';
import {SetupProfileStackParamList} from '../navigation/Stacks';
import {Baby, UserProfile} from '../types';
import {FormUtils} from '../utils';

interface UserProfileSetupProps {
  navigation: StackNavigationProp<
    SetupProfileStackParamList,
    'USER_PROFILE_SETUP'
  >;
}

function UserProfileSetup({navigation}: UserProfileSetupProps) {
  const {t} = I18N.useTranslation('translations');
  const {babies} = BabiesHook.useState();
  const {
    createUserProfileRequestState: createRequestState,
  } = AuthHook.useState();
  const dispatch = AuthHook.useDispatch();

  const lastNameInputRef = React.useRef<TextInputNative>();

  const [firstName, setFirstName] = React.useState<null | string>(null);
  const [lastName, setLastName] = React.useState<null | string>(null);
  const [pictureUri, setPictureUri] = React.useState<null | string>(null);

  function onFirstNameSubmitEditting() {
    if (lastNameInputRef.current !== undefined) {
      lastNameInputRef.current.focus();
    }
  }

  function onAddBabyButtonPressed() {
    const currentUser = AuthHook.getCurrentUser();

    navigation.navigate('BABY_PROFILE_SETUP', {
      userId: currentUser!.uid,
      isEditting: false,
    });
  }

  function onEditBabyButtonPressed(baby: Baby) {
    const currentUser = AuthHook.getCurrentUser();

    navigation.navigate('BABY_PROFILE_SETUP', {
      userId: currentUser!.uid,
      isEditting: true,
      baby,
    });
  }

  const firstNameErrors = FormUtils.getRequiredTextfieldErrors(firstName);
  const lastNameErrors = FormUtils.getRequiredTextfieldErrors(lastName);
  const pictureUriErrors = FormUtils.getRequiredTextfieldErrors(pictureUri);

  function canPressDoneButton() {
    if (firstNameErrors.hasError || firstName === null) {
      return false;
    }

    if (firstNameErrors.hasError || lastName === null) {
      return false;
    }

    if (pictureUriErrors.hasError || pictureUri === null) {
      return false;
    }

    if (babies.length === 0) {
      return false;
    }

    return true;
  }

  function onDoneButtonPressed() {
    if (!canPressDoneButton()) {
      return;
    }

    const currentUser = AuthHook.getCurrentUser();

    if (currentUser === null) {
      return;
    }

    const babyIds: Array<string> = babies.map((baby: Baby) => baby.id);

    const profileData: UserProfile = {
      id: currentUser.uid,
      firstName: firstName!,
      lastName: lastName!,
      babies: babyIds,
      babiesInCare: [],
      pictureFilename: '',
      pictureUri: pictureUri!,
    };

    AuthHook.createUserProfile(dispatch, currentUser.uid, profileData);
  }

  React.useEffect(() => {
    if (createRequestState.status === 'rejected') {
      Alert.alert(
        t('ALERT_TITLE_SOMETHING_WENT_WRONG'),
        t('ALERT_MESSAGE_SOMETHING_WENT_WRONG'),
        [{text: t('ALERT_BUTTON_OK')}],
      );
    }
  }, [t, createRequestState]);

  return (
    <ScreenWrapperWithKeyboardAV isScrollView={true} scrollVerticalOffset={80}>
      <View flex={1}>
        <Heading type="h3">{t('PROFILE_YOUR_INFO')}</Heading>

        <Padding sizeV={7.5} />

        <TextInputController
          label={t('FORM_LABEL_FIRST_NAME')}
          errorMsg={
            firstNameErrors.hasError ? t(firstNameErrors.errorMsg!) : ''
          }>
          <TextInput
            onChangeText={setFirstName}
            value={firstName || ''}
            placeholder={t('FORM_PLACEHOLDER_FIRST_NAME')}
            returnKeyType="next"
            isRequired
            onSubmitEditing={onFirstNameSubmitEditting}
            hasError={firstNameErrors.hasError}
          />
        </TextInputController>

        <Padding sizeV={5} />

        <TextInputController
          label={t('FORM_LABEL_LAST_NAME')}
          errorMsg={lastNameErrors.hasError ? t(lastNameErrors.errorMsg!) : ''}>
          <TextInput
            forwardRef={lastNameInputRef}
            onChangeText={setLastName}
            value={lastName || ''}
            placeholder={t('FORM_PLACEHOLDER_LAST_NAME')}
            returnKeyType="done"
            isRequired
            hasError={lastNameErrors.hasError}
          />
        </TextInputController>

        <Padding sizeV={5} />

        <ImagePickerController
          label={t('FORM_LABEL_PROFILE_PICTURE')}
          errorMsg={
            pictureUriErrors.hasError ? t(pictureUriErrors.errorMsg!) : ''
          }
          imageUri={pictureUri}
          onChangePickedImage={setPictureUri}
        />

        <Padding sizeV={7.5} />
        <Divider direction="horizontal" highlight={2} />
        <Padding sizeV={7.5} />

        <ListBabies
          onAddBabyButtonPressed={onAddBabyButtonPressed}
          onEditButtonPressed={onEditBabyButtonPressed}
        />

        <Padding sizeV={7.5} />
        <Divider direction="horizontal" highlight={2} />
        <Padding sizeV={7.5} />

        <View push="pushBottom">
          <Button
            title={t('FORM_BUTTON_DONE').toUpperCase()}
            onPress={onDoneButtonPressed}
            disabled={!canPressDoneButton()}
          />
        </View>
      </View>
    </ScreenWrapperWithKeyboardAV>
  );
}

export {UserProfileSetup};

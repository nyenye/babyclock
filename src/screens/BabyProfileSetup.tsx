import React from 'react';
import {Alert} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import * as I18N from 'react-i18next';

import {v4 as uuid} from 'uuid';

import {
  Button,
  DateTimePickerController,
  ImagePickerController,
  Padding,
  ScreenWrapperWithKeyboardAV,
  SelectorController,
  SelectorControllerProps,
  TextInputController,
  TextInput,
  View,
} from '../components';
import {BabiesHook, AuthHook} from '../hooks';
import {SetupProfileStackParamList} from '../navigation/Stacks';
import {FormUtils, StorageUtils} from '../utils';
import {GenderType} from '../types';

interface BabyProfileSetupProps {
  navigation: StackNavigationProp<
    SetupProfileStackParamList,
    'BABY_PROFILE_SETUP'
  >;
  route: RouteProp<SetupProfileStackParamList, 'BABY_PROFILE_SETUP'>;
}

function BabyProfileSetup({navigation, route}: BabyProfileSetupProps) {
  const {t} = I18N.useTranslation('translations');
  const {addBabyRequestState, updateBabyRequestState} = BabiesHook.useState();
  const dispatch = BabiesHook.useDispatch();

  const {userId, isEditting = false, baby} = route.params;

  const SelectorControllerRef = React.useRef(
    (props: SelectorControllerProps<GenderType>) =>
      SelectorController<GenderType>(props),
  );

  const originalPictureUri = React.useRef(
    (isEditting && baby && baby.pictureUri) || undefined,
  );

  const originalPictureFilename = React.useRef(
    (isEditting && baby && baby.pictureFilename) || undefined,
  );

  const [name, setName] = React.useState<null | string>(
    (isEditting && baby && baby.name) || null,
  );
  const [gender, setGender] = React.useState<null | GenderType>(
    (isEditting && baby && baby.gender) || null,
  );
  const [birthDate, setBirthDate] = React.useState<null | Date>(
    (isEditting && baby && (baby.birthDate as Date)) || null,
  );
  const [pictureUri, setImageUri] = React.useState<null | string>(
    (isEditting && baby && baby.pictureUri) || null,
  );

  const nameErrors = FormUtils.getRequiredTextfieldErrors(name);
  const pictureUriErrors = FormUtils.getRequiredTextfieldErrors(pictureUri);

  function canPressDoneButton() {
    if (nameErrors.hasError || name === null) {
      return false;
    }

    if (gender === null) {
      return false;
    }

    if (birthDate === null) {
      return false;
    }

    if (pictureUriErrors.hasError || pictureUri === null) {
      return false;
    }

    return true;
  }

  function onDoneButtonPressed() {
    if (!canPressDoneButton()) {
      return;
    }

    if (isEditting && baby) {
      BabiesHook.updateBaby(
        dispatch,
        baby.id,
        {
          ...baby,
          name: name!,
          gender: gender!,
          birthDate: birthDate!,
          pictureUri: pictureUri!,
        },
        originalPictureUri.current !== pictureUri,
        originalPictureFilename.current || null,
      );
    } else {
      const currentUser = AuthHook.getCurrentUser();

      if (currentUser === null) {
        return;
      }

      BabiesHook.addBaby(
        dispatch,
        {
          id: uuid(),
          name: name!,
          gender: gender!,
          birthDate: birthDate!,
          pictureFilename: '',
          pictureUri: pictureUri!,
          length: 0,
          weight: 0,
          parents: {[userId]: true},
        },
        userId,
      );
    }
  }

  React.useEffect(() => {
    if (addBabyRequestState.status === 'resolved') {
      dispatch({type: 'RESET_REQUEST', requestStateId: 'addBabyRequestState'});
      return navigation.goBack();
    }

    if (addBabyRequestState.status === 'rejected') {
      if (addBabyRequestState.errorCode === StorageUtils.ERROR_UPLOAD_FAILED) {
        Alert.alert(
          t('ALERT_TITLE_SOMETHING_WENT_WRONG'),
          t('ALERT_MESSAGE_SOMETHING_WENT_WRONG'),
          [{text: t('ALERT_BUTTON_OK')}],
        );

        dispatch({
          type: 'RESET_REQUEST',
          requestStateId: 'addBabyRequestState',
        });

        return navigation.goBack();
      }
    }
  }, [dispatch, addBabyRequestState, t, navigation]);

  React.useEffect(() => {
    if (updateBabyRequestState.status === 'resolved') {
      dispatch({
        type: 'RESET_REQUEST',
        requestStateId: 'updateBabyRequestState',
      });
      return navigation.goBack();
    }

    if (updateBabyRequestState.status === 'rejected') {
      if (
        updateBabyRequestState.errorCode === StorageUtils.ERROR_UPLOAD_FAILED
      ) {
        Alert.alert(
          t('ALERT_TITLE_SOMETHING_WENT_WRONG'),
          t('ALERT_MESSAGE_SOMETHING_WENT_WRONG'),
          [{text: t('ALERT_BUTTON_OK')}],
        );

        dispatch({
          type: 'RESET_REQUEST',
          requestStateId: 'updateBabyRequestState',
        });

        return navigation.goBack();
      }
    }
  }, [dispatch, updateBabyRequestState, t, navigation]);

  return (
    <ScreenWrapperWithKeyboardAV isScrollView={true} scrollVerticalOffset={80}>
      <View flex={1}>
        <TextInputController
          label={t('FORM_LABEL_BABY_S_NAME')}
          errorMsg={nameErrors.hasError ? t(nameErrors.errorMsg!) : ''}>
          <TextInput
            onChangeText={setName}
            value={name || ''}
            placeholder={t('FORM_PLACEHOLDER_NAME')}
            returnKeyType="done"
            isRequired
            hasError={nameErrors.hasError}
          />
        </TextInputController>

        <Padding sizeV={5} />

        <SelectorControllerRef.current
          label={t('FORM_LABEL_GENDER')}
          errorMsg=""
          selectors={[
            {label: t('GENDER_BOY'), value: 'boy'},
            {label: t('GENDER_GIRL'), value: 'girl'},
            {label: t('GENDER_D/S'), value: 'n/a'},
          ]}
          selected={gender}
          onChangeSelected={setGender}
        />

        <Padding sizeV={5} />

        <DateTimePickerController
          label={t('FORM_LABEL_BIRTH_DATE')}
          controllerMode="date"
          value={birthDate || new Date()}
          isNull={birthDate === null}
          onChangeDate={setBirthDate}
          maximumDate={new Date()}
          isRequired={true}
        />

        <Padding sizeV={5} />

        <ImagePickerController
          label={t('FORM_LABEL_BABY_S_PICTURE')}
          errorMsg={
            pictureUriErrors.hasError ? t(pictureUriErrors.errorMsg!) : ''
          }
          imageUri={pictureUri}
          onChangePickedImage={setImageUri}
        />

        <Padding sizeV={10} />

        <View push="pushBottom">
          <Button
            title={t('FORM_BUTTON_DONE').toUpperCase()}
            onPress={onDoneButtonPressed}
            disabled={!canPressDoneButton()}
            loading={
              (isEditting && updateBabyRequestState.status === 'pending') ||
              addBabyRequestState.status === 'pending'
            }
          />
        </View>
      </View>
    </ScreenWrapperWithKeyboardAV>
  );
}

export {BabyProfileSetup};

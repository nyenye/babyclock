import React from 'react';
import {View, Text} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {BottomTabNavigationProp} from '@react-navigation/bottom-tabs';

import {ScreenWrapper, Button, Padding} from '../components';
import {AuthHook, ThemeHook} from '../hooks';
import {TabParamList} from '../navigation/MainNavigator';
import {Layouts, Typography} from '../styles';

interface AccountProps {
  navigation: BottomTabNavigationProp<TabParamList, 'ACCOUNT'>;
  route: RouteProp<TabParamList, 'ACCOUNT'>;
}

function Account({}: AccountProps) {
  const dispatchAuth = AuthHook.useDispatch();
  const dispatchTheme = ThemeHook.useDispatch();

  function onThemeToggleButtonPressed() {
    dispatchTheme({type: 'TOGGLE_THEME'});
  }

  function onLogoutPressed() {
    AuthHook.logout(dispatchAuth);
  }

  return (
    <ScreenWrapper>
      <View style={[Layouts.expand, Layouts.centerContent]}>
        <Text style={[Typography.headings.h1, Typography.color.gray]}>
          Account
        </Text>
      </View>
      <View>
        <Button title="Toggle Theme" onPress={onThemeToggleButtonPressed} />
        <Padding sizeV={5} />
        <Button type="primary" title="Log Out" onPress={onLogoutPressed} />
      </View>
    </ScreenWrapper>
  );
}

export {Account};

import React from 'react';
import * as I18N from 'react-i18next';
import {TextInput as TextInputNative, Alert} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';

import Auth, {FirebaseAuthTypes} from '@react-native-firebase/auth';

import {
  Button,
  CheckBox,
  CheckBoxController,
  Padding,
  ScreenWrapperWithKeyboardAV,
  TextInput,
  TextInputController,
  View,
} from '../components';
import {InitStackParamList} from '../navigation/Stacks';

import {FormUtils, AuthUtils} from '../utils';

interface SignUpProps {
  navigation: StackNavigationProp<InitStackParamList, 'SIGN_UP'>;
}

function SignUp({}: SignUpProps) {
  const {t} = I18N.useTranslation('translations');

  const passwordInputRef = React.useRef<TextInputNative>();
  const passworConfirmationdInputRef = React.useRef<TextInputNative>();

  const [email, setEmail] = React.useState<null | string>(null);
  const [password, setPassword] = React.useState<null | string>(null);
  const [passwordConfirmation, setPasswordConfirmation] = React.useState<
    null | string
  >(null);
  const [tos, setToS] = React.useState<null | boolean>(null);

  const [isSignupLoading, setIsSignupLoading] = React.useState(false);

  function onSubmitNext(
    refToFocus: React.MutableRefObject<TextInputNative | undefined>,
  ) {
    if (refToFocus.current) {
      refToFocus.current.focus();
    }
  }

  function onSignupPressed() {
    setIsSignupLoading(true);

    Auth()
      .createUserWithEmailAndPassword(email!, password!)
      .then(function (_userCredentials: FirebaseAuthTypes.UserCredential) {
        setIsSignupLoading(false);
      })
      .catch(function (error) {
        setIsSignupLoading(false);

        const {code} = error;

        if (code === AuthUtils.ERROR_WEAK_PASSWORD) {
          Alert.alert(
            t('ALERT_TITLE_PASSWORD_TOO_WEAK'),
            t('ALERT_MESSAGE_PASSWORD_TOO_WEAK'),
            [{text: t('ALERT_BUTTON_OK')}],
          );
        } else if (code === AuthUtils.ERROR_EMAIL_ALREADY_IN_USE) {
          Alert.alert(
            t('ALERT_TITLE_EMAIL_ALREADY_IN_USE'),
            t('ALERT_MESSAGE_EMAIL_ALREADY_IN_USE'),
            [{text: t('ALERT_BUTTON_OK')}],
          );
        }
      });
  }

  const emailErros = FormUtils.getEmailErrors(email);

  const passwordErrors = FormUtils.getSignupPasswordErrors(
    password,
    passwordConfirmation,
  );

  const tosErrors = FormUtils.getRequiredCheckboxErrors(tos);

  function canSignup(): boolean {
    if (email === null || password === null || passwordConfirmation === null) {
      return false;
    }

    if (emailErros.hasError || passwordErrors.hasError || tosErrors.hasError) {
      return false;
    }

    if (!tos) {
      return false;
    }

    return true;
  }

  return (
    <ScreenWrapperWithKeyboardAV isScrollView={true} scrollVerticalOffset={80}>
      <View flex={1}>
        <TextInputController
          label={t('FORM_LABEL_ACCOUNT_EMAIL')}
          errorMsg={emailErros.hasError ? t(emailErros.errorMsg!) : ''}>
          <TextInput
            onChangeText={setEmail}
            value={email || ''}
            placeholder={t('EMAIL')}
            returnKeyType="next"
            isRequired
            onSubmitEditing={() => onSubmitNext(passwordInputRef)}
            hasError={emailErros.hasError}
          />
        </TextInputController>

        <Padding sizeV={5} />

        <TextInputController
          label={t('FORM_LABEL_ACCOUNT_PASSWORD')}
          errorMsg={passwordErrors.hasError ? t(passwordErrors.errorMsg!) : ''}>
          <TextInput
            forwardRef={passwordInputRef}
            onChangeText={setPassword}
            value={password || ''}
            placeholder={t('PASSWORD')}
            returnKeyType="next"
            secureTextEntry
            isRequired
            onSubmitEditing={() => onSubmitNext(passworConfirmationdInputRef)}
            hasError={passwordErrors.hasError}
          />

          <Padding sizeV={5} />

          <TextInput
            forwardRef={passworConfirmationdInputRef}
            onChangeText={setPasswordConfirmation}
            value={passwordConfirmation || ''}
            placeholder={t('PASSWORD_CONFIRMATION')}
            returnKeyType="done"
            secureTextEntry
            isRequired
            hasError={
              passwordErrors.hasError &&
              passwordErrors.errorMsg === 'FORM_ERROR_PASSWORDS_DONT_MATCH'
            }
          />
        </TextInputController>

        <Padding sizeV={5} />

        <CheckBoxController
          errorMsg={tosErrors.hasError ? t(tosErrors.errorMsg!) : ''}>
          <CheckBox
            value={tos === null ? false : tos}
            onValueChange={setToS}
            label={t('FORM_ACCEPT_TERMS_OF_SERVICE')}
          />
        </CheckBoxController>
      </View>

      <Padding sizeV={5} />

      <View>
        <Button
          type="primary"
          title={t('SIGN_UP').toUpperCase()}
          onPress={onSignupPressed}
          loading={isSignupLoading}
          disabled={!canSignup()}
        />
      </View>
    </ScreenWrapperWithKeyboardAV>
  );
}

export {SignUp};

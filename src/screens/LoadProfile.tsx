import React from 'react';
import {ActivityIndicator} from 'react-native';
import * as I18N from 'react-i18next';

import {ScreenWrapper, View, Text, Padding} from '../components';
import {ThemeHook, AuthHook} from '../hooks';
import {Colors} from '../styles';

interface LoadProfileProps {
  message: string;
}

function LoadProfile({message}: LoadProfileProps) {
  const {t} = I18N.useTranslation('translations');
  const {theme} = ThemeHook.useState();
  const dispatch = AuthHook.useDispatch();

  React.useState(() => {
    const currentUser = AuthHook.getCurrentUser();

    if (currentUser !== null) {
      setTimeout(() => {
        AuthHook.fetchCurrentUserProfile(dispatch, currentUser.uid);
      }, 0);
    }
  });

  return (
    <ScreenWrapper>
      <View flex={1} main="justifyCenter" cross="alignCenter">
        <ActivityIndicator
          color={Colors.activityIndicatorColors[theme]}
          size="large"
        />
        <Padding sizeV={7.5} />
        <Text>{t(message)}</Text>
      </View>
    </ScreenWrapper>
  );
}

export {LoadProfile};

import React from 'react';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

import * as I18N from 'react-i18next';

import {ScreenWrapper, View, Padding, HeadingBnW} from '../components';
import {Header, TrackingCards} from '../components/screens/Trackings';
import {ThemeHook} from '../hooks';
import {TrackingHookGetters, BabiesHookGetters} from '../hooks/getters';
import {TrackingsStackParamList} from '../navigation/Stacks';
import {Colors} from '../styles';

interface TrackingsProps {
  navigation: StackNavigationProp<TrackingsStackParamList, 'TRACKINGS'>;
  route: RouteProp<TrackingsStackParamList, 'TRACKINGS'>;
}

function Trackings({navigation}: TrackingsProps) {
  const {t} = I18N.useTranslation('translations');

  const {theme} = ThemeHook.useState();

  const {selectedBaby} = BabiesHookGetters.useGetters();
  const {
    ongoingTrackings,
    finishedTrackings,
  } = TrackingHookGetters.useGetters();

  if (
    selectedBaby === null ||
    !ongoingTrackings[selectedBaby.id] ||
    !finishedTrackings[selectedBaby.id]
  ) {
    return <View />;
  }

  const trackingCardProps = {
    lastSleep: finishedTrackings[selectedBaby.id].sleeping[0],
    lastFeed: finishedTrackings[selectedBaby.id].feeding[0],
    lastDiaper: finishedTrackings[selectedBaby.id].diaper[0],
    ongoingSleep: ongoingTrackings[selectedBaby.id].sleeping,
    ongoingFeed: ongoingTrackings[selectedBaby.id].feeding,
    ongoingDiaper: ongoingTrackings[selectedBaby.id].diaper,
  };

  return (
    <ScreenWrapper
      isScrollView={true}
      hasPadding={false}
      backgroundColor={Colors.backgroundColors.highlights[theme][1]}>
      <Header selectedBaby={selectedBaby} />

      <TrackingCards navigation={navigation} {...trackingCardProps} />

      <Padding sizeV={5} />

      <View padding={15}>
        <View direction="row" cross="alignEnd">
          <HeadingBnW type="h4">{t('COMMON_TODAY')}</HeadingBnW>
        </View>
      </View>
    </ScreenWrapper>
  );
}

export {Trackings};

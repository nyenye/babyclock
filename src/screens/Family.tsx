import React from 'react';
import {View, Text} from 'react-native';

import {ScreenWrapper} from '../components';
import {Layouts, Typography} from '../styles';

function Family() {
  return (
    <ScreenWrapper>
      <View style={[Layouts.expand, Layouts.centerContent]}>
        <Text style={[Typography.headings.h1, Typography.color.gray]}>
          Family
        </Text>
      </View>
    </ScreenWrapper>
  );
}

export {Family};

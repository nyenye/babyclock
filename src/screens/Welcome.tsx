import React from 'react';
import {
  StyleSheet,
  Image,
  TextInput as TextInputNative,
  Alert,
} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import * as I18N from 'react-i18next';

import Auth, {FirebaseAuthTypes} from '@react-native-firebase/auth';

import {
  Button,
  Padding,
  TextInput,
  Text,
  View,
  ScreenWrapperWithKeyboardAV,
} from '../components';

import {Colors, Typography} from '../styles';
import {InitStackParamList} from '../navigation/Stacks';
import {AuthUtils} from '../utils';

import WelcomeImage from '../assets/welcome.png';

const styles = StyleSheet.create({
  header: {
    alignItems: 'center',
  },
  title: {
    fontSize: 38,
    fontWeight: 'bold',
  },
  subtitle: {
    marginLeft: 120,
    color: Colors.tealDark,
  },
  image: {
    maxWidth: '80%',
    marginHorizontal: 'auto',
  },
});

interface WelcomeProps {
  navigation: StackNavigationProp<InitStackParamList, 'WELCOME'>;
}

function Welcome({navigation}: WelcomeProps) {
  const {t} = I18N.useTranslation('translations');

  const passwordInputRef = React.useRef<TextInputNative>();

  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');

  const [showEmailError, setShowEmailError] = React.useState(false);
  const [showPasswordError, setShowPasswordError] = React.useState(false);

  const [isLoginLoading, setIsLoginLoading] = React.useState(false);

  async function onSignupPressed() {
    navigation.navigate('SIGN_UP');
  }

  async function onLoginPressed() {
    setShowEmailError(false);
    setShowPasswordError(false);

    if (email.length === 0) {
      setShowEmailError(true);
      return;
    }
    if (password.length === 0) {
      setShowPasswordError(true);
      return;
    }

    setIsLoginLoading(true);

    Auth()
      .signInWithEmailAndPassword(email, password)
      .then(function (_userCredentials: FirebaseAuthTypes.UserCredential) {
        setIsLoginLoading(false);
      })
      .catch(function (error) {
        setIsLoginLoading(false);

        if (
          error.code === AuthUtils.ERROR_INVALID_EMIAL ||
          error.code === AuthUtils.ERROR_USER_NOT_FOUND ||
          error.code === AuthUtils.ERROR_WRONG_PASSWORD
        ) {
          Alert.alert(
            t('ALERT_TITLE_INVALID_CREDENTIALS'),
            t('ALERT_MESSAGE_INVALID_CREDENTIALS'),
            [{text: t('ALERT_BUTTON_OK')}],
          );
        }
      });
  }

  function onSubmitEditingEmail() {
    if (passwordInputRef.current) {
      passwordInputRef.current.focus();
    }
  }

  return (
    <ScreenWrapperWithKeyboardAV
      backgroundColor={Colors.tealLight}
      statusBarColor={Colors.tealLight}
      isScrollView={true}>
      <View style={styles.header}>
        <Text
          style={[
            styles.title,
            Typography.color.white,
            Typography.alignment.center,
          ]}>
          {t('APP_NAME')}
        </Text>

        <Text
          style={[
            styles.subtitle,
            Typography.text.bold,
            Typography.alignment.center,
          ]}>
          {t('APP_SLOGAN')}
        </Text>

        <Image
          source={WelcomeImage}
          style={styles.image}
          resizeMode="contain"
        />
      </View>

      <View flex={1} main="justifyCenter">
        <TextInput
          onChangeText={setEmail}
          value={email}
          placeholder={t('EMAIL')}
          keyboardType="email-address"
          returnKeyType="next"
          onSubmitEditing={onSubmitEditingEmail}
          hasError={showEmailError}
        />

        <Padding sizeV={5} />

        <TextInput
          forwardRef={passwordInputRef}
          onChangeText={setPassword}
          value={password}
          placeholder={t('PASSWORD')}
          returnKeyType="done"
          secureTextEntry
          onSubmitEditing={onLoginPressed}
          hasError={showPasswordError}
        />

        <Padding sizeV={5} />

        <View direction="row">
          <Button
            title={t('SIGN_UP').toUpperCase()}
            onPress={onSignupPressed}
            flex={1}
            type="outlined"
            color="white"
          />

          <Padding sizeH={5} />

          <Button
            type="primary"
            title={t('LOG_IN').toUpperCase()}
            onPress={onLoginPressed}
            loading={isLoginLoading}
            flex={1.5}
          />
        </View>
      </View>
    </ScreenWrapperWithKeyboardAV>
  );
}

export {Welcome};

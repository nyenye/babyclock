import React from 'react';
import {Platform, TouchableNativeFeedback, StyleSheet} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {Picker, PickerIOS} from '@react-native-community/picker';

import * as I18N from 'react-i18next';
import {v4 as uuid} from 'uuid';

import {
  ScreenWrapper,
  View,
  Text,
  DateTimePickerController,
  Padding,
  PickerController,
  TextInputController,
  TextArea,
  Button,
} from '../../../components';
import {ThemeHook, TrackingHook, BabiesHook} from '../../../hooks';
import {TrackingsStackParamList} from '../../../navigation/Stacks';
import {Colors} from '../../../styles';
import {TrackingUtils} from '../../../utils';
import {FinishedBreastFeedTracking} from '../../../types';

interface TrackingFormBreastFeedProps {
  navigation: StackNavigationProp<
    TrackingsStackParamList,
    'TRACKING_FORM_BREASTFEED'
  >;
  route: RouteProp<TrackingsStackParamList, 'TRACKING_FORM_BREASTFEED'>;
}

function TrackingFormBreastFeed({
  navigation,
  route,
}: TrackingFormBreastFeedProps) {
  const {t} = I18N.useTranslation('translations');

  const {theme} = ThemeHook.useState();

  const {selectedBabyId} = BabiesHook.useState();

  const trackingDispatch = TrackingHook.useDispatch();
  const {
    reportTrackingRequestState,
    updateTrackingRequestState,
  } = TrackingHook.useState();

  const {tracking, isEditting} = route.params;

  const [start, setStart] = React.useState<Date | null>(
    (isEditting && tracking?.start) || new Date(),
  );

  const [leftDurationInMinutes, setLeftDuration] = React.useState<number>(
    tracking?.leftDuration
      ? Math.min(60, Math.round(tracking.leftDuration / (1000 * 60)))
      : 0,
  );
  const [rightDurationInMinutes, setRightDuration] = React.useState<number>(
    tracking?.rightDuration
      ? Math.min(60, Math.round(tracking.rightDuration / (1000 * 60)))
      : 0,
  );

  const [notes, setNotes] = React.useState<string | null>(
    (isEditting && tracking?.notes) || null,
  );

  function setStartDate(date: Date) {
    const updateStart = new Date(start || Date.now());

    updateStart.setFullYear(date.getFullYear());
    updateStart.setMonth(date.getMonth());
    updateStart.setDate(date.getDate());

    if (start === null) {
      updateStart.setHours(0, 0, 0);
    }

    setStart(updateStart);
  }

  function setStartTime(date: Date) {
    const updateStart = new Date(start || Date.now());
    updateStart.setHours(date.getHours(), date.getMinutes(), date.getSeconds());

    setStart(updateStart);
  }

  function onLeftDurationChanged(
    itemValue: React.ReactText,
    itemIndex: number,
  ) {
    setLeftDuration(TrackingUtils.breastfeedDurationSelector[itemIndex].value);
  }

  function onRightDurationChanged(
    itemValue: React.ReactText,
    itemIndex: number,
  ) {
    setRightDuration(TrackingUtils.breastfeedDurationSelector[itemIndex].value);
  }

  function canSave() {
    console.log({
      selectedBabyId,
      start,
      leftDurationInMinutes,
      rightDurationInMinutes,
      check: leftDurationInMinutes === 0 && rightDurationInMinutes === 0,
    });

    if (selectedBabyId === null) {
      return false;
    }

    if (start === null) {
      return false;
    }

    if (leftDurationInMinutes === 0 && rightDurationInMinutes === 0) {
      return false;
    }

    return true;
  }

  function onSave() {
    if (selectedBabyId === null || start === null) {
      return;
    }

    if (isEditting && tracking === undefined) {
      return;
    }

    const leftDuration = leftDurationInMinutes * 60 * 1000;
    const rightDuration = rightDurationInMinutes * 60 * 1000;

    const leftTimer = {
      start: new Date(start.getTime()),
      end: new Date(start.getTime() + leftDuration),
    };

    const rightTimer = {
      start: new Date(leftTimer.start.getTime()),
      end: new Date(leftTimer.start.getTime() + rightDuration),
    };

    // TODO: Handle that end is not in the future? Make user change start?
    const end = new Date(rightTimer.end.getTime());

    if (isEditting && tracking !== undefined) {
      const edittedTracking: FinishedBreastFeedTracking = {
        ...tracking,
        start,
        end,
        leftTimers: [leftTimer],
        leftDuration,
        rightTimers: [rightTimer],
        rightDuration,
      };

      return TrackingHook.updateTracking(
        trackingDispatch,
        selectedBabyId,
        edittedTracking,
      );
    }

    // Add Sleep
    const newTracking: FinishedBreastFeedTracking = {
      id: uuid(),
      type: 'feeding',
      mode: 'breast',
      start,
      end,
      leftTimers: [leftTimer],
      leftDuration,
      rightTimers: [rightTimer],
      rightDuration,
    };

    TrackingHook.reportTrackingBreastfeed(
      trackingDispatch,
      selectedBabyId,
      newTracking,
    );
  }

  const dynamicStyles = StyleSheet.create({
    picker: {
      color: Colors.textColors[theme],
    },
  });

  React.useEffect(() => {
    if (reportTrackingRequestState.status === 'resolved') {
      trackingDispatch({
        type: 'RESET_REQUEST',
        requestStateId: 'reportTrackingRequestState',
      });
      navigation.goBack();
    } else if (reportTrackingRequestState.status === 'rejected') {
      // TODO: Show Alert
    }
  }, [navigation, trackingDispatch, reportTrackingRequestState]);

  React.useEffect(() => {
    if (updateTrackingRequestState.status === 'resolved') {
      trackingDispatch({
        type: 'RESET_REQUEST',
        requestStateId: 'updateTrackingRequestState',
      });
      navigation.goBack();
    } else if (updateTrackingRequestState.status === 'rejected') {
      // TODO: Show Alert
    }
  }, [navigation, trackingDispatch, updateTrackingRequestState]);

  React.useLayoutEffect(() => {
    function deleteTracking() {}

    navigation.setOptions({
      title: isEditting
        ? t('SCREEN_TITLE_TRACKING_EDIT_BREASTFEED')
        : t('SCREEN_TITLE_TRACKING_ADD_BREASTFEED'),
      headerRight: isEditting
        ? () => (
            <View padding={15}>
              <TouchableNativeFeedback onPress={deleteTracking}>
                <Text weight="bold" color="red">
                  Delete
                </Text>
              </TouchableNativeFeedback>
            </View>
          )
        : undefined,
    });
  }, [navigation, t, isEditting]);

  const isLoading =
    reportTrackingRequestState.status === 'pending' ||
    updateTrackingRequestState.status === 'pending';

  return (
    <ScreenWrapper isScrollView hasPadding>
      <View direction="row">
        <View flex={1}>
          <DateTimePickerController
            label={t('FORM_LABEL_TRACKING_SLEEP_START')}
            controllerMode="date"
            value={start || new Date()}
            isNull={start === null}
            onChangeDate={setStartDate}
            maximumDate={new Date()}
            disabled={isLoading}
          />
        </View>

        <Padding sizeH={7.5} />

        <View flex={1}>
          <DateTimePickerController
            label=""
            controllerMode="time"
            value={start || new Date()}
            isNull={start === null}
            onChangeDate={setStartTime}
            disabled={isLoading}
          />
        </View>
      </View>

      <Padding sizeV={5} />

      <View direction="row">
        <View flex={1}>
          <PickerController
            label={t('FORM_LABEL_TRACKING_BREASTFEED_LEFT_BREAST')}
            errorMsg="">
            {(Platform.OS === 'android' && (
              <Picker
                selectedValue={leftDurationInMinutes}
                onValueChange={onLeftDurationChanged}
                style={dynamicStyles.picker}>
                {TrackingUtils.breastfeedDurationSelector.map((item) => {
                  return (
                    <Picker.Item
                      key={item.label}
                      label={
                        item.label === 0
                          ? t(
                              'FORM_LABEL_TRACKING_BREASTFEED_DURATION_PICKER_NOT_OFFERED',
                            )
                          : t(
                              'FORM_LABEL_TRACKING_BREASTFEED_DURATION_PICKER',
                              {count: item.label},
                            )
                      }
                      value={item.value}
                    />
                  );
                })}
              </Picker>
            )) || (
              <PickerIOS
                selectedValue={leftDurationInMinutes}
                onValueChange={onLeftDurationChanged}
                style={dynamicStyles.picker}>
                {TrackingUtils.breastfeedDurationSelector.map((item) => {
                  return (
                    <Picker.Item
                      key={item.label}
                      label={
                        item.label === 0
                          ? t(
                              'FORM_LABEL_TRACKING_BREASTFEED_DURATION_PICKER_NOT_OFFERED',
                            )
                          : t(
                              'FORM_LABEL_TRACKING_BREASTFEED_DURATION_PICKER',
                              {count: item.label},
                            )
                      }
                      value={item.value}
                    />
                  );
                })}
              </PickerIOS>
            )}
          </PickerController>
        </View>

        <Padding sizeH={7.5} />

        <View flex={1}>
          <PickerController
            label={t('FORM_LABEL_TRACKING_BREASTFEED_RIGHT_BREAST')}
            errorMsg="">
            {(Platform.OS === 'android' && (
              <Picker
                selectedValue={rightDurationInMinutes}
                onValueChange={onRightDurationChanged}
                style={dynamicStyles.picker}>
                {TrackingUtils.breastfeedDurationSelector.map((item) => {
                  return (
                    <Picker.Item
                      key={item.label}
                      label={
                        item.label === 0
                          ? t(
                              'FORM_LABEL_TRACKING_BREASTFEED_DURATION_PICKER_NOT_OFFERED',
                            )
                          : t(
                              'FORM_LABEL_TRACKING_BREASTFEED_DURATION_PICKER',
                              {count: item.label},
                            )
                      }
                      value={item.value}
                    />
                  );
                })}
              </Picker>
            )) || (
              <PickerIOS
                selectedValue={rightDurationInMinutes}
                onValueChange={onRightDurationChanged}
                style={dynamicStyles.picker}>
                {TrackingUtils.breastfeedDurationSelector.map((item) => {
                  return (
                    <Picker.Item
                      key={item.label}
                      label={
                        item.label === 0
                          ? t(
                              'FORM_LABEL_TRACKING_BREASTFEED_DURATION_PICKER_NOT_OFFERED',
                            )
                          : t(
                              'FORM_LABEL_TRACKING_BREASTFEED_DURATION_PICKER',
                              {count: item.label},
                            )
                      }
                      value={item.value}
                    />
                  );
                })}
              </PickerIOS>
            )}
          </PickerController>
        </View>
      </View>

      <Padding sizeV={5} />

      <TextInputController
        label={t('FORM_LABEL_TRACKING_BREASTFEED_NOTES')}
        errorMsg="">
        <TextArea
          onChangeText={setNotes}
          value={notes || ''}
          placeholder={t('FORM_PLACEHOLDER_TRACKING_BREASTFEED_NOTES')}
          returnKeyType="done"
          rows="three"
          hasError={false}
        />
      </TextInputController>

      <View push="pushBottom" />

      <Button
        title={t('FORM_LABEL_TRACKING_BREASTFEED_SUBMMIT')}
        onPress={onSave}
        disabled={!canSave()}
        loading={isLoading}
      />
    </ScreenWrapper>
  );
}

export {TrackingFormBreastFeed};

import React from 'react';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

import * as I18N from 'react-i18next';

import {
  ScreenWrapper,
  View,
  DateTimePickerController,
  Padding,
  TextInputController,
  TextArea,
  Button,
} from '../../../components';
import {DiaperModeSelector} from '../../../components/screens/Trackings/Diapers';
import {TrackingsStackParamList} from '../../../navigation/Stacks';
import {DiaperMode} from '../../../types';

interface TrackingFormDiaperProps {
  navigation: StackNavigationProp<
    TrackingsStackParamList,
    'TRACKING_FORM_DIAPER'
  >;
  route: RouteProp<TrackingsStackParamList, 'TRACKING_FORM_DIAPER'>;
}

function TrackingFormDiaper({navigation}: TrackingFormDiaperProps) {
  const {t} = I18N.useTranslation('translations');

  const [datetime, setDatetime] = React.useState<Date | null>();

  const [mode, setMode] = React.useState<DiaperMode>('pee');

  const [notes, setNotes] = React.useState<string | null>();

  function setDate(date: Date) {
    const updatedDatetime = new Date(datetime || Date.now());

    updatedDatetime.setFullYear(date.getFullYear());
    updatedDatetime.setMonth(date.getMonth());
    updatedDatetime.setDate(date.getDate());

    if (datetime === null) {
      updatedDatetime.setHours(0, 0, 0);
    }

    setDatetime(updatedDatetime);
  }

  function setTime(date: Date) {
    const updatedDatetime = new Date(datetime || Date.now());
    updatedDatetime.setHours(
      date.getHours(),
      date.getMinutes(),
      date.getSeconds(),
    );

    setDatetime(updatedDatetime);
  }

  function canSave() {
    if (datetime === null) {
      return false;
    }

    return true;
  }

  function onSave() {
    // Report Diaper
  }

  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: t('SCREEN_TITLE_TRACKING_ADD_DIAPER'),
    });
  }, [navigation, t]);

  return (
    <ScreenWrapper isScrollView hasPadding>
      <View direction="row">
        <View flex={1}>
          <DateTimePickerController
            label={t('FORM_LABEL_TRACKING_DIAPER_DATETIME')}
            controllerMode="date"
            value={datetime || new Date()}
            isNull={datetime === null}
            onChangeDate={setDate}
            maximumDate={new Date()}
          />
        </View>

        <Padding sizeH={7.5} />

        <View flex={1}>
          <DateTimePickerController
            label=""
            controllerMode="time"
            value={datetime || new Date()}
            isNull={datetime === null}
            onChangeDate={setTime}
          />
        </View>
      </View>

      <Padding sizeV={5} />

      <DiaperModeSelector selectedMode={mode} onChange={setMode} />

      <Padding sizeV={5} />

      <TextInputController
        label={t('FORM_LABEL_TRACKING_DIAPER_NOTES')}
        errorMsg="">
        <TextArea
          onChangeText={setNotes}
          value={notes || ''}
          placeholder={t('FORM_PLACEHOLDER_TRACKING_DIAPER_NOTES')}
          returnKeyType="done"
          rows="three"
          hasError={false}
        />
      </TextInputController>

      <View push="pushBottom" />

      <Button
        title={t('FORM_LABEL_TRACKING_DIAPER_SUBMMIT')}
        color="pink"
        onPress={onSave}
        disabled={canSave()}
      />
    </ScreenWrapper>
  );
}

export {TrackingFormDiaper};

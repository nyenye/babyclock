import React from 'react';
import {TouchableNativeFeedback, Alert} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

import * as I18N from 'react-i18next';
import {v4 as uuid} from 'uuid';

import {
  ScreenWrapper,
  View,
  Text,
  DateTimePickerController,
  Padding,
  TextInputController,
  TextArea,
  Button,
} from '../../../components';
import {SleepModeSelector} from '../../../components/screens/Trackings/Sleeping';
import {BabiesHook, TrackingHook} from '../../../hooks';
import {TrackingsStackParamList} from '../../../navigation/Stacks';
import {SleepingMode, FinishedSleepTracking} from '../../../types';

interface TrackingFormSleepProps {
  navigation: StackNavigationProp<
    TrackingsStackParamList,
    'TRACKING_FORM_SLEEP'
  >;
  route: RouteProp<TrackingsStackParamList, 'TRACKING_FORM_SLEEP'>;
}

function TrackingFormSleep({navigation, route}: TrackingFormSleepProps) {
  const {t} = I18N.useTranslation('translations');

  const {selectedBabyId} = BabiesHook.useState();

  const trackingDispatch = TrackingHook.useDispatch();
  const {
    reportTrackingRequestState,
    updateTrackingRequestState,
  } = TrackingHook.useState();

  const {tracking, isEditting} = route.params;

  const [start, setStart] = React.useState<Date | null>(
    (isEditting && tracking?.start) || null,
  );
  const [end, setEnd] = React.useState<Date | null>(
    (isEditting && tracking?.end) || null,
  );

  // const [duration, setDuration] = React.useState<number | null>(
  //   start && end ? end.getTime() - start.getTime() : null,
  // );
  const [modes, setModes] = React.useState<Record<SleepingMode, boolean>>(
    tracking?.modes || {
      car: false,
      crib: false,
      held: false,
      nursing: false,
      stroller: false,
    },
  );

  const [notes, setNotes] = React.useState<string | null>(
    (isEditting && tracking?.notes) || null,
  );

  function setStartDate(date: Date) {
    const updateStart = new Date(start || Date.now());

    updateStart.setFullYear(date.getFullYear());
    updateStart.setMonth(date.getMonth());
    updateStart.setDate(date.getDate());

    if (start === null) {
      updateStart.setHours(0, 0, 0);
    }

    setStart(updateStart);
  }

  function setStartTime(date: Date) {
    const updateStart = new Date(start || Date.now());
    updateStart.setHours(date.getHours(), date.getMinutes(), date.getSeconds());

    if (end !== null) {
      if (end.getTime() - updateStart!.getTime() < 0) {
        return Alert.alert(
          t('ALERT_TITLE_END_TIME_IS_BEFORE_START_TIME'),
          t('ALERT_MESSAGE_END_TIME_IS_BEFORE_START_TIME'),
        );
      }
    }

    setStart(updateStart);
  }

  function setEndDate(date: Date) {
    const updateEnd = new Date(end || Date.now());

    updateEnd.setFullYear(date.getFullYear());
    updateEnd.setMonth(date.getMonth());
    updateEnd.setDate(date.getDate());

    if (end === null) {
      updateEnd.setHours(
        start!.getHours(),
        start!.getMinutes(),
        start!.getSeconds(),
      );
    }

    setEnd(updateEnd);
  }

  function setEndTime(date: Date) {
    const updateEnd = new Date(end || Date.now());
    updateEnd.setHours(date.getHours(), date.getMinutes(), date.getSeconds());

    if (updateEnd.getTime() - start!.getTime() < 0) {
      return Alert.alert(
        t('ALERT_TITLE_END_TIME_IS_BEFORE_START_TIME'),
        t('ALERT_MESSAGE_END_TIME_IS_BEFORE_START_TIME'),
      );
    }

    setEnd(updateEnd);
  }

  function canSave() {
    if (start === null || end === null) {
      return false;
    }

    if (end.getTime() - start.getTime() < 0) {
      return false;
    }

    if (
      Object.keys(modes).filter((key) => modes[key as SleepingMode] === true)
        .length === 0
    ) {
      return false;
    }

    return true;
  }

  function onSave() {
    if (selectedBabyId === null || start === null || end === null) {
      return;
    }

    if (isEditting && tracking !== undefined) {
      const edittedTracking: FinishedSleepTracking = {
        id: tracking.id,
        type: 'sleeping',
        start,
        end,
        modes,
        notes,
      };

      return TrackingHook.updateTracking(
        trackingDispatch,
        selectedBabyId,
        edittedTracking,
      );
    }

    // Add Sleep
    const newTracking: FinishedSleepTracking = {
      id: uuid(),
      type: 'sleeping',
      start,
      end,
      modes,
      notes,
    };

    TrackingHook.reportTrackingSleep(
      trackingDispatch,
      selectedBabyId,
      newTracking,
    );
  }

  React.useEffect(() => {
    if (reportTrackingRequestState.status === 'resolved') {
      trackingDispatch({
        type: 'RESET_REQUEST',
        requestStateId: 'reportTrackingRequestState',
      });
      navigation.goBack();
    } else if (reportTrackingRequestState.status === 'rejected') {
      // TODO: Show Alert
    }
  }, [navigation, trackingDispatch, reportTrackingRequestState]);

  React.useEffect(() => {
    if (updateTrackingRequestState.status === 'resolved') {
      trackingDispatch({
        type: 'RESET_REQUEST',
        requestStateId: 'updateTrackingRequestState',
      });
      navigation.goBack();
    } else if (updateTrackingRequestState.status === 'rejected') {
      // TODO: Show Alert
    }
  }, [navigation, trackingDispatch, updateTrackingRequestState]);

  React.useLayoutEffect(() => {
    function deleteTracking() {}

    navigation.setOptions({
      title: isEditting
        ? t('SCREEN_TITLE_TRACKING_EDIT_SLEEP')
        : t('SCREEN_TITLE_TRACKING_ADD_SLEEP'),
      headerRight: isEditting
        ? () => (
            <View padding={15}>
              <TouchableNativeFeedback onPress={deleteTracking}>
                <Text weight="bold" color="red">
                  Delete
                </Text>
              </TouchableNativeFeedback>
            </View>
          )
        : undefined,
    });
  }, [navigation, t, isEditting]);

  const isLoading =
    reportTrackingRequestState.status === 'pending' ||
    updateTrackingRequestState.status === 'pending';

  return (
    <ScreenWrapper isScrollView hasPadding>
      <View direction="row">
        <View flex={1}>
          <DateTimePickerController
            label={t('FORM_LABEL_TRACKING_SLEEP_START')}
            controllerMode="date"
            value={start || new Date()}
            isNull={start === null}
            onChangeDate={setStartDate}
            maximumDate={new Date()}
          />
        </View>

        <Padding sizeH={7.5} />

        <View flex={1}>
          <DateTimePickerController
            label=""
            controllerMode="time"
            value={start || new Date()}
            isNull={start === null}
            onChangeDate={setStartTime}
          />
        </View>
      </View>

      <Padding sizeV={5} />

      <View direction="row">
        <View flex={1}>
          <DateTimePickerController
            label={t('FORM_LABEL_TRACKING_SLEEP_END')}
            controllerMode="date"
            value={end || new Date()}
            isNull={end === null}
            onChangeDate={setEndDate}
            minimumDate={start || new Date()}
            maximumDate={new Date()}
          />
        </View>

        <Padding sizeH={7.5} />

        <View flex={1}>
          <DateTimePickerController
            label=""
            controllerMode="time"
            value={end || new Date()}
            isNull={end === null}
            onChangeDate={setEndTime}
          />
        </View>
      </View>

      <Padding sizeV={5} />

      <SleepModeSelector selectedModes={modes} onChange={setModes} />

      <Padding sizeV={5} />

      <TextInputController
        label={t('FORM_LABEL_TRACKING_SLEEP_NOTES')}
        errorMsg="">
        <TextArea
          onChangeText={setNotes}
          value={notes || ''}
          placeholder={t('FORM_PLACEHOLDER_TRACKING_SLEEP_NOTES')}
          returnKeyType="done"
          rows="three"
          hasError={false}
        />
      </TextInputController>

      <View push="pushBottom" />

      <Button
        title={t('FORM_LABEL_TRACKING_SLEEP_SUBMMIT')}
        onPress={onSave}
        disabled={!canSave()}
        loading={isLoading}
      />
    </ScreenWrapper>
  );
}

export {TrackingFormSleep};

import React from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

import {View, Padding} from '../../components';
import {DiaperTracker} from '../../components/screens/Trackings/Trackers/DiaperTracker';
import {BabiesHookGetters, TrackingHookGetters} from '../../hooks/getters';
import {TrackingsStackParamList} from '../../navigation/Stacks';

const styles = StyleSheet.create({
  container: {},
  scrollViewContainer: {
    minHeight: '100%',
  },
});

interface DiaperTrackingTabProps {
  navigation: StackNavigationProp<TrackingsStackParamList, 'TRACKINGS_DETAILS'>;
  route: RouteProp<TrackingsStackParamList, 'TRACKINGS_DETAILS'>;
}

function DiaperTrackingTab({navigation}: DiaperTrackingTabProps) {
  const {selectedBaby} = BabiesHookGetters.useGetters();
  const {finishedTrackings} = TrackingHookGetters.useGetters();

  function goToFormScreen() {
    navigation.navigate('TRACKING_FORM_DIAPER');
  }

  if (selectedBaby === null || !finishedTrackings[selectedBaby.id]) {
    return <View />;
  }

  // const finishedDiaperTrackings: Array<FinishedDiaperTracking> = finishedTrackings[
  //   selectedBaby.id
  // ].sleeping as FinishedDiaperTracking[];

  return (
    <View flex={1}>
      <ScrollView contentContainerStyle={styles.scrollViewContainer}>
        <Padding sizeV={30} />

        <View padding={15}>
          <DiaperTracker
            babyId={selectedBaby.id}
            goToFormScreen={goToFormScreen}
          />
        </View>
      </ScrollView>
    </View>
  );
}

export {DiaperTrackingTab};

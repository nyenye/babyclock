import React from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

import {View, Padding, Divider} from '../../components';
import {
  FeedingEntryList,
  FeedingModeSelector,
} from '../../components/screens/Trackings/Feeding';
import {
  BreastFeedingTracker,
  BottleFeedingTracker,
} from '../../components/screens/Trackings/Trackers/FeedingTrackers';

import {BabiesHookGetters, TrackingHookGetters} from '../../hooks/getters';
import {TrackingsStackParamList} from '../../navigation/Stacks';
import {
  OngoingFeedTracking,
  OngoingBreastFeedTracking,
  OngoingBottleFeedTracking,
  FeedingMode,
  FinishedFeedTracking,
  FinishedBreastFeedTracking,
  FinishedBottleFeedTracking,
} from '../../types';

const styles = StyleSheet.create({
  container: {},
  scrollViewContainer: {
    minHeight: '100%',
  },
});

interface FeedTrackingTabProps {
  navigation: StackNavigationProp<TrackingsStackParamList, 'TRACKINGS_DETAILS'>;
  route: RouteProp<TrackingsStackParamList, 'TRACKINGS_DETAILS'>;
}

function FeedTrackingTab({navigation}: FeedTrackingTabProps) {
  const {selectedBaby} = BabiesHookGetters.useGetters();
  const {
    ongoingTrackings,
    finishedTrackings,
  } = TrackingHookGetters.useGetters();

  const [feedingMode, setFeedingMode] = React.useState<FeedingMode>('breast');

  function goToBreastFeedFormScreen(
    tracking?: FinishedBreastFeedTracking,
    isEditting: boolean = false,
  ) {
    navigation.navigate('TRACKING_FORM_BREASTFEED', {tracking, isEditting});
  }

  function goToBottleFeedFormScreen(
    tracking?: FinishedBottleFeedTracking,
    isEditting: boolean = false,
  ) {
    navigation.navigate('TRACKING_FORM_BOTTLEFEED', {tracking, isEditting});
  }

  if (
    selectedBaby === null ||
    !ongoingTrackings[selectedBaby.id] ||
    !finishedTrackings[selectedBaby.id]
  ) {
    return <View />;
  }

  const ongoingTracking: OngoingFeedTracking | undefined =
    (ongoingTrackings[selectedBaby.id].feeding as OngoingFeedTracking) ||
    undefined;

  const finishedFeedTrackings: Array<FinishedFeedTracking> = finishedTrackings[
    selectedBaby.id
  ].feeding as FinishedFeedTracking[];

  // No ongoing feed tracker for selected baby
  if (ongoingTracking === undefined) {
    return (
      <View flex={1}>
        <ScrollView contentContainerStyle={styles.scrollViewContainer}>
          <Padding sizeV={10} />

          <View padding={15}>
            <FeedingModeSelector
              onChange={setFeedingMode}
              currentMode={feedingMode}
            />

            <Padding sizeV={7.5} />
            {feedingMode === 'breast' ? (
              <BreastFeedingTracker
                babyId={selectedBaby.id}
                tracking={undefined}
                goToFormScreen={goToBreastFeedFormScreen}
              />
            ) : (
              <BottleFeedingTracker
                babyId={selectedBaby.id}
                tracking={undefined}
                goToFormScreen={goToBottleFeedFormScreen}
              />
            )}
          </View>

          <Divider highlight={2} size={0.5} margin={15} />

          <View padding={15}>
            <FeedingEntryList
              trackings={finishedFeedTrackings}
              goToBreastFeedFormScreen={goToBreastFeedFormScreen}
              goToBottleFeedFormScreen={goToBottleFeedFormScreen}
            />
          </View>
        </ScrollView>
      </View>
    );
  }

  // Ongoing breastfeed tracker for selected baby
  if (ongoingTracking.mode === 'breast') {
    return (
      <View flex={1}>
        <ScrollView contentContainerStyle={styles.scrollViewContainer}>
          <Padding sizeV={30} />

          <View padding={15}>
            <BreastFeedingTracker
              babyId={selectedBaby.id}
              tracking={ongoingTracking as OngoingBreastFeedTracking}
              goToFormScreen={goToBreastFeedFormScreen}
            />
          </View>

          <Divider highlight={2} size={0.5} margin={15} />

          <View padding={15}>
            <FeedingEntryList
              trackings={finishedFeedTrackings}
              goToBreastFeedFormScreen={goToBreastFeedFormScreen}
              goToBottleFeedFormScreen={goToBottleFeedFormScreen}
            />
          </View>
        </ScrollView>
      </View>
    );
  }

  // Ongoing bottle feed tracker for selected baby
  return (
    <View flex={1}>
      <ScrollView contentContainerStyle={styles.scrollViewContainer}>
        <Padding sizeV={30} />

        <View padding={15}>
          <BottleFeedingTracker
            babyId={selectedBaby.id}
            tracking={ongoingTracking as OngoingBottleFeedTracking}
            goToFormScreen={goToBottleFeedFormScreen}
          />
        </View>

        <Divider highlight={2} size={0.5} margin={15} />

        <View padding={15}>
          <FeedingEntryList
            trackings={finishedFeedTrackings}
            goToBreastFeedFormScreen={goToBreastFeedFormScreen}
            goToBottleFeedFormScreen={goToBottleFeedFormScreen}
          />
        </View>
      </ScrollView>
    </View>
  );
}

export {FeedTrackingTab};

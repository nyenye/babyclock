import React from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

import {View, Padding} from '../../components';
import {SleepTracker} from '../../components/screens/Trackings/Trackers/SleepingTracker';
import {BabiesHookGetters, TrackingHookGetters} from '../../hooks/getters';
import {TrackingsStackParamList} from '../../navigation/Stacks';
import {OngoingSleepTracking, FinishedSleepTracking} from '../../types';

const styles = StyleSheet.create({
  container: {},
  scrollViewContainer: {
    minHeight: '100%',
  },
});

interface SleepTrackingTabProps {
  navigation: StackNavigationProp<TrackingsStackParamList, 'TRACKINGS_DETAILS'>;
  route: RouteProp<TrackingsStackParamList, 'TRACKINGS_DETAILS'>;
}

function SleepTrackingTab({navigation}: SleepTrackingTabProps) {
  const {selectedBaby} = BabiesHookGetters.useGetters();
  const {
    ongoingTrackings,
    finishedTrackings,
  } = TrackingHookGetters.useGetters();

  function goToFormScreen(
    tracking?: FinishedSleepTracking,
    isEditting: boolean = false,
  ) {
    navigation.navigate('TRACKING_FORM_SLEEP', {tracking, isEditting});
  }

  if (
    selectedBaby === null ||
    !ongoingTrackings[selectedBaby.id] ||
    !finishedTrackings[selectedBaby.id]
  ) {
    return <View />;
  }

  const ongoingTracking: OngoingSleepTracking | undefined =
    (ongoingTrackings[selectedBaby.id].sleeping as OngoingSleepTracking) ||
    undefined;

  return (
    <View flex={1}>
      <ScrollView contentContainerStyle={styles.scrollViewContainer}>
        <Padding sizeV={30} />

        <View padding={15}>
          <SleepTracker
            babyId={selectedBaby.id}
            tracking={ongoingTracking}
            goToFormScreen={goToFormScreen}
          />
        </View>
      </ScrollView>
    </View>
  );
}

export {SleepTrackingTab};

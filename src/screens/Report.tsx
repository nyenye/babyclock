import React from 'react';

import {Storage} from '../storage';
import {
  ScreenWrapper,
  WeekSelector,
  WeekSchedule,
  Padding,
} from '../components';
import {ReportHook} from '../hooks';

function Report() {
  const schdeuleDetailsDispatch = ReportHook.useDispatch();

  async function checkLastStartOfWeek() {
    const lastStartOfWeek = await Storage.get(
      '@scheduleDetails/lastStartOfWeek',
    );

    if (lastStartOfWeek === null) {
      return;
    }

    schdeuleDetailsDispatch({
      type: 'SET_WEEK',
      currentStartOfWeek: lastStartOfWeek,
    });
  }

  React.useEffect(() => {
    checkLastStartOfWeek();
  });

  return (
    <ScreenWrapper isScrollView={true}>
      <WeekSelector />
      <Padding size={7.5} />
      <WeekSchedule />
      <Padding size={7.5} />
      {/* <TrackingFilters /> */}
    </ScreenWrapper>
  );
}

function ProvidedReport() {
  return (
    <ReportHook.Provider>
      <Report />
    </ReportHook.Provider>
  );
}

export {ProvidedReport as Report};

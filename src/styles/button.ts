import {StyleSheet} from 'react-native';
import * as Colors from './colors';

export const common = StyleSheet.create({
  disabled: {
    opacity: 0.5,
  },
  outterContainer: {
    borderRadius: 8,
    overflow: 'hidden',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
  },
  rounded: {
    borderRadius: 100,
  },
  text: {
    fontWeight: 'bold',
  },
});

export const size = {
  small: StyleSheet.create({
    container: {
      height: 35,
    },
    text: {
      fontSize: 14,
    },
    icon: {
      fontSize: 16,
    },
  }),
  medium: StyleSheet.create({
    container: {
      height: 42,
    },
    text: {
      fontSize: 16,
    },
    icon: {
      fontSize: 18,
    },
  }),
  large: StyleSheet.create({
    container: {
      height: 55,
    },
    text: {
      fontSize: 22,
    },
    icon: {
      fontSize: 24,
    },
  }),
  xl: StyleSheet.create({
    container: {
      height: 55,
    },
    text: {
      fontSize: 22,
    },
    icon: {
      fontSize: 24,
    },
  }),
};

export const padding = StyleSheet.create({
  small: {
    paddingHorizontal: 10,
  },
  medium: {
    paddingHorizontal: 22,
  },
  large: {
    paddingHorizontal: 30,
  },
  xl: {
    paddingHorizontal: 38,
  },
});

export const primary = {
  white: StyleSheet.create({
    container: {
      backgroundColor: Colors.white,
    },
    text: {
      color: Colors.teal,
    },
  }),
  teal: StyleSheet.create({
    container: {
      backgroundColor: Colors.teal,
    },
    text: {
      color: Colors.white,
    },
  }),
  green: StyleSheet.create({
    container: {
      backgroundColor: Colors.green,
    },
    text: {
      color: Colors.white,
    },
  }),
  orange: StyleSheet.create({
    container: {
      backgroundColor: Colors.orange,
    },
    text: {
      color: Colors.white,
    },
  }),
  pink: StyleSheet.create({
    container: {
      backgroundColor: Colors.pink,
    },
    text: {
      color: Colors.white,
    },
  }),
  grayLight: StyleSheet.create({
    container: {
      backgroundColor: Colors.grayLight,
    },
    text: {
      color: Colors.white,
    },
  }),
  gray: StyleSheet.create({
    container: {
      backgroundColor: Colors.gray,
    },
    text: {
      color: Colors.white,
    },
  }),
  grayDark: StyleSheet.create({
    container: {
      backgroundColor: Colors.grayDark,
    },
    text: {
      color: Colors.white,
    },
  }),
};

export const outlined = {
  white: StyleSheet.create({
    container: {
      backgroundColor: Colors.transparent,
      borderWidth: 2,
      borderColor: Colors.white,
    },
    text: {
      color: Colors.white,
    },
  }),
  teal: StyleSheet.create({
    container: {
      backgroundColor: Colors.transparent,
      borderWidth: 2,
      borderColor: Colors.teal,
    },
    text: {
      color: Colors.teal,
    },
  }),
  green: StyleSheet.create({
    container: {
      backgroundColor: Colors.transparent,
      borderWidth: 2,
      borderColor: Colors.green,
    },
    text: {
      color: Colors.green,
    },
  }),
  orange: StyleSheet.create({
    container: {
      backgroundColor: Colors.transparent,
      borderWidth: 2,
      borderColor: Colors.orange,
    },
    text: {
      color: Colors.orange,
    },
  }),
  pink: StyleSheet.create({
    container: {
      backgroundColor: Colors.transparent,
      borderWidth: 2,
      borderColor: Colors.pink,
    },
    text: {
      color: Colors.pink,
    },
  }),
  grayLight: StyleSheet.create({
    container: {
      backgroundColor: Colors.transparent,
      borderWidth: 2,
      borderColor: Colors.grayLight,
    },
    text: {
      color: Colors.grayLight,
    },
  }),
  gray: StyleSheet.create({
    container: {
      backgroundColor: Colors.transparent,
      borderWidth: 2,
      borderColor: Colors.gray,
    },
    text: {
      color: Colors.gray,
    },
  }),
  grayDark: StyleSheet.create({
    container: {
      backgroundColor: Colors.transparent,
      borderWidth: 2,
      borderColor: Colors.grayDark,
    },
    text: {
      color: Colors.grayDark,
    },
  }),
};

export const circle = StyleSheet.create({
  small: {
    height: 35,
    width: 35,
    borderRadius: 17.5,
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  medium: {
    height: 70,
    width: 70,
    borderRadius: 35,
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  large: {
    height: 80,
    width: 80,
    borderRadius: 40,
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  xl: {
    height: 130,
    width: 130,
    borderRadius: 65,
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
});

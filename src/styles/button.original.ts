import {StyleSheet} from 'react-native';
import * as Colors from './colors';

export const common = StyleSheet.create({
  container: {
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
  },
  rounded: {
    borderRadius: 100,
    overflow: 'hidden',
  },
  text: {
    fontWeight: 'bold',
  },
});

export const containerSize = StyleSheet.create({
  small: {
    height: 35,
    paddingHorizontal: 10,
  },
  medium: {
    height: 42,
    paddingHorizontal: 22,
  },
  large: {
    height: 55,
    paddingHorizontal: 30,
  },
});

export const textSize = StyleSheet.create({
  small: {
    fontSize: 14,
  },
  medium: {
    fontSize: 16,
  },
  large: {
    fontSize: 22,
  },
});

export const iconSize = StyleSheet.create({
  small: {
    fontSize: 16,
  },
  medium: {
    fontSize: 18,
  },
  large: {
    fontSize: 24,
  },
});

export const outlined = StyleSheet.create({
  container: {
    backgroundColor: Colors.transparent,
    borderWidth: 1,
    borderColor: Colors.buttonColors.outline.light.outline,
  },
  text: {
    color: Colors.buttonColors.outline.light.text,
  },
});

export const primary = StyleSheet.create({
  container: {
    backgroundColor: Colors.buttonColors.primary.light.bg,
  },
  text: {
    color: Colors.buttonColors.primary.light.text,
  },
});

export const light = StyleSheet.create({
  containerDisabled: {
    opacity: 0.5,
  },
  textDisabled: {
    color: Colors.buttonColors.disabled.light.text,
  },

  containeroutlined: {
    backgroundColor: Colors.transparent,
    borderWidth: 1,
    borderColor: Colors.buttonColors.outline.light.outline,
  },
  textoutlined: {
    color: Colors.buttonColors.outline.light.text,
  },

  containerprimary: {
    backgroundColor: Colors.buttonColors.primary.light.bg,
  },
  textprimary: {
    color: Colors.buttonColors.primary.light.text,
  },
});

export const dark = StyleSheet.create({
  containerDisabled: {
    opacity: 0.8,
  },
  textDisabled: {
    color: Colors.buttonColors.disabled.dark.text,
  },

  containeroutlined: {
    backgroundColor: Colors.transparent,
    borderWidth: 1,
    borderColor: Colors.buttonColors.outline.dark.outline,
  },
  textoutlined: {
    color: Colors.buttonColors.outline.dark.text,
  },

  containerprimary: {
    backgroundColor: Colors.buttonColors.primary.dark.bg,
  },
  textprimary: {
    color: Colors.buttonColors.primary.dark.text,
  },
});

export const colored = {
  primary: StyleSheet.create({
    teal: {
      backgroundColor: Colors.teal,
    },
    green: {
      backgroundColor: Colors.green,
    },
    orange: {
      backgroundColor: Colors.orange,
    },
    pink: {
      backgroundColor: Colors.pink,
    },
    grayLight: {
      backgroundColor: Colors.gray,
    },
    gray: {
      backgroundColor: Colors.gray,
    },
    grayDark: {
      backgroundColor: Colors.grayDark,
    },
  }),
  outlined: {
    teal: StyleSheet.create({
      containeroutlined: {
        backgroundColor: Colors.transparent,
        borderWidth: 1,
        borderColor: Colors.teal,
      },
      textoutlined: {
        color: Colors.teal,
      },
    }),
    green: StyleSheet.create({
      containeroutlined: {
        backgroundColor: Colors.transparent,
        borderWidth: 1,
        borderColor: Colors.green,
      },
      textoutlined: {
        color: Colors.green,
      },
    }),
    orange: StyleSheet.create({
      containeroutlined: {
        backgroundColor: Colors.transparent,
        borderWidth: 1,
        borderColor: Colors.orange,
      },
      textoutlined: {
        color: Colors.orange,
      },
    }),
    pink: StyleSheet.create({
      containeroutlined: {
        backgroundColor: Colors.transparent,
        borderWidth: 1,
        borderColor: Colors.pink,
      },
      textoutlined: {
        color: Colors.pink,
      },
    }),
    grayLight: StyleSheet.create({
      containeroutlined: {
        backgroundColor: Colors.transparent,
        borderWidth: 1,
        borderColor: Colors.grayLight,
      },
      textoutlined: {
        color: Colors.grayLight,
      },
    }),
    gray: StyleSheet.create({
      containeroutlined: {
        backgroundColor: Colors.transparent,
        borderWidth: 1,
        borderColor: Colors.gray,
      },
      textoutlined: {
        color: Colors.gray,
      },
    }),
    grayDark: StyleSheet.create({
      containeroutlined: {
        backgroundColor: Colors.transparent,
        borderWidth: 1,
        borderColor: Colors.grayDark,
      },
      textoutlined: {
        color: Colors.grayDark,
      },
    }),
  },
};

export const rounded = StyleSheet.create({
  small: {
    height: 35,
    width: 35,
    borderRadius: 17.5,
    overflow: 'hidden',
    paddingVertical: 0,
    paddingHorizontal: 0,
    alignContent: 'center',
    justifyContent: 'center',
  },
  medium: {
    height: 70,
    width: 70,
    borderRadius: 35,
    overflow: 'hidden',
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  large: {
    height: 80,
    width: 80,
    borderRadius: 40,
    overflow: 'hidden',
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
});

export const activityIndicator = {
  light: {
    primary: {
      color: Colors.buttonColors.primary.light.text,
    },
    outlined: {
      color: Colors.buttonColors.outline.light.text,
    },
  },
  dark: {
    primary: {
      color: Colors.buttonColors.primary.dark.text,
    },
    outlined: {
      color: Colors.buttonColors.outline.dark.text,
    },
  },
};

export const rippleColor = {
  dark: Colors.grayDarker,
  light: Colors.white,
};

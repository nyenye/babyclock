// export const pink = "#ffb6b6"; // "#f1b8c8" other pink
// export const pinkLight = "#fde2e2"; //#aa5585 less lighter

export const tealLighter = '#e2f8f8';
export const tealLight = '#b0e5e5';
export const teal = '#77C1C1';
export const tealDark = '#679b9b'; // #764b8e less lighter
export const tealDarker = '#637b7b';

export const white = '#FFFFFF';
export const grayLighter = '#F7F7F7';
export const grayLight = '#C5C5C5';
export const gray = '#999999';
export const grayDark = '#666666';
export const grayDarker = '#3B3B3B';
export const black = '#2B2B2B';

export const green = '#a5c19b';
export const pink = '#E5AB94';
export const orange = '#F2BB4F';
export const red = '#F05353';

export const transparent = 'transparent';

export const shadowTeal = '#EEF7F7';
export const shadowOrange = '#FFF2D9';
export const shadowPink = '#FEF3EF';

export const textColors = {
  dark: grayLight,
  light: gray,
};

export const activityIndicatorColors = {
  dark: teal,
  light: teal,
};

export const backgroundColors = {
  dark: grayDarker,
  light: white,
  highlights: {
    dark: [grayDarker, grayDark, gray],
    light: [white, grayLighter, grayLight],
  },
};

export const headerColors = {
  dark: {
    backgroundColor: backgroundColors.dark,
    tintColor: textColors.dark,
  },
  light: {
    backgroundColor: backgroundColors.light,
    tintColor: textColors.light,
  },
};

export const tabBarColors = {
  dark: {
    active: tealDark,
    inactive: gray,
    background: backgroundColors.dark,
    borderTop: gray,
  },
  light: {
    active: teal,
    inactive: gray,
    background: backgroundColors.light,
    borderTop: grayLight,
  },
};

export const statusbarColors = {
  dark: tealDark,
  light: teal,
};

export const buttonColors = {
  disabled: {
    dark: {
      bg: gray,
      text: grayLight,
      ripple: teal,
    },
    light: {
      bg: gray,
      text: grayLight,
      ripple: teal,
    },
  },
  themed: {
    dark: {
      bg: backgroundColors.dark,
      text: textColors.dark,
      ripple: textColors.dark,
    },
    light: {
      bg: backgroundColors.light,
      text: textColors.light,
      ripple: textColors.light,
    },
  },
  outline: {
    dark: {
      outline: backgroundColors.dark,
      text: backgroundColors.dark,
      ripple: backgroundColors.dark,
    },
    light: {
      outline: backgroundColors.light,
      text: backgroundColors.light,
      ripple: backgroundColors.light,
    },
  },
  primary: {
    dark: {
      bg: tealDark,
      text: white,
      ripple: white,
    },
    light: {
      bg: teal,
      text: white,
      ripple: white,
    },
  },
};

export const cardColors = {
  dark: {
    backgroundColor: grayDark,
  },
  light: {
    backgroundColor: grayLighter,
  },
};

export const textInputColors = {
  dark: {
    backgroundColor: grayDark,
    border: teal,
    text: grayLight,
  },
  light: {
    backgroundColor: grayLighter,
    border: teal,
    text: gray,
  },
};

export const selectorColors = {
  dark: {
    backgroundColor: grayDark,
    border: teal,
    text: gray,
    textSelected: teal,
  },
  light: {
    backgroundColor: grayLighter,
    border: teal,
    text: grayDark,
    textSelected: teal,
  },
};

export const checkboxColors = {
  dark: {
    isSelected: {
      outter: teal,
      inner: teal,
    },
    isNotSelected: {
      outter: gray,
      inner: backgroundColors.dark,
    },
  },
  light: {
    isSelected: {
      outter: tealLight,
      inner: teal,
    },
    isNotSelected: {
      outter: gray,
      inner: backgroundColors.light,
    },
  },
};

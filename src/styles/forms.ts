import {StyleSheet} from 'react-native';

import * as Colors from './colors';

export const textInput = {
  common: StyleSheet.create({
    container: {
      paddingVertical: 2,
      paddingHorizontal: 2,
      borderRadius: 8,
    },
    containerFocused: {
      backgroundColor: Colors.textInputColors.dark.border,
    },
    containerError: {
      backgroundColor: Colors.pink,
    },
    input: {
      height: 40,
      paddingLeft: 10,
      borderRadius: 8,
    },
    isRequiredMark: {
      height: 3,
      width: 3,
      borderRadius: 1.5,
      right: 4,
      top: 4,
      backgroundColor: Colors.pink,
    },
  }),
  dark: {
    bg0: StyleSheet.create({
      container: {
        backgroundColor: Colors.backgroundColors.highlights.dark[0],
      },
      input: {
        backgroundColor: Colors.backgroundColors.highlights.dark[0],
        color: Colors.textInputColors.dark.text,
      },
    }),
    bg1: StyleSheet.create({
      container: {
        backgroundColor: Colors.backgroundColors.highlights.dark[1],
      },
      input: {
        backgroundColor: Colors.backgroundColors.highlights.dark[1],
        color: Colors.textInputColors.dark.text,
      },
    }),
    bg2: StyleSheet.create({
      container: {
        backgroundColor: Colors.backgroundColors.highlights.dark[2],
      },
      input: {
        backgroundColor: Colors.backgroundColors.highlights.dark[2],
        color: Colors.textInputColors.dark.text,
      },
    }),
  },
  light: {
    bg0: StyleSheet.create({
      container: {
        backgroundColor: Colors.backgroundColors.highlights.light[0],
      },
      input: {
        backgroundColor: Colors.backgroundColors.highlights.light[0],
        color: Colors.textInputColors.light.text,
      },
    }),
    bg1: StyleSheet.create({
      container: {
        backgroundColor: Colors.backgroundColors.highlights.light[1],
      },
      input: {
        backgroundColor: Colors.backgroundColors.highlights.light[1],
        color: Colors.textInputColors.light.text,
      },
    }),
    bg2: StyleSheet.create({
      container: {
        backgroundColor: Colors.backgroundColors.highlights.light[2],
      },
      input: {
        backgroundColor: Colors.backgroundColors.highlights.light[2],
        color: Colors.textInputColors.light.text,
      },
    }),
  },
};

export const textArea = StyleSheet.create({
  two: {
    height: 40 * 2,
  },
  three: {
    height: 40 * 3,
  },
  four: {
    height: 40 * 4,
  },
  five: {
    height: 40 * 5,
  },
  fullHeight: {
    height: '100%',
  },
});

export const selector = {
  common: StyleSheet.create({
    borderView: {
      height: '100%',
    },
    touchable: {},
    text: {},
  }),
  dark: StyleSheet.create({
    borderView: {
      backgroundColor: Colors.selectorColors.dark.backgroundColor,
    },
    borderViewSelected: {
      backgroundColor: Colors.selectorColors.dark.border,
    },
    touchable: {
      backgroundColor: Colors.selectorColors.dark.backgroundColor,
    },
    textSelected: {
      color: Colors.selectorColors.dark.textSelected,
      fontWeight: 'bold',
    },
  }),
  light: StyleSheet.create({
    borderView: {
      backgroundColor: Colors.selectorColors.light.backgroundColor,
    },
    borderViewSelected: {
      backgroundColor: Colors.selectorColors.light.border,
    },
    touchable: {
      backgroundColor: Colors.selectorColors.light.backgroundColor,
    },
    textSelected: {
      color: Colors.selectorColors.light.textSelected,
      fontWeight: 'bold',
    },
  }),
};

import {StyleSheet, Platform} from 'react-native';

import * as Colors from './colors';

import {ThemeHook} from '../hooks';

const fontFamily = Platform.OS === 'ios' ? 'System' : 'Roboto';

export const headings = StyleSheet.create({
  h1: {
    fontFamily,
    fontSize: 26,
    fontWeight: 'bold',
  },
  h2: {
    fontFamily,
    fontSize: 20,
    fontWeight: 'bold',
  },
  h3: {
    fontFamily,
    fontSize: 18,
    fontWeight: 'bold',
  },
  h4: {
    fontFamily,
    fontSize: 17,
    fontWeight: 'bold',
  },
});

export const text = StyleSheet.create({
  p: {
    fontSize: 14,
    fontWeight: 'normal',
  },
  bold: {
    fontSize: 14,
    fontWeight: 'bold',
  },
});

export const weight = StyleSheet.create({
  normal: {
    fontWeight: 'normal',
  },
  bold: {
    fontWeight: 'bold',
  },
});

export const size = StyleSheet.create({
  small: {
    fontSize: 12,
  },
  medium: {
    fontSize: 14,
  },
  large: {
    fontSize: 16,
  },
  xl: {
    fontSize: 18,
  },
});

export const color = StyleSheet.create({
  white: {
    color: Colors.white,
  },
  black: {
    color: Colors.black,
  },
  gray: {
    color: Colors.gray,
  },
  tealLighter: {
    color: Colors.tealLighter,
  },
  teal: {
    color: Colors.teal,
  },
  tealDarker: {
    color: Colors.tealDarker,
  },
  green: {
    color: Colors.green,
  },
  orange: {
    color: Colors.orange,
  },
  pink: {
    color: Colors.pink,
  },
  red: {
    color: Colors.red,
  },
});

export const alignment = StyleSheet.create({
  center: {
    textAlign: 'center',
  },
  left: {
    textAlign: 'left',
  },
  right: {
    textAlign: 'right',
  },
  justify: {
    textAlign: 'justify',
  },
});

export function getThemedTypographyColors(theme: ThemeHook.Theme) {
  return StyleSheet.create({
    text: {
      color: Colors.textColors[theme],
    },
  });
}

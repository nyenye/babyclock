import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  expand: {
    flex: 1,
  },
  notExpand: {
    flex: 0,
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    flexDirection: 'column',
  },
  noFill: {},
  fillWidth: {
    width: '100%',
  },
  fillHeight: {
    height: '100%',
  },
  fillBoth: {
    height: '100%',
    width: '100%',
  },
  centerContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  justifyCenter: {
    justifyContent: 'center',
  },
  alignCenter: {
    alignItems: 'center',
  },
  justifyEnd: {
    justifyContent: 'flex-end',
  },
  alignEnd: {
    alignItems: 'flex-end',
  },
  justifyStart: {
    justifyContent: 'flex-start',
  },
  spaceBetween: {
    justifyContent: 'space-between',
  },
  spaceEvenly: {
    justifyContent: 'space-evenly',
  },
  alignStart: {
    alignItems: 'flex-start',
  },
  alignStretch: {
    alignItems: 'stretch',
  },
  absolute: {
    position: 'absolute',
  },
  relative: {
    position: 'relative',
  },
  noPush: {},
  pushRight: {
    marginLeft: 'auto',
  },
  pushLeft: {
    marginRight: 'auto',
  },
  pushTop: {
    marginBottom: 'auto',
  },
  pushBottom: {
    marginTop: 'auto',
  },
});

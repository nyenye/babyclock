import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';

import {
  Baby,
  Tracking,
  UserProfile,
  TrackingType,
  FeedingMode,
  FinishedBreastFeedTracking,
  OngoingBreastFeedTracking,
  FinishedSleepTracking,
  OngoingSleepTracking,
  FinishedBottleFeedTracking,
  OngoingBottleFeedTracking,
  FinishedDiaperTracking,
} from './types';

class BabyFactory {
  public static fromFirestoreQueryDocumentSnapshot(
    doc: FirebaseFirestoreTypes.QueryDocumentSnapshot,
  ): Baby {
    const data = doc.data();

    return {
      id: doc.id,
      name: data.name,
      gender: data.gender,
      birthDate: data.birthDate.toDate(),
      caretakers: data.caretakers || [],
      parents: data.parents || [],
      pictureFilename: data.pictureFilename || undefined,
      pictureUri: data.pictureUri || undefined,
      trackings: data.trackings || [],
    };
  }

  public static fromFirestoreDocumentSnapshot(
    doc: FirebaseFirestoreTypes.DocumentSnapshot,
  ): Baby | undefined {
    const data = doc.data();

    if (data === undefined) {
      return undefined;
    }

    return {
      id: doc.id,
      name: data.name,
      gender: data.gender,
      birthDate: data.birthDate.toDate(),
      caretakers: data.caretakers || [],
      parents: data.parents || [],
      pictureFilename: data.pictureFilename || undefined,
      pictureUri: data.pictureUri || undefined,
      trackings: data.trackings || [],
    };
  }
}

class TrackingFactory {
  public static sleepTracking(
    id: string,
    data: any,
  ): FinishedSleepTracking | OngoingSleepTracking {
    return {
      id: id,
      type: data.type,
      mode: data.mode,
      start: data.start.toDate(),
      end: data.end ? data.end.toDate() : undefined,
    };
  }

  public static breastFeedTracking(
    id: string,
    data: any,
  ): FinishedBreastFeedTracking | OngoingBreastFeedTracking {
    const leftTimers: Array<{
      start: Date;
      end: Date;
    }> = data.leftTimers.map(
      (item: {
        start: FirebaseFirestoreTypes.Timestamp;
        end?: FirebaseFirestoreTypes.Timestamp;
      }) => ({
        start: item.start.toDate(),
        end: item.end ? item.end.toDate() : undefined,
      }),
    );
    const rightTimers: Array<{
      start: Date;
      end: Date;
    }> = data.rightTimers.map(
      (item: {
        start: FirebaseFirestoreTypes.Timestamp;
        end?: FirebaseFirestoreTypes.Timestamp;
      }) => ({
        start: item.start.toDate(),
        end: item.end ? item.end.toDate() : undefined,
      }),
    );

    const leftDuration = leftTimers.reduce((accum: number, curr) => {
      if (curr.end === undefined) {
        return accum;
      }

      return accum + (curr.end.valueOf() - curr.start.valueOf());
    }, 0);
    const rightDuration = rightTimers.reduce((accum: number, curr) => {
      if (curr.end === undefined) {
        return accum;
      }

      return accum + (curr.end.valueOf() - curr.start.valueOf());
    }, 0);

    return {
      id: id,
      type: data.type,
      mode: data.mode,
      start: data.start.toDate(),
      end: data.end ? data.end.toDate() : undefined,
      leftTimers,
      leftDuration,
      rightTimers,
      rightDuration,
    };
  }

  public static bottleFeedTracking(
    id: string,
    data: any,
  ): FinishedBottleFeedTracking | OngoingBottleFeedTracking {
    return {
      id: id,
      type: data.type,
      mode: data.mode,
      start: data.start.toDate(),
      end: data.end ? data.end.toDate() : undefined,
      mL: data.mL,
    };
  }

  public static diaperTracking(id: string, data: any): FinishedDiaperTracking {
    return {
      id: id,
      type: data.type,
      mode: data.mode,
      start: data.start.toDate(),
      end: data.start.toDate(),
    };
  }

  public static fromFirestoreQueryDocumentSnapshot(
    doc: FirebaseFirestoreTypes.QueryDocumentSnapshot,
  ): Tracking {
    const data = doc.data();

    if ((data.type as TrackingType) === 'sleeping') {
      return TrackingFactory.sleepTracking(doc.id, data);
    }

    if ((data.type as TrackingType) === 'feeding') {
      if ((data.mode as FeedingMode) === 'breast') {
        return TrackingFactory.breastFeedTracking(doc.id, data);
      }

      if ((data.mode as FeedingMode) === 'bottle') {
        return TrackingFactory.bottleFeedTracking(doc.id, data);
      }
    }

    if ((data.type as TrackingType) === 'diaper') {
      return TrackingFactory.diaperTracking(doc.id, data);
    }

    return {
      id: doc.id,
      type: data.type,
      start: data.start.toDate(),
      end: data.end ? data.end.toDate() : undefined,
    };
  }

  public static fromFirestoreDocumentSnapshot(
    doc: FirebaseFirestoreTypes.DocumentSnapshot,
  ): Tracking | undefined {
    const data = doc.data();

    if (data === undefined) {
      return undefined;
    }

    if ((data.type as TrackingType) === 'sleeping') {
      return TrackingFactory.sleepTracking(doc.id, data);
    }

    if ((data.type as TrackingType) === 'feeding') {
      if ((data.mode as FeedingMode) === 'breast') {
        return TrackingFactory.breastFeedTracking(doc.id, data);
      }

      if ((data.mode as FeedingMode) === 'bottle') {
        return TrackingFactory.bottleFeedTracking(doc.id, data);
      }
    }

    if ((data.type as TrackingType) === 'diaper') {
      return TrackingFactory.diaperTracking(doc.id, data);
    }

    return undefined;
  }
}

class UserProfileFactory {
  public static fromFirestoreQueryDocumentSnapshot(
    doc: FirebaseFirestoreTypes.QueryDocumentSnapshot,
  ): UserProfile {
    const data = doc.data();

    return {
      id: doc.id,
      firstName: data.firstName,
      lastName: data.lastName,
      babies: data.babies || [],
      babiesInCare: data.babiesInCare || [],
      pictureFilename: data.pictureFilename || undefined,
      pictureUri: data.pictureUri || undefined,
    };
  }

  public static fromFirestoreDocumentSnapshot(
    doc: FirebaseFirestoreTypes.DocumentSnapshot,
  ): UserProfile | undefined {
    const data = doc.data();

    if (data === undefined) {
      return undefined;
    }

    return {
      id: doc.id,
      firstName: data.firstName,
      lastName: data.lastName,
      babies: data.babies || [],
      babiesInCare: data.babiesInCare || [],
      pictureFilename: data.pictureFilename || undefined,
      pictureUri: data.pictureUri || undefined,
    };
  }
}

export {BabyFactory, TrackingFactory, UserProfileFactory};

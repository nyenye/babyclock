import React from 'react';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';
import * as I18N from 'react-i18next';
import Icon from 'react-native-vector-icons/Feather';

import {BabiesHook, ThemeHook} from '../hooks';
import * as Screens from '../screens';
import {Colors, Typography} from '../styles';
import {
  Baby,
  TrackingType,
  FinishedSleepTracking,
  FinishedBreastFeedTracking,
  FinishedBottleFeedTracking,
} from '../types';

import * as Routes from './routes';

export type InitStackParamList = {
  [Routes.WELCOME]: undefined;
  [Routes.SIGN_UP]: undefined;
};

export type SetupProfileStackParamList = {
  [Routes.USER_PROFILE_SETUP]: undefined;
  [Routes.BABY_PROFILE_SETUP]: {
    userId: string;
    isEditting?: boolean;
    baby?: Baby;
  };
};

export type TrackingsStackParamList = {
  [Routes.TRACKINGS]: undefined;
  [Routes.TRACKINGS_DETAILS]: {
    initialTracking?: TrackingType;
  };
  [Routes.TRACKING_FORM_BREASTFEED]: {
    isEditting: boolean;
    tracking?: FinishedBreastFeedTracking;
  };
  [Routes.TRACKING_FORM_BOTTLEFEED]: {
    isEditting: boolean;
    tracking?: FinishedBottleFeedTracking;
  };
  [Routes.TRACKING_FORM_DIAPER]: undefined;
  [Routes.TRACKING_FORM_SLEEP]: {
    isEditting: boolean;
    tracking?: FinishedSleepTracking;
  };
};

const InitStack = createStackNavigator<InitStackParamList>();
const SetupProfileStack = createStackNavigator<SetupProfileStackParamList>();
const TrackingsStack = createStackNavigator<TrackingsStackParamList>();

function getStackScreenOptions(theme: ThemeHook.Theme): StackNavigationOptions {
  return {
    headerStyle: {
      elevation: 0,
      borderBottomWidth: 0.5,
      borderBottomColor: Colors.backgroundColors.highlights.light[2],
      backgroundColor: Colors.headerColors[theme].backgroundColor,
    },
    headerTintColor: theme === 'light' ? Colors.teal : Colors.tealDark,
    headerTitleStyle: [
      Typography.headings.h4,
      Typography.color[theme === 'light' ? 'black' : 'white'],
    ],
    headerBackImage: ({tintColor}) => (
      <Icon name="arrow-left" color={tintColor} size={18} />
    ),
    headerTitleAlign: 'center',
  };
}

function InitNavigator() {
  const {theme} = ThemeHook.useState();
  const {t} = I18N.useTranslation('translations');

  return (
    <InitStack.Navigator
      screenOptions={getStackScreenOptions(theme)}
      initialRouteName="WELCOME">
      <InitStack.Screen
        options={{headerShown: false}}
        name={Routes.WELCOME}
        component={Screens.Welcome}
      />
      <InitStack.Screen
        options={{title: t('SCREEN_TITLE_SIGN_UP')}}
        name={Routes.SIGN_UP}
        component={Screens.SignUp}
      />
    </InitStack.Navigator>
  );
}

function ProvidedInitNavigator() {
  return (
    <ThemeHook.Provider defaultTheme="light">
      <InitNavigator />
    </ThemeHook.Provider>
  );
}

function SetupNavigator() {
  const {theme} = ThemeHook.useState();
  const {t} = I18N.useTranslation('translations');

  return (
    <SetupProfileStack.Navigator
      screenOptions={getStackScreenOptions(theme)}
      initialRouteName="USER_PROFILE_SETUP">
      <SetupProfileStack.Screen
        options={{title: t('SCREEN_TITLE_USER_PROFILE')}}
        name={Routes.USER_PROFILE_SETUP}
        component={Screens.UserProfileSetup}
      />
      <SetupProfileStack.Screen
        options={{title: t('SCREEN_TITLE_BABY_PROFILE')}}
        name={Routes.BABY_PROFILE_SETUP}
        component={Screens.BabyProfileSetup}
        initialParams={{
          isEditting: false,
        }}
      />
    </SetupProfileStack.Navigator>
  );
}

function ProvidedSetupNavigator() {
  return (
    <BabiesHook.Provider>
      <ThemeHook.Provider defaultTheme="light">
        <SetupNavigator />
      </ThemeHook.Provider>
    </BabiesHook.Provider>
  );
}

function TrackingsNavigator() {
  const {theme} = ThemeHook.useState();
  const {t} = I18N.useTranslation('translations');

  return (
    <TrackingsStack.Navigator
      screenOptions={getStackScreenOptions(theme)}
      initialRouteName="TRACKINGS">
      <TrackingsStack.Screen
        options={{title: t('SCREEN_TITLE_USER_PROFILE'), headerShown: false}}
        name={Routes.TRACKINGS}
        component={Screens.Trackings}
      />
      <TrackingsStack.Screen
        options={{title: t('SCREEN_TITLE_TRACKINGS'), headerShown: true}}
        name={Routes.TRACKINGS_DETAILS}
        component={Screens.TrackingsDetails}
        initialParams={{
          initialTracking: 'sleeping',
        }}
      />
      <TrackingsStack.Screen
        options={{
          headerShown: true,
        }}
        name={Routes.TRACKING_FORM_BREASTFEED}
        component={Screens.TrackingFormBreastFeed}
      />
      <TrackingsStack.Screen
        options={{
          headerShown: true,
        }}
        name={Routes.TRACKING_FORM_BOTTLEFEED}
        component={Screens.TrackingFormBottleFeed}
      />
      <TrackingsStack.Screen
        options={{
          title: t('SCREEN_TITLE_TRACKING_ADD_DIAPER'),
          headerShown: true,
        }}
        name={Routes.TRACKING_FORM_DIAPER}
        component={Screens.TrackingFormDiaper}
      />
      <TrackingsStack.Screen
        options={{
          headerShown: true,
        }}
        name={Routes.TRACKING_FORM_SLEEP}
        component={Screens.TrackingFormSleep}
        initialParams={{
          tracking: undefined,
        }}
      />
    </TrackingsStack.Navigator>
  );
}

export {
  ProvidedInitNavigator as InitNavigator,
  ProvidedSetupNavigator as SetupNavigator,
  TrackingsNavigator,
};

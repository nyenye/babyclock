import React from 'react';

import {AuthHook} from '../hooks';
import * as Screens from '../screens';
import {LoadingUtils} from '../utils';

import {MainNavigation} from './MainNavigator';
import * as Stacks from './Stacks';

function Navigation() {
  const {
    isLoggedIn,
    userProfile,
    fetchCurrentUserProfileRequestState: fetchRequestState,
  } = AuthHook.useState();

  const loadingMessageRef = React.useRef(
    LoadingUtils.getRandomLoadingMessage(),
  );

  if (!isLoggedIn) {
    return <Stacks.InitNavigator />;
  }

  if (
    userProfile === null &&
    (fetchRequestState.status === 'idle' ||
      fetchRequestState.status === 'pending')
  ) {
    return <Screens.LoadProfile message={loadingMessageRef.current} />;
  }

  if (
    userProfile === null &&
    fetchRequestState.status !== 'idle' &&
    fetchRequestState.status !== 'pending'
  ) {
    return <Stacks.SetupNavigator />;
  }

  return <MainNavigation />;
}

export {Navigation};

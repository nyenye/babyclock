import React from 'react';
import {
  BottomTabBarOptions,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';

import * as I18N from 'react-i18next';
import Icon from 'react-native-vector-icons/Feather';

import {AuthHook, BabiesHook, ThemeHook, TrackingHook} from '../hooks';
import {BabiesHookGetters, TrackingHookGetters} from '../hooks/getters';
import * as Screens from '../screens';
import {Colors} from '../styles';

import * as Routes from './routes';
import {BottomTabBar} from './BottomTabBar';
import {TrackingsNavigator} from './Stacks';

export type TabParamList = {
  [Routes.TRACKINGS]: undefined;
  [Routes.REPORT]: undefined;
  [Routes.FAMILY]: undefined;
  [Routes.ACCOUNT]: undefined;
};

type TabBarIconOptions = {focused: boolean; color: string; size: number};

const Tab = createBottomTabNavigator<TabParamList>();

function getTabBarIcon({color, size}: TabBarIconOptions, icon: string) {
  return <Icon name={icon} size={size} color={color} />;
}

function getTabBarOption(theme: ThemeHook.Theme): BottomTabBarOptions {
  return {
    style: {
      elevation: 0,
      borderTopWidth: 0.5,
      borderTopColor: Colors.backgroundColors.highlights.light[2],
      backgroundColor: Colors.headerColors[theme].backgroundColor,
    },
    inactiveBackgroundColor: Colors.tabBarColors[theme].background,
    activeBackgroundColor: Colors.tabBarColors[theme].background,
    keyboardHidesTabBar: true,
    activeTintColor: Colors.tabBarColors[theme].active,
    inactiveTintColor: Colors.tabBarColors[theme].inactive,
  };
}

function MainNavigation() {
  const {theme} = ThemeHook.useState();
  const {t} = I18N.useTranslation('translations');

  const {userProfile} = AuthHook.useState();

  const {
    fetchBabiesRequestState,
    babies,
    selectedBabyId,
  } = BabiesHook.useState();
  const babiesDispatch = BabiesHook.useDispatch();

  const {trackings, fetchTrackingsRequestState} = TrackingHook.useState();
  const trackingDispatch = TrackingHook.useDispatch();

  React.useEffect(() => {
    // Fetch Babies
    if (
      userProfile &&
      fetchBabiesRequestState.status === 'idle' &&
      babies.length === 0
    ) {
      BabiesHook.fetchBabies(babiesDispatch, userProfile.id);
    }

    // Set selectedBabyId
    if (
      fetchBabiesRequestState.status === 'resolved' ||
      fetchBabiesRequestState.status === 'rejected'
    ) {
      if (babies.length > 0) {
        babiesDispatch({type: 'SET_SELECTED', babyId: babies[0].id});
      }

      babiesDispatch({
        type: 'RESET_REQUEST',
        requestStateId: 'fetchBabiesRequestState',
      });
    }
  }, [babiesDispatch, userProfile, babies, fetchBabiesRequestState]);

  React.useEffect(() => {
    if (selectedBabyId === null) {
      return;
    }

    if (trackings[selectedBabyId] !== undefined) {
      return;
    }

    if (fetchTrackingsRequestState.status === 'idle') {
      TrackingHook.fetchTrackingsForBaby(trackingDispatch, selectedBabyId);
    }

    if (
      fetchTrackingsRequestState.status === 'resolved' ||
      fetchTrackingsRequestState.status === 'rejected'
    ) {
      trackingDispatch({
        type: 'RESET_REQUEST',
        requestStateId: 'fetchTrackingsRequestState',
      });
    }
  }, [trackingDispatch, trackings, fetchTrackingsRequestState, selectedBabyId]);

  return (
    <Tab.Navigator
      tabBar={(props) => <BottomTabBar {...props} />}
      tabBarOptions={getTabBarOption(theme)}
      initialRouteName="TRACKINGS">
      <Tab.Screen
        key={Routes.TRACKINGS}
        name={Routes.TRACKINGS}
        component={TrackingsNavigator}
        options={{
          tabBarLabel: t('TAB_BAR_TITLE_TRACKINGS'),
          tabBarAccessibilityLabel: t('TAB_BAR_TITLE_TRACKINGS'),
          tabBarIcon: (iconOptions) =>
            getTabBarIcon(iconOptions, 'plus-circle'),
        }}
      />
      <Tab.Screen
        key={Routes.REPORT}
        name={Routes.REPORT}
        component={Screens.Report}
        options={{
          tabBarLabel: t('TAB_BAR_TITLE_REPORT'),
          tabBarAccessibilityLabel: t('TAB_BAR_TITLE_REPORT'),
          tabBarIcon: (iconOptions) =>
            getTabBarIcon(iconOptions, 'bar-chart-2'),
        }}
      />
      <Tab.Screen
        key={Routes.FAMILY}
        name={Routes.FAMILY}
        component={Screens.Family}
        options={{
          tabBarLabel: t('TAB_BAR_TITLE_FAMILY'),
          tabBarAccessibilityLabel: t('TAB_BAR_TITLE_FAMILY'),
          tabBarIcon: (iconOptions) => getTabBarIcon(iconOptions, 'heart'),
        }}
      />
      <Tab.Screen
        key={Routes.ACCOUNT}
        name={Routes.ACCOUNT}
        component={Screens.Account}
        options={{
          tabBarLabel: t('TAB_BAR_TITLE_ACCOUNT'),
          tabBarAccessibilityLabel: t('TAB_BAR_TITLE_ACCOUNT'),
          tabBarIcon: (iconOptions) => getTabBarIcon(iconOptions, 'settings'),
        }}
      />
    </Tab.Navigator>
  );
}

function ProvidedMainNavigation() {
  return (
    <BabiesHook.Provider>
      <BabiesHookGetters.Provider>
        <TrackingHook.Provider>
          <TrackingHookGetters.Provider>
            <MainNavigation />
          </TrackingHookGetters.Provider>
        </TrackingHook.Provider>
      </BabiesHookGetters.Provider>
    </BabiesHook.Provider>
  );
}

export {ProvidedMainNavigation as MainNavigation};

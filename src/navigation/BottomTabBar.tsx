import React from 'react';
import {Route} from '@react-navigation/native';
import {BottomTabBarProps} from '@react-navigation/bottom-tabs';
import {StyleSheet, TouchableNativeFeedback} from 'react-native';

import {View, Text} from '../components';
import {Colors} from '../styles';

const styles = StyleSheet.create({
  container: {
    height: 55,
  },
  focusedTextColor: {
    color: Colors.teal,
  },
});

function BottomTabBar({
  state,
  descriptors,
  navigation,
  style,
  ...props
}: BottomTabBarProps) {
  return (
    <View bgHighlight={0} direction="row" style={[styles.container, style]}>
      {state.routes.map((route: Route<string>, index: number) => {
        const {options} = descriptors[route.key];

        const label = options.tabBarLabel;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        const {activeTintColor, inactiveTintColor} = props;

        const icon = options.tabBarIcon!({
          focused: isFocused,
          color: (isFocused && activeTintColor!) || inactiveTintColor!,
          size: 20,
        });

        const dynamicStyles = StyleSheet.create({
          textColor: {
            color: isFocused ? activeTintColor! : inactiveTintColor,
          },
        });

        return (
          <TouchableNativeFeedback
            key={route.key}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            onPress={onPress}
            onLongPress={onLongPress}
            background={TouchableNativeFeedback.Ripple(Colors.teal)}>
            <View main="justifyCenter" cross="alignCenter" flex={1}>
              {icon}
              <Text style={dynamicStyles.textColor}>{label}</Text>
            </View>
          </TouchableNativeFeedback>
        );
      })}
    </View>
  );
}

export {BottomTabBar};

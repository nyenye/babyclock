import ASFactory from '@react-native-community/async-storage';
import LegacyStorage from '@react-native-community/async-storage-backend-legacy';

const legacyStorage = new LegacyStorage();

type StorageModel = {
  '@user/name': string;
  '@user/last-name': string;
  '@user/token': string;
  '@scheduleDetails/lastStartOfWeek': string;
  '@ui/theme': string;
};

// class JSONStorage extends LegacyStorage {
//   constructor() {
//     super();
//   }

//   public async getObject<K extends keyof EmptyStorageModel>(
//     key: K,
//     opts?: StorageOptions,
//   ): Promise<EmptyStorageModel[K] | null> {
//     return JSON.parse(await this.getSingle(key, opts));
//   }

//   public async setObject<K extends keyof EmptyStorageModel>(
//     key: K,
//     value: EmptyStorageModel[K],
//     opts?: StorageOptions,
//   ): Promise<void> {
//     return this.setSingle(key, JSON.stringify(value), opts);
//   }
// }

const Storage = ASFactory.create<StorageModel>(legacyStorage, {});

export {Storage};

import React from 'react';

import {Baby} from '../../types';

import * as BabiesHook from '../babiesHook';

type State = {
  selectedBaby: Baby | null;
};

type ProviderProps = {children: React.ReactNode};

const BabiesGettersContext = React.createContext<State | undefined>(undefined);

function Provider({children}: ProviderProps) {
  const babiesState = BabiesHook.useState();

  const [selectedBaby, setSelectedBaby] = React.useState<Baby | null>(null);

  const defaultState: State = {
    selectedBaby,
  };

  React.useEffect(() => {
    if (babiesState.selectedBabyId === null) {
      return;
    }

    if (
      selectedBaby !== null &&
      selectedBaby.id === babiesState.selectedBabyId
    ) {
      return;
    }

    setSelectedBaby(
      getBabyById(babiesState.babies, babiesState.selectedBabyId),
    );
  }, [selectedBaby, babiesState]);

  return (
    <BabiesGettersContext.Provider value={defaultState}>
      {children}
    </BabiesGettersContext.Provider>
  );
}

function useGetters(): State {
  const context = React.useContext(BabiesGettersContext);

  if (context === undefined) {
    throw new Error('useGetters must be used within a Provider');
  }

  return context;
}

function getBabyById(babies: Array<Baby>, babyId: string | null): Baby | null {
  const baby = babies.find((b) => b.id === babyId);
  return baby || null;
}

export {Provider, useGetters};

import React from 'react';

import {
  FinishedTrackings,
  OngoingTrackings,
  Tracking,
  TrackingType,
  OngoingTracking,
  FinishedTracking,
} from '../../types';

import * as TrackingHook from '../trackingHook';

type State = {
  finishedTrackings: FinishedTrackings;
  ongoingTrackings: OngoingTrackings;
};

type ProviderProps = {children: React.ReactNode};

const TrackingGettersContext = React.createContext<State | undefined>(
  undefined,
);

function Provider({children}: ProviderProps) {
  const trackingState = TrackingHook.useState();

  const [finishedTrackings, setFinishedTrackings] = React.useState<
    FinishedTrackings
  >({});

  const [ongoingTrackings, setOngoingTrackings] = React.useState<
    OngoingTrackings
  >({});

  const defaultState: State = {
    finishedTrackings,
    ongoingTrackings,
  };

  React.useEffect(() => {
    setFinishedTrackings(getFinishedTrackings(trackingState.trackings));
    setOngoingTrackings(getOngoingTrackings(trackingState.trackings));
  }, [trackingState.trackings]);

  return (
    <TrackingGettersContext.Provider value={defaultState}>
      {children}
    </TrackingGettersContext.Provider>
  );
}

function useGetters(): State {
  const context = React.useContext(TrackingGettersContext);

  if (context === undefined) {
    throw new Error('useGetters must be used within a Provider');
  }

  return context;
}

function getFinishedTrackings(trackings: {
  [key: string]: Array<Tracking>;
}): FinishedTrackings {
  const finished = Object.keys(trackings).reduce((accum, curr) => {
    accum[curr] = trackings[curr]
      .filter((t) => t.end !== undefined)
      .reduce(
        (a, c) => {
          a[c.type].push(c as FinishedTracking);
          return a;
        },
        {sleeping: [], feeding: [], diaper: []} as Record<
          TrackingType,
          Array<FinishedTracking>
        >,
      );
    return accum;
  }, {} as FinishedTrackings);

  return finished;
}

function getOngoingTrackings(trackings: {
  [key: string]: Array<Tracking>;
}): OngoingTrackings {
  const ongoing = Object.keys(trackings).reduce((accum, curr) => {
    accum[curr] = trackings[curr]
      .filter((t) => t.end === undefined)
      .reduce((a, c) => {
        a[c.type] = c as OngoingTracking;
        return a;
      }, {} as Record<TrackingType, OngoingTracking | undefined>);
    return accum;
  }, {} as OngoingTrackings);

  return ongoing;
}

export {Provider, useGetters};

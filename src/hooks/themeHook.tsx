import React from 'react';

export type Theme = 'light' | 'dark';

interface State {
  theme: Theme;
}

type Action = {type: 'SET_THEME'; theme: Theme} | {type: 'TOGGLE_THEME'};
type Dispatch = (action: Action) => void;

type ProviderProps = {defaultTheme: Theme; children: React.ReactNode};

const ThemeStateContext = React.createContext<State | undefined>(undefined);
const ThemeDispatchContext = React.createContext<Dispatch | undefined>(
  undefined,
);

const themeReducer: React.Reducer<State, Action> = (
  state: State,
  action: Action,
): State => {
  switch (action.type) {
    case 'SET_THEME': {
      return {theme: action.theme};
    }
    case 'TOGGLE_THEME': {
      return {theme: (state.theme === 'light' && 'dark') || 'light'};
    }
    default: {
      throw new Error(`Unhandled action type: ${action!.type}`);
    }
  }
};

function Provider({defaultTheme = 'light', children}: ProviderProps) {
  const defaultState: State = {
    theme: defaultTheme,
  };
  const [state, dispatch] = React.useReducer<React.Reducer<State, Action>>(
    themeReducer,
    defaultState,
  );

  return (
    <ThemeStateContext.Provider value={state}>
      <ThemeDispatchContext.Provider value={dispatch}>
        {children}
      </ThemeDispatchContext.Provider>
    </ThemeStateContext.Provider>
  );
}

function useState(): State {
  const context = React.useContext(ThemeStateContext);

  if (context === undefined) {
    throw new Error('useState must be used within a Provider (ThemeHook)');
  }

  return context;
}

function useDispatch(): Dispatch {
  const context = React.useContext(ThemeDispatchContext);

  if (context === undefined) {
    throw new Error('useDispatch must be used within a Provider (ThemeHook)');
  }

  return context;
}

export {Provider, useState, useDispatch};

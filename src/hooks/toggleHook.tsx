import React from 'react';

type UseToggleReturn = [boolean, () => void];

function useToggle(defaultValue: boolean = false): UseToggleReturn {
  const [bool, setBool] = React.useState(defaultValue);

  // const toggle = React.useCallback(() => setBool((b) => !b), [setBool]);

  function toggle() {
    setBool((b) => !b);
  }

  return [bool, toggle];
}

export {useToggle};

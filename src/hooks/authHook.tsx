import React from 'react';
import Auth, {FirebaseAuthTypes} from '@react-native-firebase/auth';
import Firestore, {
  FirebaseFirestoreTypes,
} from '@react-native-firebase/firestore';
import Storage from '@react-native-firebase/storage';

import {UserProfileFactory} from '../factories';
import {FileUploadResult, RequestState, UserProfile} from '../types';
import {FirestoreUtils, StorageUtils} from '../utils';
import {RequestActions} from './requestActions';

const storageRef = Storage().ref();

type RequestsState = {
  fetchCurrentUserProfileRequestState: RequestState;
  createUserProfileRequestState: RequestState;
  updateUserProfileRequestState: RequestState;
};

type State = {
  isLoggedIn: boolean;

  isLoadingUserProfile: boolean;
  userProfile: UserProfile | null;
} & RequestsState;

type RequestStateId = keyof RequestsState;

type Action =
  | {type: 'SET_IS_LOGGED_IN_USER'; isLoggedIn: boolean}
  | {type: 'SET_IS_LOADING_USER_PROFILE'; isLoadingUserProfile: boolean}
  | {type: 'SET_USER_PROFILE'; profile: UserProfile | null}
  | RequestActions<RequestStateId>;

type Dispatch = (action: Action) => void;

type ProviderProps = {
  children: React.ReactNode;
};

const AuthStateContext = React.createContext<State | undefined>(undefined);
const AuthDispatchContext = React.createContext<Dispatch | undefined>(
  undefined,
);

const authReducer: React.Reducer<State, Action> = (
  state: State,
  action: Action,
): State => {
  switch (action.type) {
    case 'SET_IS_LOGGED_IN_USER': {
      return {
        ...state,
        isLoggedIn: action.isLoggedIn,
      };
    }
    case 'SET_IS_LOADING_USER_PROFILE': {
      return {
        ...state,
        isLoadingUserProfile: action.isLoadingUserProfile,
      };
    }
    case 'SET_USER_PROFILE': {
      return {
        ...state,
        userProfile: action.profile,
      };
    }
    case 'RESET_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'idle',
        },
      };
    }
    case 'START_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'pending',
        },
      };
    }
    case 'FINISH_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'resolved',
        },
      };
    }
    case 'FAIL_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'rejected',
          errorCode: action.errorCode,
        },
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action!.type}`);
    }
  }
};

const initialRequestState: RequestState = {
  status: 'idle',
  errorCode: undefined,
};

function Provider({children}: ProviderProps) {
  const isLoggedIn = Auth().currentUser !== null;

  const defaultState: State = {
    isLoggedIn,
    isLoadingUserProfile: false,
    userProfile: null,
    fetchCurrentUserProfileRequestState: initialRequestState,
    createUserProfileRequestState: initialRequestState,
    updateUserProfileRequestState: initialRequestState,
  };

  const [state, dispatch] = React.useReducer<React.Reducer<State, Action>>(
    authReducer,
    defaultState,
  );

  React.useEffect(() => {
    const unsubscribe = Auth().onAuthStateChanged(
      (user: FirebaseAuthTypes.User | null) => {
        dispatch({
          type: 'SET_IS_LOGGED_IN_USER',
          isLoggedIn: user !== null,
        });
      },
    );

    return () => {
      unsubscribe();
    };
  }, [state.isLoggedIn]);

  return (
    <AuthStateContext.Provider value={state}>
      <AuthDispatchContext.Provider value={dispatch}>
        {children}
      </AuthDispatchContext.Provider>
    </AuthStateContext.Provider>
  );
}

function useState(): State {
  const context = React.useContext(AuthStateContext);

  if (context === undefined) {
    throw new Error('useState must be used within a Provider (AuthHook)');
  }

  return context;
}

function useDispatch(): Dispatch {
  const context = React.useContext(AuthDispatchContext);

  if (context === undefined) {
    throw new Error('useDispatch must be used within a Provider (AuthHook)');
  }

  return context;
}

function getCurrentUser(): FirebaseAuthTypes.User | null {
  return Auth().currentUser;
}

async function fetchCurrentUserProfile(dispatch: Dispatch, userId: string) {
  dispatch({
    type: 'START_REQUEST',
    requestStateId: 'fetchCurrentUserProfileRequestState',
  });

  Firestore()
    .collection(FirestoreUtils.COLLECTION_USERS)
    .doc(userId)
    .get()
    .then((snapshot: FirebaseFirestoreTypes.DocumentSnapshot) => {
      const profile = UserProfileFactory.fromFirestoreDocumentSnapshot(
        snapshot,
      );

      if (profile !== undefined) {
        dispatch({
          type: 'SET_USER_PROFILE',
          profile,
        });

        dispatch({
          type: 'FINISH_REQUEST',
          requestStateId: 'fetchCurrentUserProfileRequestState',
        });
      } else {
        dispatch({
          type: 'FAIL_REQUEST',
          requestStateId: 'fetchCurrentUserProfileRequestState',
        });
      }
    })
    .catch(() => {
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'fetchCurrentUserProfileRequestState',
      });
    });
}

async function createUserProfile(
  dispatch: Dispatch,
  accountUid: string,
  data: UserProfile,
) {
  dispatch({
    type: 'START_REQUEST',
    requestStateId: 'createUserProfileRequestState',
  });

  let fileUpload: FileUploadResult | undefined;

  try {
    fileUpload = await StorageUtils.uploadFileToStorage(
      storageRef,
      StorageUtils.FOLDER_IMAGES,
      data.pictureUri,
      'jpg',
    );
  } catch (err) {
    dispatch({
      type: 'FAIL_REQUEST',
      requestStateId: 'createUserProfileRequestState',
      errorCode: StorageUtils.ERROR_UPLOAD_FAILED,
    });
  }

  const profileToCreate: UserProfile = {
    id: accountUid,
    firstName: data.firstName,
    lastName: data.lastName,
    pictureUri: fileUpload!.fileUri,
    pictureFilename: fileUpload!.filename,
    babies: data.babies,
    babiesInCare: [],
  };

  Firestore()
    .collection(FirestoreUtils.COLLECTION_USERS)
    .doc(accountUid)
    .set(profileToCreate)
    .then(() => {
      dispatch({
        type: 'SET_USER_PROFILE',
        profile: profileToCreate,
      });
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'createUserProfileRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'createUserProfileRequestState',
        errorCode: FirestoreUtils.ERROR_ON_CREATE,
      });
    });
}

function logout(dispatch: Dispatch) {
  Auth()
    .signOut()
    .then(function () {
      dispatch({type: 'SET_USER_PROFILE', profile: null});
      dispatch({
        type: 'RESET_REQUEST',
        requestStateId: 'fetchCurrentUserProfileRequestState',
      });
    })
    .catch(function () {
      console.warn('could not log out');
    });
}

export {
  Provider,
  useState,
  useDispatch,
  getCurrentUser,
  fetchCurrentUserProfile,
  createUserProfile,
  logout,
};

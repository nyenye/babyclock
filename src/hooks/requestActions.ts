export type RequestActions<T> =
  | {type: 'RESET_REQUEST'; requestStateId: T}
  | {type: 'START_REQUEST'; requestStateId: T}
  | {type: 'FINISH_REQUEST'; requestStateId: T}
  | {type: 'FAIL_REQUEST'; requestStateId: T; errorCode?: string};

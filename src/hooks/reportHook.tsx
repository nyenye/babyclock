import React from 'react';
import dayjs from 'dayjs';

import {TrackingType} from '../types';

const dateFormat = 'YYYY-MM-DD';

type State = {
  currentStartOfWeek: string;
  activeTrackingTypes: Set<TrackingType>;
};

type Action =
  | {type: 'PREV_WEEK'}
  | {type: 'NEXT_WEEK'}
  | {type: 'SET_WEEK'; currentStartOfWeek: string}
  | {type: 'TOGGLE_TRACKING_FILTER'; filter: TrackingType};
type Dispatch = (action: Action) => void;

type ProviderProps = {children: React.ReactNode};

const ReportStateContext = React.createContext<State | undefined>(undefined);
const ReportDispatchContext = React.createContext<Dispatch | undefined>(
  undefined,
);

const scheduleDetailsReducer: React.Reducer<State, Action> = (
  state: State,
  action: Action,
): State => {
  switch (action.type) {
    case 'SET_WEEK': {
      return {
        ...state,
        currentStartOfWeek: action.currentStartOfWeek,
      };
    }
    case 'PREV_WEEK': {
      return {
        ...state,
        currentStartOfWeek: dayjs(state.currentStartOfWeek)
          .subtract(1, 'week')
          .format(dateFormat),
      };
    }
    case 'NEXT_WEEK': {
      return {
        ...state,
        currentStartOfWeek: dayjs(state.currentStartOfWeek)
          .add(1, 'week')
          .format(dateFormat),
      };
    }
    case 'TOGGLE_TRACKING_FILTER': {
      const {activeTrackingTypes} = state;

      if (activeTrackingTypes.has(action.filter)) {
        activeTrackingTypes.delete(action.filter);
      } else {
        activeTrackingTypes.add(action.filter);
      }

      return {
        ...state,
        activeTrackingTypes: activeTrackingTypes,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action!.type}`);
    }
  }
};

function Provider({children}: ProviderProps) {
  const defaultState: State = {
    activeTrackingTypes: new Set(['sleeping', 'feeding']),
    currentStartOfWeek: dayjs().startOf('week').format(dateFormat),
  };
  const [state, dispatch] = React.useReducer<React.Reducer<State, Action>>(
    scheduleDetailsReducer,
    defaultState,
  );

  return (
    <ReportStateContext.Provider value={state}>
      <ReportDispatchContext.Provider value={dispatch}>
        {children}
      </ReportDispatchContext.Provider>
    </ReportStateContext.Provider>
  );
}

function useState(): State {
  const context = React.useContext(ReportStateContext);

  if (context === undefined) {
    throw new Error('useState must be used within a Provider (ReportHook)');
  }

  return context;
}

function useDispatch(): Dispatch {
  const context = React.useContext(ReportDispatchContext);

  if (context === undefined) {
    throw new Error('useDispatch must be used within a Provider (ReportHook)');
  }

  return context;
}

export {Provider, useState, useDispatch};

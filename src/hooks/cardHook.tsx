import React from 'react';

type State = {
  isCollapsed: boolean;
};

type Action = {type: 'TOGGLE_COLLAPSE'};
type Dispatch = (action: Action) => void;

type ProviderProps = {
  isCollapsed?: boolean;
  children: React.ReactNode;
};

const CardStateContext = React.createContext<State | undefined>(undefined);
const CardDispatchContext = React.createContext<Dispatch | undefined>(
  undefined,
);

const cardReducer: React.Reducer<State, Action> = (
  state: State,
  action: Action,
): State => {
  switch (action.type) {
    case 'TOGGLE_COLLAPSE': {
      return {
        isCollapsed: !state.isCollapsed,
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};

function Provider({isCollapsed = false, children}: ProviderProps) {
  const defaultState: State = {isCollapsed};
  const [state, dispatch] = React.useReducer<React.Reducer<State, Action>>(
    cardReducer,
    defaultState,
  );

  return (
    <CardStateContext.Provider value={state}>
      <CardDispatchContext.Provider value={dispatch}>
        {children}
      </CardDispatchContext.Provider>
    </CardStateContext.Provider>
  );
}

function useState(): State {
  const context = React.useContext(CardStateContext);

  if (context === undefined) {
    throw new Error('useState must be used within a Provider (CardHook)');
  }

  return context;
}

function useDispatch(): Dispatch {
  const context = React.useContext(CardDispatchContext);

  if (context === undefined) {
    throw new Error('useDispatch must be used within a Provider (CardHook)');
  }

  return context;
}

export {Provider, useState, useDispatch};

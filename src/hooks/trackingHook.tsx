import React from 'react';
import Firestore, {
  FirebaseFirestoreTypes,
} from '@react-native-firebase/firestore';

import {v4 as uuid} from 'uuid';

import {TrackingFactory} from '../factories';
import {
  RequestState,
  Tracking,
  LorR,
  OngoingTracking,
  OngoingBreastFeedTracking,
  OngoingSleepTracking,
  FinishedDiaperTracking,
  DiaperMode,
  FinishedSleepTracking,
  FinishedBreastFeedTracking,
} from '../types';
import {FirestoreUtils} from '../utils';

import {RequestActions} from './requestActions';

type RequestsState = {
  fetchTrackingsRequestState: RequestState;
  startTrackingRequestState: RequestState;
  stopTrackingRequestState: RequestState;
  updateTrackingRequestState: RequestState;
  removeTrackingRequestState: RequestState;
  reportTrackingRequestState: RequestState;
};

type State = {
  trackings: {[key: string]: Array<Tracking>};
  fetchCursor: FirebaseFirestoreTypes.QueryDocumentSnapshot | undefined;
} & RequestsState;

type RequestStateId = keyof RequestsState;

type Action =
  | {type: 'SET_TRACKINGS'; babyId: string; trackings: Array<Tracking>}
  | {type: 'ADD_TRACKING'; babyId: string; tracking: Tracking}
  | {type: 'UPDATE_TRACKING'; babyId: string; tracking: Tracking}
  | {type: 'REMOVE_TRACKING'; babyId: string; trackingId: string}
  | {type: 'SET_CURSOR'; cursor: FirebaseFirestoreTypes.QueryDocumentSnapshot}
  | RequestActions<RequestStateId>;

type Dispatch = (action: Action) => void;

type ProviderProps = {children: React.ReactNode};

const TrackingsStateContext = React.createContext<State | undefined>(undefined);
const TrackingsDispatchContext = React.createContext<Dispatch | undefined>(
  undefined,
);

const babiesReducer: React.Reducer<State, Action> = (
  state: State,
  action: Action,
): State => {
  switch (action.type) {
    case 'SET_TRACKINGS': {
      return {
        ...state,
        trackings: {
          ...state.trackings,
          [action.babyId]: action.trackings,
        },
      };
    }
    case 'ADD_TRACKING': {
      return {
        ...state,
        trackings: {
          ...state.trackings,
          [action.babyId]: [action.tracking, ...state.trackings[action.babyId]],
        },
      };
    }
    case 'UPDATE_TRACKING': {
      const trackings = state.trackings[action.babyId].map((tracking) =>
        tracking.id === action.tracking.id ? action.tracking : tracking,
      );

      return {
        ...state,
        trackings: {
          ...state.trackings,
          [action.babyId]: trackings,
        },
      };
    }
    case 'REMOVE_TRACKING': {
      const trackings = state.trackings[action.babyId].filter(
        (tracking) => tracking.id !== action.trackingId,
      );

      return {
        ...state,
        trackings: {
          ...state.trackings,
          [action.babyId]: trackings,
        },
      };
    }
    case 'SET_CURSOR': {
      return {
        ...state,
        fetchCursor: action.cursor,
      };
    }
    case 'RESET_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'idle',
        },
      };
    }
    case 'START_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'pending',
        },
      };
    }
    case 'FINISH_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'resolved',
        },
      };
    }
    case 'FAIL_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'rejected',
          errorCode: action.errorCode,
        },
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action!.type}`);
    }
  }
};

const initialRequestState: RequestState = {
  status: 'idle',
  errorCode: undefined,
};

function Provider({children}: ProviderProps) {
  const defaultState: State = {
    trackings: {},
    fetchCursor: undefined,
    fetchTrackingsRequestState: initialRequestState,
    startTrackingRequestState: initialRequestState,
    stopTrackingRequestState: initialRequestState,
    updateTrackingRequestState: initialRequestState,
    removeTrackingRequestState: initialRequestState,
    reportTrackingRequestState: initialRequestState,
  };

  const [state, dispatch] = React.useReducer<React.Reducer<State, Action>>(
    babiesReducer,
    defaultState,
  );

  return (
    <TrackingsStateContext.Provider value={state}>
      <TrackingsDispatchContext.Provider value={dispatch}>
        {children}
      </TrackingsDispatchContext.Provider>
    </TrackingsStateContext.Provider>
  );
}

function useState(): State {
  const context = React.useContext(TrackingsStateContext);

  if (context === undefined) {
    throw new Error('useState must be used within a Provider (TrackingHook)');
  }

  return context;
}

function useDispatch(): Dispatch {
  const context = React.useContext(TrackingsDispatchContext);

  if (context === undefined) {
    throw new Error(
      'useDispatch must be used within a Provider (TrackingHook)',
    );
  }

  return context;
}

async function fetchTrackingsForBaby(
  dispatch: Dispatch,
  babyId: string,
  cursor = undefined,
) {
  dispatch({
    type: 'START_REQUEST',
    requestStateId: 'fetchTrackingsRequestState',
  });

  const query = Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .doc(babyId)
    .collection(FirestoreUtils.COLLECTION_TRACKINGS)
    .orderBy('start', 'desc');

  if (cursor !== undefined) {
    query.startAfter(cursor);
  }

  query
    .limit(100)
    .get()
    .then((snapshot) => {
      const fetchedTrackings: Array<Tracking> = snapshot.docs.map((doc) => {
        return TrackingFactory.fromFirestoreQueryDocumentSnapshot(doc);
      });

      dispatch({
        type: 'SET_TRACKINGS',
        babyId: babyId,
        trackings: fetchedTrackings,
      });

      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'fetchTrackingsRequestState',
      });

      const nextCursor = snapshot.docs[snapshot.docs.length - 1];
      dispatch({type: 'SET_CURSOR', cursor: nextCursor});
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'fetchTrackingsRequestState',
        errorCode: FirestoreUtils.ERROR_ON_FETCH,
      });
    });
}

async function startTrackingSleep(dispatch: Dispatch, babyId: string) {
  const id = uuid();

  const tracking: OngoingSleepTracking = {
    id,
    type: 'sleeping',
    modes: {
      car: false,
      crib: true,
      held: false,
      nursing: false,
      stroller: false,
    },
    start: new Date(),
    end: undefined,
  };

  dispatch({
    type: 'START_REQUEST',
    requestStateId: 'startTrackingRequestState',
  });

  Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .doc(babyId)
    .collection(FirestoreUtils.COLLECTION_TRACKINGS)
    .doc(id)
    .set(tracking)
    .then(() => {
      dispatch({
        type: 'ADD_TRACKING',
        babyId,
        tracking,
      });
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'startTrackingRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'startTrackingRequestState',
        errorCode: FirestoreUtils.ERROR_ON_CREATE,
      });
    });
}

async function reportTrackingSleep(
  dispatch: Dispatch,
  babyId: string,
  tracking: FinishedSleepTracking,
) {
  dispatch({
    type: 'START_REQUEST',
    requestStateId: 'reportTrackingRequestState',
  });

  Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .doc(babyId)
    .collection(FirestoreUtils.COLLECTION_TRACKINGS)
    .doc(tracking.id)
    .set(tracking)
    .then(() => {
      dispatch({
        type: 'ADD_TRACKING',
        babyId,
        tracking,
      });
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'reportTrackingRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'reportTrackingRequestState',
        errorCode: FirestoreUtils.ERROR_ON_CREATE,
      });
    });
}

async function startTrackingBreastfeed(
  dispatch: Dispatch,
  babyId: string,
  breast: LorR,
) {
  const id = uuid();

  const leftTimers = breast === 'l' ? [{start: new Date()}] : [];
  const rightTimers = breast === 'r' ? [{start: new Date()}] : [];

  const tracking: OngoingBreastFeedTracking = {
    id,
    type: 'feeding',
    mode: 'breast',
    start: new Date(),
    end: undefined,
    leftTimers,
    leftDuration: 0,
    rightTimers,
    rightDuration: 0,
  };

  dispatch({
    type: 'START_REQUEST',
    requestStateId: 'startTrackingRequestState',
  });

  Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .doc(babyId)
    .collection(FirestoreUtils.COLLECTION_TRACKINGS)
    .doc(id)
    .set(tracking)
    .then(() => {
      dispatch({
        type: 'ADD_TRACKING',
        babyId,
        tracking,
      });
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'startTrackingRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'startTrackingRequestState',
        errorCode: FirestoreUtils.ERROR_ON_CREATE,
      });
    });
}

async function reportTrackingBreastfeed(
  dispatch: Dispatch,
  babyId: string,
  tracking: FinishedBreastFeedTracking,
) {
  dispatch({
    type: 'START_REQUEST',
    requestStateId: 'reportTrackingRequestState',
  });

  Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .doc(babyId)
    .collection(FirestoreUtils.COLLECTION_TRACKINGS)
    .doc(tracking.id)
    .set(tracking)
    .then(() => {
      dispatch({
        type: 'ADD_TRACKING',
        babyId,
        tracking,
      });
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'reportTrackingRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'reportTrackingRequestState',
        errorCode: FirestoreUtils.ERROR_ON_CREATE,
      });
    });
}

async function reportTrackingDiaper(
  dispatch: Dispatch,
  babyId: string,
  mode: DiaperMode,
) {
  const id = uuid();

  const tracking: FinishedDiaperTracking = {
    id,
    type: 'diaper',
    mode,
    start: new Date(),
    end: new Date(),
  };

  dispatch({
    type: 'START_REQUEST',
    requestStateId: 'startTrackingRequestState',
  });

  Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .doc(babyId)
    .collection(FirestoreUtils.COLLECTION_TRACKINGS)
    .doc(id)
    .set(tracking)
    .then(() => {
      dispatch({
        type: 'ADD_TRACKING',
        babyId,
        tracking,
      });
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'startTrackingRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'startTrackingRequestState',
        errorCode: FirestoreUtils.ERROR_ON_CREATE,
      });
    });
}

async function updateTracking(
  dispatch: Dispatch,
  babyId: string,
  trackingUpdate: Tracking,
) {
  dispatch({
    type: 'START_REQUEST',
    requestStateId: 'updateTrackingRequestState',
  });

  Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .doc(babyId)
    .collection(FirestoreUtils.COLLECTION_TRACKINGS)
    .doc(trackingUpdate.id)
    .set({...trackingUpdate}, {merge: true})
    .then(() => {
      dispatch({
        type: 'UPDATE_TRACKING',
        babyId,
        tracking: trackingUpdate,
      });
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'updateTrackingRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'updateTrackingRequestState',
        errorCode: FirestoreUtils.ERROR_ON_CREATE,
      });
    });
}

async function stopTracking(
  dispatch: Dispatch,
  babyId: string,
  ongoing: OngoingTracking,
) {
  updateTracking(dispatch, babyId, {...ongoing, end: new Date()});
}

async function restartTracking(
  dispatch: Dispatch,
  babyId: string,
  ongoing: OngoingTracking,
) {
  updateTracking(dispatch, babyId, {...ongoing, start: new Date()});
}

async function removeTracking(
  dispatch: Dispatch,
  babyId: string,
  trackingId: string,
) {
  dispatch({
    type: 'START_REQUEST',
    requestStateId: 'removeTrackingRequestState',
  });

  Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .doc(babyId)
    .collection(FirestoreUtils.COLLECTION_TRACKINGS)
    .doc(trackingId)
    .delete()
    .then(() => {
      dispatch({
        type: 'REMOVE_TRACKING',
        babyId,
        trackingId,
      });
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'removeTrackingRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'removeTrackingRequestState',
        errorCode: FirestoreUtils.ERROR_ON_CREATE,
      });
    });
}

export {
  Provider,
  useState,
  useDispatch,
  fetchTrackingsForBaby,
  startTrackingSleep,
  reportTrackingSleep,
  startTrackingBreastfeed,
  reportTrackingBreastfeed,
  reportTrackingDiaper,
  updateTracking,
  stopTracking,
  restartTracking,
  removeTracking,
};

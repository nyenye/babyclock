import React from 'react';
import Firestore from '@react-native-firebase/firestore';
import Storage from '@react-native-firebase/storage';

import {BabyFactory} from '../factories';
import {Baby, RequestState, FileUploadResult} from '../types';
import {FirestoreUtils, StorageUtils} from '../utils';

import {RequestActions} from './requestActions';

const storageRef = Storage().ref();

type RequestsState = {
  fetchBabiesRequestState: RequestState;
  addBabyRequestState: RequestState;
  updateBabyRequestState: RequestState;
  removeBabyRequestState: RequestState;
};

type State = {
  babies: Array<Baby>;
  selectedBabyId: string | null;
} & RequestsState;

type RequestStateId = keyof RequestsState;

type Action =
  | {type: 'SET_SELECTED'; babyId: string}
  | {type: 'SET_BABIES'; babies: Array<Baby>}
  | {type: 'ADD_BABY'; baby: Baby}
  | {type: 'UPDATE_BABY'; babyId: string; baby: Baby}
  | {type: 'REMOVE_BABY'; babyId: string}
  | RequestActions<RequestStateId>;

type Dispatch = (action: Action) => void;

type ProviderProps = {children: React.ReactNode};

const BabiesStateContext = React.createContext<State | undefined>(undefined);
const BabiesDispatchContext = React.createContext<Dispatch | undefined>(
  undefined,
);

const babiesReducer: React.Reducer<State, Action> = (
  state: State,
  action: Action,
): State => {
  switch (action.type) {
    case 'SET_SELECTED': {
      return {
        ...state,
        selectedBabyId: action.babyId,
      };
    }
    case 'SET_BABIES': {
      return {
        ...state,
        babies: action.babies,
      };
    }
    case 'ADD_BABY': {
      return {
        ...state,
        babies: [...state.babies, action.baby],
      };
    }
    case 'UPDATE_BABY': {
      const babies = state.babies.map((baby: Baby) =>
        baby.id === action.babyId ? action.baby : baby,
      );

      return {
        ...state,
        babies,
      };
    }
    case 'REMOVE_BABY': {
      const babies = state.babies.filter(
        (baby: Baby) => baby.id !== action.babyId,
      );

      return {
        ...state,
        babies,
      };
    }
    case 'RESET_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'idle',
        },
      };
    }
    case 'START_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'pending',
        },
      };
    }
    case 'FINISH_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'resolved',
        },
      };
    }
    case 'FAIL_REQUEST': {
      return {
        ...state,
        [action.requestStateId]: {
          status: 'rejected',
          errorCode: action.errorCode,
        },
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action!.type}`);
    }
  }
};

const initialRequestState: RequestState = {
  status: 'idle',
  errorCode: undefined,
};

function Provider({children}: ProviderProps) {
  const defaultState: State = {
    selectedBabyId: null,
    babies: [],
    fetchBabiesRequestState: initialRequestState,
    addBabyRequestState: initialRequestState,
    updateBabyRequestState: initialRequestState,
    removeBabyRequestState: initialRequestState,
  };

  const [state, dispatch] = React.useReducer<React.Reducer<State, Action>>(
    babiesReducer,
    defaultState,
  );

  return (
    <BabiesStateContext.Provider value={state}>
      <BabiesDispatchContext.Provider value={dispatch}>
        {children}
      </BabiesDispatchContext.Provider>
    </BabiesStateContext.Provider>
  );
}

function useState(): State {
  const context = React.useContext(BabiesStateContext);

  if (context === undefined) {
    throw new Error('useState must be used within a Provider (BabiesHook)');
  }

  return context;
}

function useDispatch(): Dispatch {
  const context = React.useContext(BabiesDispatchContext);

  if (context === undefined) {
    throw new Error('useDispatch must be used within a Provider (BabiesHook)');
  }

  return context;
}

async function fetchBabies(dispatch: Dispatch, userId: string) {
  dispatch({type: 'START_REQUEST', requestStateId: 'fetchBabiesRequestState'});

  Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .where(`parents.${userId}`, '==', true)
    .get()
    .then((snapshot) => {
      const fetchedBabies: Array<Baby> = snapshot.docs.map((doc) => {
        return BabyFactory.fromFirestoreQueryDocumentSnapshot(doc);
      });

      dispatch({
        type: 'SET_BABIES',
        babies: fetchedBabies,
      });
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'fetchBabiesRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'fetchBabiesRequestState',
        errorCode: FirestoreUtils.ERROR_ON_FETCH,
      });
    });
}

async function addBaby(dispatch: Dispatch, data: Baby, parentId: string) {
  dispatch({type: 'START_REQUEST', requestStateId: 'addBabyRequestState'});

  let fileUpload: FileUploadResult | undefined;

  try {
    fileUpload = await StorageUtils.uploadFileToStorage(
      storageRef,
      StorageUtils.FOLDER_IMAGES,
      data.pictureUri,
      'jpg',
    );
  } catch (err) {
    dispatch({
      type: 'FAIL_REQUEST',
      requestStateId: 'addBabyRequestState',
      errorCode: StorageUtils.ERROR_UPLOAD_FAILED,
    });
  }

  const babyToAdd: Baby = {
    id: data.id,
    name: data.name,
    gender: data.gender,
    birthDate: Firestore.Timestamp.fromDate(data.birthDate as Date),
    weight: data.weight,
    length: data.length,
    pictureUri: fileUpload!.fileUri,
    pictureFilename: fileUpload!.filename,
    parents: {[parentId]: true},
  };

  Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .doc(data.id)
    .set(babyToAdd)
    .then(() => {
      dispatch({
        type: 'ADD_BABY',
        baby: babyToAdd,
      });
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'addBabyRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'addBabyRequestState',
        errorCode: FirestoreUtils.ERROR_ON_CREATE,
      });
    });
}

async function updateBaby(
  dispatch: Dispatch,
  id: string,
  data: Baby,
  pictureHasChanged: boolean,
  originalFilename: string | null,
) {
  dispatch({type: 'START_REQUEST', requestStateId: 'updateBabyRequestState'});

  let fileUpload: FileUploadResult | undefined;

  if (originalFilename !== null) {
    StorageUtils.removeFileFromStorage(
      storageRef,
      StorageUtils.FOLDER_IMAGES,
      originalFilename,
    );
  }

  if (pictureHasChanged) {
    if (data.pictureUri !== undefined) {
      try {
        fileUpload = await StorageUtils.uploadFileToStorage(
          storageRef,
          StorageUtils.FOLDER_IMAGES,
          data.pictureUri,
          'jpg',
        );
      } catch (err) {
        dispatch({
          type: 'FAIL_REQUEST',
          requestStateId: 'updateBabyRequestState',
          errorCode: StorageUtils.ERROR_UPLOAD_FAILED,
        });
      }
    }
  }

  const babyToUpdate: Baby = {
    ...data,
    birthDate: Firestore.Timestamp.fromDate(data.birthDate as Date),
    pictureFilename:
      (fileUpload && fileUpload.filename) || data.pictureFilename,
    pictureUri: (fileUpload && fileUpload.fileUri) || data.pictureUri,
  };

  Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .doc(id)
    .set(babyToUpdate, {merge: true})
    .then(() => {
      dispatch({
        type: 'UPDATE_BABY',
        babyId: id,
        baby: babyToUpdate,
      });
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'updateBabyRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'updateBabyRequestState',
        errorCode: FirestoreUtils.ERROR_ON_UPDATE,
      });
    });
}

async function removeBaby(dispatch: Dispatch, id: string) {
  dispatch({type: 'START_REQUEST', requestStateId: 'removeBabyRequestState'});

  Firestore()
    .collection(FirestoreUtils.COLLECTION_BABIES)
    .doc(id)
    .delete()
    .then(() => {
      dispatch({
        type: 'FINISH_REQUEST',
        requestStateId: 'removeBabyRequestState',
      });
    })
    .catch((reason) => {
      console.warn(reason);
      dispatch({
        type: 'FAIL_REQUEST',
        requestStateId: 'removeBabyRequestState',
        errorCode: FirestoreUtils.ERROR_ON_CREATE,
      });
    });
}

export {
  Provider,
  useState,
  useDispatch,
  fetchBabies,
  addBaby,
  updateBaby,
  removeBaby,
};

import {Colors} from '../styles';
import {TrackingType, SleepingMode, DiaperMode} from '../types';

import sleepIcon from '../assets/icons/sleep.png';
import feedIcon from '../assets/icons/feed.png';
import diaperIcon from '../assets/icons/diaper.png';
import sleepIconInactive from '../assets/icons/sleep_inactive.png';
import feedIconInactive from '../assets/icons/feed_inactive.png';
import diaperIconInactive from '../assets/icons/diaper_inactive.png';

import carIcon from '../assets/icons/sleep-modes/car.png';
import cribIcon from '../assets/icons/sleep-modes/crib.png';
import heldIcon from '../assets/icons/sleep-modes/held.png';
import nursingIcon from '../assets/icons/sleep-modes/nursing.png';
import strollerIcon from '../assets/icons/sleep-modes/stroller.png';

export const typeToColor: Record<TrackingType, string> = {
  sleeping: Colors.teal,
  feeding: Colors.orange,
  diaper: Colors.pink,
};

export const typeToShadow: Record<TrackingType, string> = {
  sleeping: Colors.shadowTeal,
  feeding: Colors.shadowOrange,
  diaper: Colors.shadowPink,
};

export const typeToColorName: Record<
  TrackingType,
  'teal' | 'orange' | 'pink'
> = {
  sleeping: 'teal',
  feeding: 'orange',
  diaper: 'pink',
};

export const typeToIcon: Record<
  TrackingType,
  {
    active: number;
    inactive: number;
  }
> = {
  sleeping: {
    active: sleepIcon,
    inactive: sleepIconInactive,
  },
  feeding: {
    active: feedIcon,
    inactive: feedIconInactive,
  },
  diaper: {
    active: diaperIcon,
    inactive: diaperIconInactive,
  },
};

export const sleepModeToIcon: Record<SleepingMode, number> = {
  car: carIcon,
  crib: cribIcon,
  held: heldIcon,
  nursing: nursingIcon,
  stroller: strollerIcon,
};

export const diaperModeToIcon: Record<DiaperMode, number> = {
  pee: carIcon,
  poop: cribIcon,
  both: heldIcon,
};

export function durationToHms(
  milliseconds: number,
): {hours: number; minutes: number; seconds: number} {
  const seconds: number = Math.floor((milliseconds / 1000) % 60);
  const minutes: number = Math.floor((milliseconds / (1000 * 60)) % 60);
  const hours: number = Math.floor(milliseconds / (1000 * 60 * 60));

  return {hours, minutes, seconds};
}

export function durationToms(
  milliseconds: number,
): {minutes: number; seconds: number} {
  const seconds: number = Math.floor((milliseconds / 1000) % 60);
  const minutes: number = Math.floor(milliseconds / (1000 * 60));

  return {minutes, seconds};
}

function showTwoNumbers(number: number): string {
  return number < 10 ? `0${number}` : String(number);
}

export function durationToHHmmss(milliseconds: number): string {
  const {hours, minutes, seconds} = durationToHms(milliseconds);

  const displayH = showTwoNumbers(hours);
  const displayM = showTwoNumbers(minutes);
  const displayS = showTwoNumbers(seconds);

  return `${displayH}:${displayM}:${displayS}`;
}

export function durationTommss(milliseconds: number): string {
  const {minutes, seconds} = durationToms(milliseconds);

  const displayM = showTwoNumbers(minutes);
  const displayS = showTwoNumbers(seconds);

  return `${displayM}:${displayS}`;
}

export function isToday(date: Date): boolean {
  const today = new Date();
  return today.toDateString() === date.toDateString();
}

export function isYesterday(date: Date): boolean {
  const yesterday = new Date();
  yesterday.setDate(yesterday.getDate() - 1);
  return yesterday.toDateString() === date.toDateString();
}

export function isCurrentYear(date: Date): boolean {
  const currentYear = new Date();
  return currentYear.getFullYear() === date.getFullYear();
}

export function isFuture(date: Date): boolean {
  return new Date().valueOf() - date.valueOf() < 0;
}

export const breastfeedDurationSelector: {label: number; value: number}[] = [
  {label: 0, value: 0},
  {label: 1, value: 1},
  {label: 2, value: 2},
  {label: 3, value: 3},
  {label: 4, value: 4},
  {label: 5, value: 5},
  {label: 6, value: 6},
  {label: 7, value: 7},
  {label: 8, value: 8},
  {label: 9, value: 9},
  {label: 10, value: 10},
  {label: 11, value: 11},
  {label: 12, value: 12},
  {label: 13, value: 13},
  {label: 14, value: 14},
  {label: 15, value: 15},
  {label: 16, value: 16},
  {label: 17, value: 17},
  {label: 18, value: 18},
  {label: 19, value: 19},
  {label: 20, value: 20},
  {label: 21, value: 21},
  {label: 22, value: 22},
  {label: 23, value: 23},
  {label: 24, value: 24},
  {label: 25, value: 25},
  {label: 26, value: 26},
  {label: 27, value: 27},
  {label: 28, value: 28},
  {label: 29, value: 29},
  {label: 30, value: 30},
  {label: 31, value: 31},
  {label: 32, value: 32},
  {label: 33, value: 33},
  {label: 34, value: 34},
  {label: 35, value: 35},
  {label: 36, value: 36},
  {label: 37, value: 37},
  {label: 38, value: 38},
  {label: 39, value: 39},
  {label: 40, value: 40},
  {label: 41, value: 41},
  {label: 42, value: 42},
  {label: 43, value: 43},
  {label: 44, value: 44},
  {label: 45, value: 45},
  {label: 46, value: 46},
  {label: 47, value: 47},
  {label: 48, value: 48},
  {label: 49, value: 49},
  {label: 50, value: 50},
  {label: 51, value: 51},
  {label: 52, value: 52},
  {label: 53, value: 53},
  {label: 54, value: 54},
  {label: 55, value: 55},
  {label: 56, value: 56},
  {label: 57, value: 57},
  {label: 58, value: 58},
  {label: 59, value: 59},
  {label: 60, value: 60},
];

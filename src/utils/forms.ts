export type FormError = {
  hasError: boolean;
  errorMsg: string | null;
};

export function getRequiredTextfieldErrors(text: string | null): FormError {
  if (text === null) {
    return {hasError: false, errorMsg: null};
  }

  if (text.length === 0) {
    return {hasError: true, errorMsg: 'FORM_ERROR_IS_REQUIRED'};
  }

  return {hasError: false, errorMsg: null};
}

export function getEmailErrors(
  email: string | null,
  isRequired: boolean = true,
): FormError {
  if (email === null) {
    return {hasError: false, errorMsg: null};
  }

  if (isRequired && email.length === 0) {
    return {hasError: true, errorMsg: 'FORM_ERROR_IS_REQUIRED'};
  }

  if (!simpleEmailRegexValidation(email)) {
    return {hasError: true, errorMsg: 'FORM_ERROR_EMAIL_NOT_VALID'};
  }

  return {hasError: false, errorMsg: null};
}

export function getSignupPasswordErrors(
  password: string | null,
  confirmation: string | null,
  isRequired: boolean = true,
): FormError {
  if (password === null) {
    if (confirmation !== null) {
      return {hasError: true, errorMsg: 'FORM_ERROR_IS_REQUIRED'};
    }

    return {hasError: false, errorMsg: null};
  }

  if (isRequired && password.length === 0) {
    return {hasError: true, errorMsg: 'FORM_ERROR_IS_REQUIRED'};
  }

  if (confirmation === null) {
    return {hasError: false, errorMsg: null};
  }

  if (confirmation !== password) {
    return {hasError: true, errorMsg: 'FORM_ERROR_PASSWORDS_DONT_MATCH'};
  }

  return {hasError: false, errorMsg: null};
}

export function getRequiredCheckboxErrors(value: boolean | null): FormError {
  if (value === null) {
    return {hasError: false, errorMsg: null};
  }

  if (value === false) {
    return {hasError: true, errorMsg: 'FORM_ERROR_CHECKBOX_REQURIED'};
  }

  return {hasError: false, errorMsg: null};
}

function simpleEmailRegexValidation(email: string) {
  var re = /\S+@\S+\.\S+/;
  return re.test(email);
}

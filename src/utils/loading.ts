const messages: Array<string> = [
  'LOADING_MESSAGE_CHANGING_DIAPERS',
  'LOADING_MESSAGE_PUTTING_BABY_TO_SLEEP',
  'LOADING_MESSAGE_PREPARING_FOR_A_PHOTO_SESSION',
];

/**
 * Generates a random integer between min and max. The maximum is exclusive and the minimum is inclusive
 */
function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomLoadingMessage(): string {
  const randomIndex = getRandomInt(0, messages.length);
  return messages[randomIndex];
}

export {getRandomLoadingMessage};

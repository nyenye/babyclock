import {FirebaseStorageTypes} from '@react-native-firebase/storage';
import {v4 as uuid} from 'uuid';

import {FileType, FileUploadResult} from '../types';

import {getBlob} from './blob';

// Auth

export const AuthUtils = {
  ERROR_USER_NOT_FOUND: 'auth/user-not-found',
  ERROR_INVALID_EMIAL: 'auth/invalid-email',
  ERROR_USER_DISABLED: 'auth/user-disabled',
  ERROR_WRONG_PASSWORD: 'auth/wrong-password',
  ERROR_EMAIL_ALREADY_IN_USE: 'auth/email-already-in-use',
  ERROR_OPERATION_NOT_ALLOWED: 'auth/operation-not-allowed',
  ERROR_WEAK_PASSWORD: 'auth/weak-password',
};

// Firestore

export const FirestoreUtils = {
  ERROR_ON_FETCH: 'firestore/error-on-fetch',
  ERROR_ON_CREATE: 'firestore/error-on-create',
  ERROR_ON_UPDATE: 'firestore/error-on-update',
  ERROR_ON_DELETE: 'firestore/error-on-delete',
  COLLECTION_USERS: 'users',
  COLLECTION_BABIES: 'babies',
  COLLECTION_TRACKINGS: 'trackings',
};

// Storage

async function uploadFileToStorage(
  storageRef: FirebaseStorageTypes.Reference,
  folder: string,
  uri: string,
  fileType: FileType,
): Promise<FileUploadResult> {
  let fileUri: string | undefined;
  let filename: string | undefined;

  filename = `${uuid()}.${fileType}`;

  const blob = await getBlob(uri);

  const fileRef = storageRef.child(folder).child(filename);

  await fileRef.put(blob);

  fileUri = await fileRef.getDownloadURL();

  return {filename, fileUri};
}

async function removeFileFromStorage(
  storageRef: FirebaseStorageTypes.Reference,
  folder: string,
  filename: string,
): Promise<void> {
  const fileRef = storageRef.child(folder).child(filename);
  await fileRef.delete();
}

export const StorageUtils = {
  ERROR_UPLOAD_FAILED: 'storage/upload-failed',
  FOLDER_IMAGES: 'images',
  uploadFileToStorage,
  removeFileFromStorage,
};

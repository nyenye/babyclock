import {FirebaseFirestoreTypes} from '@react-native-firebase/firestore';

export type Duration = {hours?: number; minutes?: number; seconds?: number};

export type LorR = 'l' | 'r';

export type TrackingType = 'sleeping' | 'feeding' | 'diaper'; // | 'pooping';

export type SleepingMode = 'car' | 'crib' | 'held' | 'nursing' | 'stroller';
export type FeedingMode = 'breast' | 'bottle';
export type DiaperMode = 'pee' | 'poop' | 'both';

export type GenderType = 'boy' | 'girl' | 'n/a';

export interface Baby {
  id: string;
  name: string;
  gender: GenderType;
  birthDate: Date | FirebaseFirestoreTypes.Timestamp;
  pictureUri: string;
  pictureFilename: string;
  weight?: number;
  length?: number;
  parents: {[key: string]: boolean};
}

export interface Tracking {
  id: string;
  type: TrackingType;
  start: Date | FirebaseFirestoreTypes.Timestamp;
  end?: Date | FirebaseFirestoreTypes.Timestamp;
  notes?: string;
  [propName: string]: any;
}

export interface FinishedTracking {
  id: string;
  type: TrackingType;
  start: Date;
  end: Date;
  [propName: string]: any;
}

export interface OngoingTracking {
  id: string;
  type: TrackingType;
  start: Date;
  end: undefined;
  [propName: string]: any;
}

// Sleeping

interface SleepTracking {
  modes: Record<SleepingMode, boolean>;
}

export type FinishedSleepTracking = FinishedTracking & SleepTracking;
export type OngoingSleepTracking = OngoingTracking & SleepTracking;

// Feeding

interface FeedTracking {
  mode: FeedingMode;
}

interface BreastFeedTracking {
  leftTimers: Array<{start: Date; end?: Date}>;
  leftDuration: number;
  rightTimers: Array<{start: Date; end?: Date}>;
  rightDuration: number;
}

interface BottleFeedTracking {
  mL: number;
}

export type FinishedFeedTracking = FinishedTracking & FeedTracking;
export type OngoingFeedTracking = OngoingTracking & FeedTracking;

export type FinishedBreastFeedTracking = FinishedFeedTracking &
  BreastFeedTracking;
export type FinishedBottleFeedTracking = FinishedFeedTracking &
  BottleFeedTracking;

export type OngoingBreastFeedTracking = OngoingFeedTracking &
  BreastFeedTracking;
export type OngoingBottleFeedTracking = OngoingFeedTracking &
  BottleFeedTracking;

// Diapers
interface DiaperTracking {
  mode: DiaperMode;
}
export type FinishedDiaperTracking = FinishedTracking & DiaperTracking;

// Tracking store
export type OngoingTrackings = {
  [id: string]: Record<TrackingType, OngoingTracking | undefined>;
};

export type FinishedTrackings = {
  [id: string]: Record<TrackingType, Array<FinishedTracking>>;
};

export interface UserProfile {
  id: string;
  firstName: string;
  lastName: string;
  pictureFilename: string;
  pictureUri: string;
  babies: Array<string>;
  babiesInCare: Array<string>;
}

export type RequestStatus = 'idle' | 'pending' | 'resolved' | 'rejected';

export interface RequestState {
  status: RequestStatus;
  errorCode?: string;
}

export interface FileUploadResult {
  filename: string;
  fileUri: string;
}

export type FileType = 'jpg';

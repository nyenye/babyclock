const jestPreset = require('@testing-library/react-native/jest-preset');

module.exports = {
  rootDir: './',
  preset: '@testing-library/react-native',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  transform: {
    '\\.js$': '<rootDir>/node_modules/react-native/jest/preprocessor.js',
  },
  transformIgnorePatterns: [
    'node_modules/(?!(react-native|@react-navigation|react-native-screens|react-native-gesture-handler|react-native-reanimated)/)',
  ],
  moduleNameMapper: {
    'test-utils': '<rootDir>/__mocks__/testUtils.js',
    '@react-native-firebase/app': '<rootDir>/__mocks__/firebase/app.js',
    '@react-native-firebase/auth': '<rootDir>/__mocks__/firebase/auth.js',
    '@react-native-firebase/firestore':
      '<rootDir>/__mocks__/firebase/firestore.js',
    '@react-native-firebase/storage': '<rootDir>/__mocks__/firebase/storage.js',
    'react-native-image-picker':
      '<rootDir>/__mocks__/react-native-image-picker.js',
    'react-native-vector-icons/Feather':
      '<rootDir>/__mocks__/react-native-vector-icons-Feather.js',
    '@react-native-community/datetimepicker':
      '<rootDir>/__mocks__/react-native-community-datetimepicker.js',
    'react-native-linear-gradient':
      '<rootDir>/__mocks__/react-native-mock-view.js',
    '@react-native-community/masked-view':
      '<rootDir>/__mocks__/react-native-mock-view.js',
    '@react-native-community/async-storage-backend-legacy':
      '<rootDir>/__mocks__/async-storage-backend-legacy.ts',
    '\\.(jpe?g|png|gif|webp|svg|mp4|webm|ogg|mp3|wav|flac|aac|woff2?|eot|ttf|otf)$':
      '<rootDir>/__mocks__/assetsTransformer.js',
  },
  setupFiles: [
    ...jestPreset.setupFiles,
    '<rootDir>/__mocks__/setup.js',
    '<rootDir>/node_modules/react-native-gesture-handler/jestSetup.js',
  ],
  // This is the only part which you can keep
  // from the above linked tutorial's config:
  cacheDirectory: '.jest/cache',
};
